#ifndef PROCESS_MONITOR_H
#define PROCESS_MONITOR_H

#include <dlna/common/task.h>
#include <dlna/common/messagequeue.h>

#include <vector>

namespace dlna {
namespace monitor {

/**
 * \file processmonitor.h
 * \ingroup group_monitor
 */

/**
 * This is responsible to monitor:
 */
class ProcessMonitor : public dlna::common::daemon::Task {
public:
  /**
   * Default constructor
   */
  ProcessMonitor();


  /**
   * Default destructor
   */
  virtual ~ProcessMonitor();


  /**
   * This method monitors whether the file size of a directory changes
   * and informs interested processes.
   */
  void execute();


  /**
   * Initialize the queue server
   */
  void executeOneTime();


  const std::string getName() const;


  unsigned short getDelay() const;

private:
  /**
   * The message queue that is used for the interprocess communication
   * to the db process.
   */
  dlna::common::daemon::MessageQueue* m_mq;

  /**
   * This vector contains a list, where each entry represents the file size
   * of an directory including the sub directories.
   */
  std::vector<unsigned long long> m_sizes;
};

} /* monitor */
} /* dlna */
#endif /* PROCESS_MONITOR_H  */
