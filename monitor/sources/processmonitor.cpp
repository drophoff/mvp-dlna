#include "processmonitor.h"

#include <dlna/common/logutil.h>
#include <dlna/common/configreader.h>

#include <ftw.h>
#include <string>

namespace dlna {
namespace monitor {

static unsigned long long directorySize = 0;

ProcessMonitor::ProcessMonitor() {
    this->m_mq = nullptr;
}

ProcessMonitor::~ProcessMonitor() {
    /**
    Should be empty, otherwise it will be called serveral times
    depending on the size of Tasks (see main).
    */
    if (this->m_mq != nullptr) {
        delete this->m_mq;
        this->m_mq = nullptr;
    }
}

int sum(const char* filePath, const struct stat* sb, int typeflag) {
    directorySize += sb->st_size;
    return 0;
}

void ProcessMonitor::execute() {
    bool changeDetected = false;

    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Check for file changes");

    dlna::common::configuration::Configuration config = dlna::common::configuration::ConfigReader::retrieve();
    for( unsigned int index = 0; index < config.getScanDirectories().size(); index++) {
        std::string folderToBeWalked = config.getScanDirectories().at( index );
        directorySize = 0;

        // Calculate the sum of each entry within the current directory
        int ret = ftw(folderToBeWalked.c_str(), &sum, 1000);
        if (ret < 0) {
            dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Could not determine the size of " + folderToBeWalked + "!");
        }

        if (this->m_sizes.size() == config.getScanDirectories().size()) {
            unsigned long long previousDirectorySize = this->m_sizes.at( index );
            if (previousDirectorySize != directorySize) changeDetected = true;
            this->m_sizes.at( index ) = directorySize;
        } else {
            changeDetected = true;
            this->m_sizes.push_back( directorySize );
        }
    }

    if (changeDetected) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Message send: Notify");
        this->m_mq->send(dlna::common::daemon::Message::NOTIFY);
    }
}


void ProcessMonitor::executeOneTime() {
    if (this->m_mq == nullptr) {
        this->m_mq = new dlna::common::daemon::MessageQueue(dlna::common::daemon::MessageQueue::QUEUE_FILE_CHANGE, true);
    }
}

unsigned short ProcessMonitor::getDelay() const {
    dlna::common::configuration::Configuration config = dlna::common::configuration::ConfigReader::retrieve();
    return config.getScanInterval() * 60;
}

const std::string ProcessMonitor::getName() const {
    return "monitor";
}

} /* monitor */
} /* dlna */
