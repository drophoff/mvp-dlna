#ifndef PROPERTY_H
#define PROPERTY_H

#include "entity.h"

#include <string>

namespace dlna {
namespace db {
namespace property {
namespace entity {

/**
 * \file property.h
 * \ingroup group_db
 */

/**
 * \brief A property represents the smalles granularity of content
 * storage. A property consists of a name and a value. The
 * name is an unique identifier, which are used to access the
 * corresponding value.
 * 
 * Note:
 * NAME=VALUE represents a Property and the following represents
 * a concrete Property ALLOW_UPDATES=true. ALLOW_UPDATES stands
 * for the NAME and true is the VALUE.
 * 
 */
class Property : public dlna::db::foundation::entity::Entity {
public:
  /**
   * Default Constructor
   */
  Property();

  
  void setName(std::string name);

  
  std::string getName() const;

  
  void setValue(std::string value);

  
  std::string getValue() const;

private:
  std::string m_name;

  
  std::string m_value;
};

} /* entity */
} /* property */
} /* db */
} /* dlna */
#endif /* PROPERTY_H */
