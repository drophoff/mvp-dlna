#ifndef PROPERTY_DTO_H
#define PROPERTY_DTO_H

#include "datatransportobject.h"

#include <string>

namespace dlna {
namespace db {
namespace property {
namespace entity {

/**
 * \file propertydto.h
 * \ingroup group_db
 */

/**
 * A property represents the smalles granularity of content
 * storage. A property consists of a name and a value. The
 * name is an unique identifier, which are used to access the
 * corresponding value.
 * 
 * Note:
 * NAME=VALUE represents a Property and the following represents
 * a concrete Property ALLOW_UPDATES=true. ALLOW_UPDATES stands
 * for the NAME and true is the VALUE.
 * 
 */
class PropertyDTO : public dlna::db::foundation::entity::DataTransportObject {
public:
  /**
   * Default Constructor
   */
  PropertyDTO();


  void setValue(const std::string value);


  const std::string getValue() const;

private:
  std::string m_value;
};

} /* entity */
} /* property */
} /* db */
} /* dlna */
#endif /* PROPERTY_DTO_H */
