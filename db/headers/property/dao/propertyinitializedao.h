#ifndef PROPERTY_INITIALIZE_DAO_H
#define PROPERTY_INITIALIZE_DAO_H

#include "baseinitializedao.h"

namespace dlna {
namespace db {
namespace property {
namespace dao {

/**
 * \file propertyinitializedao.h
 * \ingroup group_db
 */

/**
 * This class provides functionality to initialize the
 * table for the entity named 'Property'.
 */
class PropertyInitializeDAO : public dlna::db::foundation::dao::BaseInitializeDAO {
public:
  /**
   * Creates the 'PROPERTY' table if not exist.
   */
  void initialize();
};

} /* dao */
} /* property */
} /* db */
} /* dlna */
#endif /* PROPERTY_INITIALIZE_DAO_H */
