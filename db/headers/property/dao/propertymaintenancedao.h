#ifndef PROPERTY_MAINTENANCE_DAO_H
#define PROPERTY_MAINTENANCE_DAO_H

#include "basedao.h"
#include "property.h"
#include "propertyretrievaldao.h"

#include <string>

namespace dlna {
namespace db {
namespace property {
namespace dao {

/**
 * \file propertymaintenancedao.h
 * \ingroup group_db
 */

/**
 * This class provides functionality to update and create
 * Property records within the database.
 */
class PropertyMaintenanceDAO : public dlna::db::foundation::dao::BaseDAO {
public:
  /**
   * This method creates a new one or updates an existing Property
   * with the provided 'NAME' and 'VALUE'. The attribute 'ID' is
   * never touched or changed by this method. The attribute 'ID'
   * of the provided Property is always overwritten on the basis
   * of the last state from the database.
   * 
   * \param property that should be updated.
   * \return true in case the Property could be updated, otherwise false.
   */
  bool update(dlna::db::property::entity::Property& property);

private:
  PropertyRetrievalDAO m_propertyRetrievalDAO;
};

} /* dao */
} /* property */
} /* db */
} /* dlna */
#endif /* PROPERTY_MAINTENANCE_DAO_H */
