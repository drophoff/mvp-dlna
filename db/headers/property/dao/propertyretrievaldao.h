#ifndef PROPERTY_RETRIEVAL_DAO_H
#define PROPERTY_RETRIEVAL_DAO_H

#include "basedao.h"
#include "property.h"

#include <string>

namespace dlna {
namespace db {
namespace property {
namespace dao {

/**
 * \file propertyretrievaldao.h
 * \ingroup group_db
 */

/**
 * This class provides functionality to read data stored
 * as Property.
 */
class PropertyRetrievalDAO : public dlna::db::foundation::dao::BaseDAO {
public:
  /**
   * This method search for a Property by the provided
   * name.
   * 
   * \param key that identifies a concrete Property
   * \return the found Property or nullptr in case no valid
   * Property could be found.
   */
  dlna::db::property::entity::Property findByName(const std::string key);
};

} /* dao */
} /* property */
} /* db */
} /* dlna */
#endif /* PROPERTY_RETRIEVAL_DAO_H */
