#ifndef TX_MANAGER_H
#define TX_MANAGER_H

#include "transactiondao.h"

#include <string>

namespace dlna {
namespace db {

/**
 * \file transactionmanager.h
 * \ingroup group_db
 */


/**
 * \brief This class provides functionality to start new database transactions and
 * end existing ones.
 */
class TxManager {
public:
    /**
     * Default constructor that configures the unterlaying database connection
     * and starts a new transaction.
     */
    TxManager();

    /**
     * Default destructor that close any existing transaction.
     */
    ~TxManager();

    /**
     * This method ends any existing transaction and starts afterwards
     * a new one.
     */
    void flush();

private:
    /**
     * Starts a new database transaction.
     */
    void beginTx();

    /**
     * Ends an existing database transaction
     */
    void endTx();

    /**
     * DAO to manage the transactions via SQL statements.
     */
    dlna::db::foundation::dao::TransactionDAO m_txDAO;
};

} /* db */
} /* dlna */
#endif /* TX_MANAGER_H */
