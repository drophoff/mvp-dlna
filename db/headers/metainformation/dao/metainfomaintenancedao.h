#ifndef META_INFO_MAINTENANCE_DAO_H
#define META_INFO_MAINTENANCE_DAO_H

#include "metainforetrievaldao.h"
#include "basedao.h"

namespace dlna {
namespace db {
namespace metainfo {
namespace dao {

/**
 * \file metainfomaintenancedao.h
 * \ingroup group_db
 */


/**
 * This class provides functionality to update and create
 * MetaInfo records within the database.
 */
class MetaInfoMaintenanceDAO : public dlna::db::foundation::dao::BaseDAO {
public:
  /**
   * This method creates a new one or updates an existing MetaInfo.
   * 
   * \param metaInfo that should be updated.
   * \return true in case the MetaInfo could be updated, otherwise false.
   */
  bool update(dlna::db::metainfo::entity::MetaInfo& metaInfo);

private:
  MetaInfoRetrievalDAO m_metaInfoRetrievalDAO;
};

} /* dao */
} /* metainfo */
} /* db */
} /* dlna */
#endif /* META_INFO_MAINTENANCE_DAO_H */
