#ifndef META_INFORMATION_INITIALIZE_DAO_H
#define META_INFORMATION_INITIALIZE_DAO_H

#include "baseinitializedao.h"

namespace dlna {
namespace db {
namespace metainfo {
namespace dao {

/**
 * \file metainfoinitializedao.h
 * \ingroup group_db
 */

/**
 * This class provides functionality to initialize the
 * table for the entity named 'MetaInfo'.
 */
class MetaInfoInitializeDAO : public dlna::db::foundation::dao::BaseInitializeDAO {
public:
  /**
   * Creates the 'DATA_STRUCTURE' table if not exist.
   */
  void initialize();
};

} /* dao */
} /* metainfo */
} /* db */
} /* dlna */
#endif /* META_INFORMATION_INITIALIZE_DAO_H */
