#ifndef META_INFO_RETRIEVAL_DAO_H
#define META_INFO_RETRIEVAL_DAO_H

#include "basedao.h"
#include "metainfo.h"

#include <string>
#include <vector>

namespace dlna {
namespace db {
namespace metainfo {
namespace dao {

/**
 * \file metainforetrievaldao.h
 * \ingroup group_db
 */

/**
 * This class provides functionality to read data stored
 * as DataStructure.
 */
class MetaInfoRetrievalDAO : public dlna::db::foundation::dao::BaseDAO {
public: 
  dlna::db::metainfo::entity::MetaInfo retrieveByIdentificationNumber(long int id);

  long int retrieveNextIdentificationNumber();
};

} /* dao */
} /* metainfo */
} /* db */
} /* dlna */
#endif /* META_INFO_RETRIEVAL_DAO_H */
