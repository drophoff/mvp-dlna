#ifndef META_INFO_DTO_H
#define META_INFO_DTO_H

#include "datatransportobject.h"

#include <string>

namespace dlna {
namespace db {
namespace metainfo {
namespace entity {

/**
 * \file metainfodto.h
 * \ingroup group_db
 */

/**
 * This class represents a data structure on a filesystem
 * like a folder or file.
 */
class MetaInfoDTO : public dlna::db::foundation::entity::DataTransportObject {
public:
  /**
   * Default Constructor
   */
  MetaInfoDTO();

  
  void setSize(long long size);

  
  long long getSize() const;
  
  
  void setDate(std::string date);

  
  std::string getDate() const;

private:
  /**
   * Contains the size of a file in bytes.
   */
  long long m_size;
  
  /**
   * Contains the date information in the format of: yyyy-mm-dd.
   */
  std::string m_date;
};

} /* entity */
} /* metainfo */
} /* db */
} /* dlna */
#endif /* META_INFO_DTO_H */
