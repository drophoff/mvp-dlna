#ifndef META_INFO_H
#define META_INFO_H

#include "changelogentity.h"
#include "entity.h"

#include <string>

namespace dlna {
namespace db {
namespace metainfo {
namespace entity {

/**
 * \file metainfo.h
 * \ingroup group_db
 */

/**
 * \brief Stores all meta information for a specific file.
 */
class MetaInfo : public dlna::db::changelog::entity::ChangeLogEntity {
public:
  /**
   * Default Constructor
   */
  MetaInfo();

  
  void setSize(long long size);

  
  long long getSize() const;


  void setDate(std::string date);

  
  std::string getDate() const;

  
  void setPath(std::string path);

  
  std::string getPath() const;

private:
  /**
   * Contains the size of a file in bytes.
   */
  long long m_size;
  
  /**
   * Contains the date information in the format of: yyyy-mm-dd.
   */
  std::string m_date;
  
  /**
   * Contains the full path of the file
   */
  std::string m_path;
};

} /* entity */
} /* metainfo */
} /* db */
} /* dlna */
#endif /* META_INFO_H */
