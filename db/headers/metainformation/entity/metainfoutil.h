#ifndef META_DATA_INFO_UTIL_H
#define META_DATA_INFO_UTIL_H

#include "metainfo.h"

namespace dlna {
namespace db {
namespace metainfo {
namespace entity {
namespace MetaInfoUtil {

/**
 * \file metainfoutil.h
 * \ingroup group_db
 */

bool areEqual(const dlna::db::metainfo::entity::MetaInfo left, const dlna::db::metainfo::entity::MetaInfo right);

} /* MetaInfoUtil */
} /* entity */
} /* metainfo */
} /* db */
} /* dlna */
#endif /* META_DATA_INFO_UTIL_H */
