#ifndef TYPE_DTO_H
#define TYPE_DTO_H

#include "datatransportobject.h"

#include <string>

namespace dlna {
namespace db {
namespace type {
namespace entity {

/**
 * \file typedto.h
 * \ingroup group_db
 */


class TypeDTO : public dlna::db::foundation::entity::DataTransportObject {
public:
  /**
   * Default Constructor
   */
  TypeDTO();

  
  void setName(const std::string name);

  
  const std::string getName() const;

  
  void setProtocol(const std::string protocol);

  
  const std::string getProtocol() const;

private:
  std::string m_name;

  std::string m_protocol;
};

} /* entity */
} /* type */
} /* db */
} /* dlna */
#endif /* TYPE_DTO_H */
