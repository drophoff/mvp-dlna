#ifndef DB_TYPE_H
#define DB_TYPE_H

#include "entity.h"

#include <string>

namespace dlna {
namespace db {
namespace type {
namespace entity {
  
/**
 * \file type.h
 * \ingroup group_db
 */


/**
 * \brief Defines the type of a DataStructure.
 */
class Type : public dlna::db::foundation::entity::Entity {
public:
  /**
   * Default Constructor
   */
  Type();


  void setName(std::string name);


  std::string getName() const;


  void setProtocol(std::string protocol);


  std::string getProtocol() const;

private:
  /**
   * The name of the type entry
   */
  std::string m_name;


  /**
   * Defines the protocol of the entry
   */
  std::string m_protocol;
};

} /* entity */
} /* type */
} /* db */
} /* dlna */
#endif /* DB_TYPE_H */
