#ifndef TYPE_MAINTENANCE_DAO_H
#define TYPE_MAINTENANCE_DAO_H

#include "basedao.h"
#include "type.h"
#include "typeretrievaldao.h"

namespace dlna {
namespace db {
namespace type {
namespace dao {

/**
 * \file typemaintenancedao.h
 * \ingroup group_db
 */

/**
 * This class provides functionality to update and create
 * Type records within the database.
 */
class TypeMaintenanceDAO : public dlna::db::foundation::dao::BaseDAO {
public:
  /**
   * This method creates a new one or updates an existing Type
   * with the provided 'NAME'. The attribute 'ID' is never touched
   * or changed by this method. The attribute 'ID' of the provided
   * Type is always overwritten on the basis of the last state from
   * the database.
   * 
   * \param type that should be updated.
   * \return true in case the Type could be updated, otherwise false.
   */
  bool update(dlna::db::type::entity::Type& type);

private:
  TypeRetrievalDAO m_typeRetrievalDAO;
};

} /* dao */
} /* type */
} /* db */
} /* dlna */
#endif /* TYPE_MAINTENANCE_DAO_H */
