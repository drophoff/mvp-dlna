#ifndef TYPE_INITIALIZE_DAO_H
#define TYPE_INITIALIZE_DAO_H

#include "baseinitializedao.h"

namespace dlna {
namespace db {
namespace type {
namespace dao {

/**
 * \file typeinitializedao.h
 * \ingroup group_db
 */


/**
 * This class provides functionality to initialize the
 * table for the entity named 'Type'.
 */
class TypeInitializeDAO : public dlna::db::foundation::dao::BaseInitializeDAO {
public:
  /**
   * Creates the 'TYPE' table if not exist.
   */
  void initialize();
};

} /* dao */
} /* type */
} /* db */
} /* dlna */
#endif /* TYPE_INITIALIZE_DAO_H */
