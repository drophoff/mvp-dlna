#ifndef TYPE_RETRIVAL_DAO_H
#define TYPE_RETRIVAL_DAO_H

#include "basedao.h"
#include "type.h"

#include <string>

namespace dlna {
namespace db {
namespace type {
namespace dao {
  
/**
 * \file typeretrievaldao.h
 * \ingroup group_db
 */


/**
 * This class provides functionality to read data stored as Type.
 */
class TypeRetrievalDAO : public dlna::db::foundation::dao::BaseDAO {
public:
  /**
   * This method search for a Type by the provided
   * identification number.
   * 
   * \param id that identifies a concrete Type
   * \return the found Type or nullptr in case no valid
   * entry could be found.
   */
  dlna::db::type::entity::Type findByIdentificationNumber(long int id);


  /**
   * This method search for a Type by the provided name.
   * 
   * \param name that identifies a concrete Type
   * \return the found Type or nullptr in case no valid
   * entry could be found.
   */
  dlna::db::type::entity::Type findByName(std::string name);
};

} /* dao */
} /* type */
} /* db */
} /* dlna */
#endif /* TYPE_RETRIVAL_DAO_H */
