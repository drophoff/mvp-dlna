#ifndef DB_ENTITY_H
#define DB_ENTITY_H

#include "identity.h"

namespace dlna {
namespace db {
namespace foundation {
namespace entity {

/**
 * \file entity.h
 * \ingroup group_db
 */

/**
 * \brief This class represents an entity that is stored within a
 * database. It should only be used within the db namespace.
 *
 * All classes that map a row in a database or an entire table
 * must inherit from this class. As a result of this each
 * attribute within this class has a corresponding column in
 * the database.
 */
class Entity : public Identity {
};

} /* entity */
} /* foundation */
} /* db */
} /* dlna */
#endif /* DB_ENTITY_H */
