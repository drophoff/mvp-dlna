#ifndef IDENTITY_H
#define IDENTITY_H

#include <string>

namespace dlna {
namespace db {
namespace foundation {
namespace entity {

/**
 * \file identity.h
 * \ingroup group_db
 */

/**
 * Base class that provides an unique identification number
 * for all Entity and DataTransportObject classes.
 */
class Identity {
public:
  /**
   * Default Constructor
   */
  Identity();


  /**
   * Sets the unique number that identifies the record.
   */
  void setID(long int id);


  /**
   * Returns the unique number that identifies the record.
   */
  long int getID() const;

private:

  /**
   * Contains the unique identifier for the current record.
   */
  long int m_id;
};

} /* entity */
} /* foundation */
} /* db */
} /* dlna */
#endif /* IDENTITY_H */
