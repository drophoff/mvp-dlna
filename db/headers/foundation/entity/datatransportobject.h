#ifndef DATA_TRANSPORT_OBJECT_H
#define DATA_TRANSPORT_OBJECT_H

#include "identity.h"

namespace dlna {
namespace db {
namespace foundation {
namespace entity {

/**
 * \file datatransportobject.h
 * \ingroup group_db
 */

/**
 * \brief This class provides read only access to database information
 * for classes outside the db namespace.
 * 
 * Beyond that the class offers a further abstraction layer to the
 * database. In contrast to the entity class, the design of this class
 * is indepenend from the database design. It may contain more or less
 * information as an concrete entity and can aggregate the information
 * from one or more entites into one object.
 */
class DataTransportObject : public Identity {
};

} /* entity */
} /* foundation */
} /* db */
} /* dlna */
#endif /* DATA_TRANSPORT_OBJECT_H */
