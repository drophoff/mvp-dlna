#ifndef SEARCH_RESULT_H
#define SEARCH_RESULT_H

#include "datatransportobject.h"
#include "datastructuredto.h"
#include "propertydto.h"

#include <vector>

namespace dlna {
namespace db {
namespace foundation {
namespace entity {

  
/**
 * \file searchresult.h
 * \ingroup group_db
 */

/**
 * This class represents a search result.
 */
template <class T>
class SearchResult {
public:

  /**
   * Default Constructor
   */
  SearchResult(T data, long int numberOfEntries) {
      this->m_totalNumber = numberOfEntries;
      this->m_result = data;
  }


  long int getTotalNumber() const {
      return this->m_totalNumber;
  }


  T getResult() {
      return this->m_result;
  }

private:
  /**
   * The total number of all existing records.
   */
  long int m_totalNumber;


  /**
   * The requested subset (selection windows or pagination)
   * of the total existing entries.
   */
  T m_result;
};

} /* entity */
} /* foundation */
} /* db */
} /* dlna */
#endif /* SEARCH_RESULT_H */
