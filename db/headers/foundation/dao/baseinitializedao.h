#ifndef BASE_INITIALIZE_DAO_H
#define BASE_INITIALIZE_DAO_H

#include "basedao.h"

namespace dlna {
namespace db {
namespace foundation {
namespace dao {

/**
 * \file baseinitializedao.h
 * \ingroup group_db
 */

/**
 * This class defines a interface for all DAOs, which shall be
 * initialized. All DAOs must inherit from this class.
 */
class BaseInitializeDAO : public BaseDAO {
public:
  /**
   * Initialize the corresponding table.
   */
  virtual void initialize() = 0;
};

} /* dao */
} /* foundation */
} /* db */
} /* dlna */
#endif /* BASE_INITIALIZE_DAO_H */
