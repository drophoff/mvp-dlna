#ifndef DATABASE_MAINTENANCE_DAO_H
#define DATABASE_MAINTENANCE_DAO_H

#include "basedao.h"

#include <sqlite3.h>

namespace dlna {
namespace db {
namespace foundation {
namespace dao {

/**
 * \file databasemaintenancedao.h
 * \ingroup group_db
 */

/**
 * The database can be configured via SQL statements. This class provides
 * these kind of configuration SQL statements.
 */
class DatabaseMaintenanceDAO : public dlna::db::foundation::dao::BaseDAO {
public:
    /**
     * Configures the underlaying database connection.
     */
    void setupDatabaseConnection(sqlite3* connection);

    /**
     * Updates the internal statistics data to improve the performance.
     */
    void updateStatistics();

    /**
     * Recreates the database based on the existing database but release unused space
     * during the creation to reduce the used hard disk space.
     */
    void shrinkDatabase();

    /**
     * Returns the number of free pages (free list). The method returns zero
     * in case no free page exists.
     */
    int retrieveUnusedPageCount();

    /**
     * Returns the number of occupied pages (page count). There is no
     * distinction as to whether the pages are realy used or free.
     */
    int retrieveOccupiedPageCount();

private:
    /**
     * This method enables the foreign key support.
     */
    void enableForeignKeyConstraints(sqlite3* connection);

    /**
     * Enable the Write-Ahead Logging. Due to this setting, 
     * a read operation blocks no longer an independent write,
     * as it would be the case with the default setting.
     */
    void enableWriteAheadLogging(sqlite3* connection);

    /**
     * Use the memory as default location for temporary
     * objects like tables, indexes, triggers and views.
     */
    void enableMemoryTempStore(sqlite3* connection);

    /**
     * Use the provided size to initialize the internal
     * cache.
     */
    void setCacheSize(sqlite3* connection,int cacheSize);
};

} /* dao */
} /* foundation */
} /* db */
} /* dlna */
#endif /* DATABASE_MAINTENANCE_DAO_H */
