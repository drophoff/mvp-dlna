#ifndef TRANSACTION_DAO_H
#define TRANSACTION_DAO_H

#include "basedao.h"

namespace dlna {
namespace db {
namespace foundation {
namespace dao {

/**
 * \file transactiondao.h
 * \ingroup group_db
 */

/**
 * Transaction are managed via SQL statements. This class provides
 * these kind of transactional SQL statements.
 */
class TransactionDAO : public dlna::db::foundation::dao::BaseDAO {
public:
    /**
     * Starts a new transaction. It is not checked if the previous
     * transaction was closed correctly or whether the opening of a new
     * transaction is possible.
     */
    void startTransaction();

    /**
     * Finish an existing transaction. It is not checked if the
     * previous transaction has been opened correctly or if the
     * current transaction can be terminted.
     */
    void endTransaction();
};

} /* dao */
} /* foundation */
} /* db */
} /* dlna */
#endif /* TRANSACTION_DAO_H */
