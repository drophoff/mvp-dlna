#ifndef BASE_DAO_H
#define BASE_DAO_H

#include <string>
#include <sqlite3.h>

namespace dlna {
namespace db {
namespace foundation {
namespace dao {

/**
 * \file basedao.h
 * \ingroup group_db
 */

/**
 * This class provides base functionality to execute SQL queries.
 * All DAO based classes must inherit from this class.
 */
class BaseDAO {
protected:
  /**
   * This methods opens a database connection if necessary and
   * executes the provided SQL statement. In case an error occurred
   * the failure is written to the application protocol and the
   * return value is set to false.
   *
   * \param statement that shall be executed and interpreted as SQL
   * \param callbackFunctionPtr contains an optional callback function
   * pointer for sqlite3. It is not called on CREATE, INSERT, UPDATE
   * or SELECT statements that returns zero results.
   * \param data that is passed to the callback function pointer in case
   * it is filled and the callback is executed (see formal parameter
   * named callbackFunctionPtr).
   * \param connection that should be used if available
   * \return true in case the provided statement are executed without
   * any error, otherwise false.
   */
  bool execute(std::string statement, int (*callbackFunctionPtr)(void* data, int numberOfColumns, char** columnValues, char** columnNames) = nullptr, void* data = nullptr, sqlite3* connection = nullptr);
};

} /* dao */
} /* foundation */
} /* db */
} /* dlna */
#endif /* BASE_DAO_H */
