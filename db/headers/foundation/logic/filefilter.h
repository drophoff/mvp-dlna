#ifndef FILE_FILTER_H
#define FILE_FILTER_H

#include <string>

namespace dlna {
namespace db {
namespace foundation {
namespace logic {

/**
 * \file filefilter.h
 * \ingroup group_db
 */

/**
 * Provides functionality to determine the type (audio, image) of a file
 * base on the file suffix (extension).
 */
class FileFilter {
public:
   /**
    * Default constructor to set the file suffix (extension) on that
    * the filter logic is working.
    */
   FileFilter(std::string fileSuffix);

   /**
    * Returns true in case the provided file suffix is from the type image,
    * otherwise false.
    */
   bool isImage();

   /**
    * Returns true in case the provided file suffix is from the type audio,
    * otherwise false.
    */
   bool isAudio();

   /**
    * Returns true in case the provided file suffix is from the type audio
    * playlist, otherwise false.
    */
   bool isPlaylist();

private:
   /**
    * Contains the file suffix as lower case.
    */
   std::string m_fileSuffix;
};

} /* logic */
} /* foundation */
} /* db */
} /* dlna */

#endif /* FILE_FILTER_H */
