#ifndef DB_CONNECTION_MANAGER_H
#define DB_CONNECTION_MANAGER_H

#include <dlna/common/singleton.h>

#include <sqlite3.h>

namespace dlna {
namespace db {
namespace foundation {
namespace logic {
  
/**
 * \file connectionmanager.h
 * \ingroup group_db
 */

/**
 * This class provides convenience functions to the underlaying
 * sqlite3 database. The class is responsible to open and close
 * connections and to release used resources on application
 * shutdown.
 */
class ConnectionManager : public dlna::common::Singleton<ConnectionManager> {
friend class dlna::common::Singleton<ConnectionManager>;
private:
  /**
   * Default private constructor to avoid direct object
   * creation
   */
  ConnectionManager();


  /**
   * Default private destructor to avoid direct deletion
   * calls.
   */
  ~ConnectionManager();


  /**
   * Contains the current connection to the database.
   */
  sqlite3* m_dbConnection;

public:
  sqlite3* getConnection() const;
};

} /* logic */
} /* foundation */
} /* db */
} /* dlna */

#endif /* DB_CONNECTION_MANAGER_H */
