#ifndef CONNECTION_SETTINGS_H
#define CONNECTION_SETTINGS_H

#include "databasemaintenancedao.h"

#include <sqlite3.h>

namespace dlna {
namespace db {
namespace foundation {
namespace logic {

/**
 * \file connectionsettings.h
 * \ingroup group_db
 */


/**
 * This class provides functionality to configure the actual
 * existing database connection.
 */
class ConnectionSettings {
public:
    /**
     * Configures the underlaying existing database connection.
     */
    void setupConnection(sqlite3* connection);

private:
    dlna::db::foundation::dao::DatabaseMaintenanceDAO m_maintainDAO;
};

} /* logic */
} /* foundation */
} /* db */
} /* dlna */
#endif /* CONNECTION_SETTINGS_H */
