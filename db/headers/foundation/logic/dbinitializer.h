#ifndef DB_INITIALIZER_H
#define DB_INITIALIZER_H

namespace dlna {
namespace db {
namespace foundation {
namespace logic {
  
/**
 * \file dbinitializer.h
 * \ingroup group_db
 */

class DbInitializer {
public:
    void performInitialization();

    void performReset();

    bool isInitialized();
};

} /* logic */
} /* foundation */
} /* db */
} /* dlna */
#endif /* DB_INITIALIZER_H */
