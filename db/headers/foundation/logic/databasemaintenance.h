#ifndef DATABASE_MAINTENANCE_H
#define DATABASE_MAINTENANCE_H

#include "databasemaintenancedao.h"

namespace dlna {
namespace db {
namespace foundation {
namespace logic {

/**
 * \file databasemaintenance.h
 * \ingroup group_db
 */


/**
 * This class provides functionality to maintain the database.
 */
class DatabaseMaintenance {
public:

    /**
     * Default constructor
     */
    DatabaseMaintenance();

    /**
     * This method performs cost intensive maintenance tasks. To
     * minimize the negative effects, the tasks are performed only after
     * pior checking.
     */
    void performMaintenanceTasks();

private:
    dlna::db::foundation::dao::DatabaseMaintenanceDAO m_maintainDAO;

    unsigned int m_performMaintenanceTasks;
};

} /* logic */
} /* foundation */
} /* db */
} /* dlna */
#endif /* DATABASE_MAINTENANCE_H */
