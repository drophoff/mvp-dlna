#ifndef FILESYSTEM_SEEKER_H
#define FILESYSTEM_SEEKER_H

#include <string>

namespace dlna {
namespace db {
namespace foundation {
namespace logic {

/**
 * \file filesystemseeker.h
 * \ingroup group_db
 */

class FilesystemSeeker {
public:
  /**
   * Default constructor to set both function pointers that are used
   * within the function named 'process'.
   * 
   * \param dataStructureCallbackPtr contains a pointer to a function,
   * which gets called, when the seeker finds a folder or a file.
   * \param acceptFilePtr contains a pointer to a function which
   * accepts (return true) or reject (return false) the provided
   * file based on their suffix.
   */
  FilesystemSeeker(void (*dataStructureCallbackPtr)(std::string directory, std::string name, std::string suffix, long long size, std::string date, long inodeIndex, long parentInodeIndex, bool isDirectory), bool (*acceptFilePtr)(std::string suffix));


  /**
   * This function retrieve all files within the provided directory.
   * The function uses recursion to navigate through the neested
   * subfolders.
   * 
   * In case no suitable file acceptor 'acceptFilePtr' (see c-tor) is
   * given, then all files are accepted per default.
   * 
   * Note: This function use the standard 'C' API to keep the
   * application small and to avoid third party library like boost
   * (see recursive_directory_iterator at libboost_filesystem and
   * libboost_system).
   */
  void process(std::string directory);

private:
  void processRecursive(std::string directory);

  /**
   * Function pointer to the file accept action.
   */
  bool (*m_acceptFile)(std::string suffix);


  /**
   * Function pointer to the folder/file found callback action.
   */
  void (*m_dataStructureCallback)(std::string directory, std::string name, std::string suffix, long long size, std::string date, long inodeIndex, long parentInodeIndex, bool isDirectory);
};

} /* logic */
} /* foundation */
} /* db */
} /* dlna */
#endif /* FILESYSTEM_SEEKER_H */
