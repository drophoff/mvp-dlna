#ifndef CHANGE_LOG_ENTITY_H
#define CHANGE_LOG_ENTITY_H

#include "entity.h"
#include "changelog.h"

namespace dlna {
namespace db {
namespace changelog {
namespace entity {

/**
 * \file changelogentity.h
 * \ingroup group_db
 */

/**
 * \brief This class represents an entity with an attribute to track
 * changes like insert, update, delete or remove.
 */
class ChangeLogEntity : public dlna::db::foundation::entity::Entity {
public:

  /**
   * Default constructor
   */
  ChangeLogEntity(ChangeLog changeLog);

  /**
   * Function to set the change attribute.
   */
  void setChangeLog(ChangeLog changeLog);

  /**
   * Function to retrieve the current change attribute
   */
  ChangeLog getChangeLog() const;

private:
  /**
   * Contains the type of change
   */
  ChangeLog m_changeLog;
};

} /* entity */
} /* changelog */
} /* db */
} /* dlna */
#endif /* CHANGE_LOG_ENTITY_H */
