#ifndef CHANGE_LOG_UTIL_H
#define CHANGE_LOG_UTIL_H

#include "changelog.h"
#include "changelogentity.h"

namespace dlna {
namespace db {
namespace changelog {
namespace entity {
namespace ChangeLogUtil {

/**
 * \file changelogutil.h
 * \ingroup group_db
 */

/**
 * \brief Converts the change log that is provided as std::string into the
 * enumeration ChangeLog.
 * 
 * The passed changeLog must be equal to the enumeration values (see changelog.h).
 */
ChangeLog valueOf(const std::string changeLog);

/**
 * \brief Converts the provided change log (see changelog.h) to std::string.
 */
std::string toString(const ChangeLog changeLog);

std::string  performCreateTransition(ChangeLogEntity& changeLog);

std::string  performUpdateTransition(ChangeLogEntity& changeLog, bool isEqual);


} /* ChangeLogUtil */
} /* entity */
} /* changelog */
} /* db */
} /* dlna */
#endif /* CHANGE_LOG_UTIL_H */
