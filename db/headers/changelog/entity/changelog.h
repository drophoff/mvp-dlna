#ifndef CHANGE_LOG_H
#define CHANGE_LOG_H

#include <string>

namespace dlna {
namespace db {
namespace changelog {
namespace entity {

/**
 * \file changelog.h
 * \ingroup group_db
 * \enum dlna::db::entity::ChangeLog ChangeLog
 */

/**
 * \brief Describes the different states of change for a specific
 * row within a database table.
 */
enum class ChangeLog : char {
  /**
   * Marks the entry as 'created'. This state can only
   * be reached from I.
   * 
   * Allowed state changes:
   * From I to C
   */
  CREATE = 'C',

  /**
   * Marks the entry as 'updated'. This state can only
   * be reached from C.
   * 
   * Allowed state changes:
   * From C to U
   */
  UPDATE = 'U',

  /**
   * Marks the entry as 'processed'. This state can be
   * reached from C and U.
   * 
   * Allowed state changes: 
   * From C to P
   * From U to P
   */
  PROCESSED = 'P',

  /**
   * Marks the entry as 'initial'. This state can be
   * reached from all states.
   * 
   * Allowed state changes:
   * From C to I
   * From U to I
   * From P to I
   */
  INITIAL = 'I',

  /**
   * Marks the entry as 'virtual'. This state is not changeable
   * and can not reached from any other state.
   */
  VIRTUAL = 'V'
};

} /* entity */
} /* changelog */
} /* db */
} /* dlna */
#endif /* CHANGE_LOG_H */
