#ifndef DB_READSERVICE_H
#define DB_READSERVICE_H

#include "searchwindow.h"
#include "searchsorting.h"
#include "searchcriterion.h"
#include "searchresultds.h"
#include "datastructureretrievaldao.h"
#include "propertyretrievaldao.h"
#include "typeretrievaldao.h"
#include "metainforetrievaldao.h"

#include <vector>
#include <memory>
#include <string>

namespace dlna {
namespace db {

/**
 * \file dbreadservice.h
 * \ingroup group_db
 */

class DbReadService {
public:
  /**
   * \brief Search a data structure by the provided identification
   * number. Start index and requested count can be used to retrieve
   * only a subset of the search result. This mechanismus is known
   * as 'selection window' or pagination (settings).
   * 
   * The following structure is an example, to illustrate how this
   * function and its formal parameter are working. The column
   * identification number is filled with an random value that is
   * provided by the underlaying database. Each entry belongs to one
   * parent, which is represented by the corresponding column. Only
   * the "root" entry has no parent and contains the value -1. Due
   * to this all entries with the exception of the root entry are
   * childs of any parent.
   * 
   *  Identification Number (id) | Parent | Attribute X | Attribute Y
   *  :----                      | :----  | :----:      | :----:
   *  0                          | -1     | ..          | ...
   *  87654                      | 0      | ...         | ...
   *  87653                      | 0      | ...         | ...
   *  87652                      | 0      | ...         | ...
   * 
   * In case the function is executed with the following arguments:
   * - ID: 0
   * - ForItSelf: true
   * - Start Index: 0
   * - Requested Count: 10
   *
   * The following virtual structure is build:
   *
   *  Identification Number (id) | Parent | Attribute X | Attribute Y | Index
   *  :----                      | :----  | :----       | :----       | :----
   *  0                          | -1     | ..          | ...         | 0 
   * 
   * Due to the lack of result entries the requested count variable is not
   * considered as a valid search criteria. The forItSelf parameter is true and 
   * therefore it returns only information about the entry 'itself'
   * identified by the provided id.
   * 
   * In case the search is executed with the following arguments:
   * - ID: 0
   * - ForItSelf: false
   * - Start Index: 1
   * - Requested Count: 10
   *
   * Virtual the following structure is created:
   *
   *  Identification Number (id) | Parent | Attribute X | Attribute Y | Index
   *  :----                      | :----  | :----       | :----       | :----
   *  87654                      | 0      | ...         | ...         | 0
   *  87653                      | 0      | ...         | ...         | 1
   *  87652                      | 0      | ...         | ...         | 2
   * 
   * Based on the number of search results the start index can be
   * considered but the requested count is greater than the returned results.
   * As a result of this the last two entries are returned.
   * 
   * The forItSelf parameter is false and due to this all 'child'
   * entries of the desired entry, which is identified by the provided
   * id, are returned. Or in in more detail way. All entries where the
   * parent column contains the value of the id parameter are considered
   * to build the virtual structure.
   * 
   * The start index describes the start point for the subset. Requested count
   * is always added to the start index. In this example the requested count
   * would end at the virtual index value 11. In case no requested count value
   * is provided then the maximum possible value of the variable type is used
   * as default value.
   * 
   * \param window contains the search window with the id, startIndex, requested count
   * and the itself flag.
   * 
   * \param criterion contains the search criteria
   * 
   * \param sorting contains the sort criteria
   * 
   * \return the desired subset defined by start index and requested count of the
   * data structure identifies by id for itself 'true' or his child 'false' elements.
   */
  dlna::db::datastructure::entity::SearchResultDS find(std::shared_ptr<dlna::db::datastructure::entity::SearchWindowDS> window, std::shared_ptr<dlna::db::datastructure::entity::SearchCriterionDS> criterion, std::shared_ptr<dlna::db::datastructure::entity::SearchSortingDS> sorting);


  /**
   * Search a property by the provided key.
   * 
   * \param key that identfies the property.
   */
  std::string findPropertyByKey(const std::string key);


  std::string findFilenameById(long int id);

private:
  /**
   * Retrieval service to access transport structures from type DataStructureDTO
   */
  dlna::db::datastructure::dao::DataStructureRetrievalDAO m_dataStructureRetrievalDAO;


  /**
   * Retrieval service to access transport structures from type 'Property'.
   */
  dlna::db::property::dao::PropertyRetrievalDAO m_propertyRetrievalDAO;

  /**
   * Retrieval service to access transport structures from type 'Type'.
   */
  dlna::db::type::dao::TypeRetrievalDAO m_typeRetrievalDAO;

  /**
   * Retrieval service to access transport structures from type 'MetaInfo'.
   */
  dlna::db::metainfo::dao::MetaInfoRetrievalDAO m_metaInfoRetrievalDAO;
};

} /* db */
} /* dlna */
#endif /* DB_READSERVICE_H */
