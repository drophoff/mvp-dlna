#ifndef CHANGE_IDENTIFIER_H
#define CHANGE_IDENTIFIER_H

#include "datatransportobject.h"

namespace dlna {
namespace db {
namespace changedetection {
namespace entity {

/**
 * \file changeidentifierds.h
 * \ingroup group_db
 */

/**
 * \brief This class represents a changed entry that holds a counter attribute,
 * which is incremeted by one on each update. The class represents a virtual 
 * subset of the data structure entity (see datastructure.h).
 */
class ChangeIdentifierDS : public dlna::db::foundation::entity::DataTransportObject {
public:
  ChangeIdentifierDS();


  void setParentId(long int parentId);


  long int getParentId() const;


  void setCounter(long int counter);


  long int getCounter() const;

private:
    long int m_id;

    long int m_parentId;

    /**
     * Contains the counter for the container update attribute from the data
     * structure entity (see datastructure.h).
     */
    long int m_counter;
};

} /* entity */
} /* changedetection */
} /* db */
} /* dlna */
#endif /* CHANGE_IDENTIFIER_H */