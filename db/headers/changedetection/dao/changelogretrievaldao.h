#ifndef CHANGE_LOG_RETRIEVAL_DAO_H
#define CHANGE_LOG_RETRIEVAL_DAO_H

#include "basedao.h"
#include "changeidentifierds.h"

#include <string>
#include <vector>

namespace dlna {
namespace db {
namespace changedetection {
namespace dao {

/**
 * \file changelogretrievaldao.h
 * \ingroup group_db
 */


class ChangeLogRetrievalDAO : public dlna::db::foundation::dao::BaseDAO {
public:
  std::vector<dlna::db::changedetection::entity::ChangeIdentifierDS> findAllChangedEntries();

  dlna::db::changedetection::entity::ChangeIdentifierDS findChangedEntry(long int id, long int parentId);

private:
  std::string buildFindAllChangedQuery();

  std::string buildFindChangedQuery(long int id, long int parentId);
};

} /* dao */
} /* changedetection */
} /* db */
} /* dlna */
#endif /* CHANGE_LOG_RETRIEVAL_DAO_H */
