#ifndef CHANGE_LOG_MAINTENANCE_DAO_H
#define CHANGE_LOG_MAINTENANCE_DAO_H

#include "basedao.h"
#include "changeidentifierds.h"

#include <string>

namespace dlna {
namespace db {
namespace changedetection {
namespace dao {


/**
 * \file changelogmaintenancedao.h
 * \ingroup group_db
 */

/**
 * This class contians all necessary maintenance operations for the
 * change log detection.
 */
class ChangeLogMaintenanceDAO : public dlna::db::foundation::dao::BaseDAO {
public:
  void removeChangeInformation();

  void deleteAllNotChangedEntities();

  void updateCounter(dlna::db::changedetection::entity::ChangeIdentifierDS identifier);
};

} /* dao */
} /* changedetection */
} /* db */
} /* dlna */
#endif /* DCHANGE_LOG_MAINTENANCE_DAO_H */
