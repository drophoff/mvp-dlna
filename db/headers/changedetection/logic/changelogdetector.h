#ifndef CHANGE_LOG_DETECTOR_H
#define CHANGE_LOG_DETECTOR_H

namespace dlna {
namespace db {
namespace changedetection {
namespace logic {

/**
 * \file changelogdetector.h
 * \ingroup group_db
 */


class ChangeLogDetector {
public:
  /**
   * Should be called before the content of the database changes. The
   * function resets all change log flags back to initial for all entities
   * that inherit from change log entity (see changelogentity.h).
   */
  void prepareChangeDetection();

  /**
   * Should be called after the content of the database has been changed.
   * The function determine the changes based on the current change log
   * flags and updates the corresponding container update counter. All
   * unchanged files with the state initial will be deleted within this
   * function.
   */
  void evaluateChanges();
};

} /* logic */
} /* changedetection */
} /* db */
} /* dlna */
#endif /* CHANGE_LOG_DETECTOR_H */
