#ifndef PROCESS_DB_H
#define PROCESS_DB_H

#include "databasemaintenance.h"

#include <dlna/common/task.h>
#include <dlna/common/messagequeue.h>

#include <sqlite3.h>

namespace dlna {
namespace db {

/**
 * \file processdb.h
 * \ingroup group_db
 */

/**
 * This class is responsible for keeping the database up to date.
 */
class ProcessDb : public dlna::common::daemon::Task {
public:
  /**
   * Default constructor
   */
  ProcessDb();


  /**
   * Default destructor
   */
  virtual ~ProcessDb();


  /**
   * This function keeps the internal database synchron with the
   * external file system.
   */
  void execute();


  const std::string getName() const;

  /**
   * This function initialize the database and fills the tables 
   * with initial data.
   */
  void executeOneTime();


  unsigned short getDelay() const;

private:
  void startDbBasedScanner();

  dlna::common::daemon::MessageQueue* m_mq;

  dlna::db::foundation::logic::DatabaseMaintenance* m_dbMaintenance;
};

} /* db */
} /* dlna */
#endif /* PROCESS_DB_H */
