#ifndef DATA_STRUCTURE_SEARCH_RESULT_H
#define DATA_STRUCTURE_SEARCH_RESULT_H

#include "searchresult.h"

namespace dlna {
namespace db {
namespace datastructure {
namespace entity {

/**
 * \file searchresultds.h
 * \ingroup group_db
 */

/**
 * This class represents a search result.
 */
class SearchResultDS : public dlna::db::foundation::entity::SearchResult<std::vector<DataStructureDTO>> {
public:

  /**
   * Default Constructor
   */
  SearchResultDS(std::vector<DataStructureDTO> data, long int numberOfEntries, long int containerUpdated);


  long int getContainerUpdated() const;

private:
  /**
   * The number how often the container has been updated.
   */
  long int m_containerUpdated;
};

} /* entity */
} /* datastructure */
} /* db */
} /* dlna */
#endif /* DATA_STRUCTURE_SEARCH_RESULT_H */
