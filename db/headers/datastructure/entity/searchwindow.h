#ifndef SEARCH_WINDOW_DS_H
#define SEARCH_WINDOW_DS_H

namespace dlna {
namespace db {
namespace datastructure {
namespace entity {

/**
 * \file searchwindow.h
 * \ingroup group_db
 */

class SearchWindowDS {
public:
  SearchWindowDS(long int objectId, long int startIndex, long int requestedCount, bool itself);

  long int getObjectId() const;

  long int getStartIndex() const;

  long int getRequestedCount() const;

  bool isItself() const;

private:
  long int m_objectId;

  long int m_startIndex;

  long int m_requestedCount;

  bool m_itself;

};

} /* entity */
} /* datastructure */
} /* db */
} /* dlna */
#endif /* SEARCH_WINDOW_DS_H */
