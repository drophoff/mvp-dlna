#ifndef SORT_ORDER_H
#define SORT_ORDER_H

namespace dlna {
namespace db {
namespace datastructure {
namespace entity {

/**
 * \file sortorder.h
 * \ingroup group_db
 * \enum dlna::db::datastructure::entity::SortOrder SortOrder
 */

/**
 * \brief Defines the sort order of results.
 */
enum class SortOrder : short {

  /**
   * Specifies that the results should not be ordered in any way.
   */
  NOT_DEFINED = 0,

  /**
   * Specifies that the results should be returned in ascending
   * order.
   */
  ASCENDING = 1,

  /**
   * Specifies that the results should be returned in descending
   * order.
   */
  DESCENDING = 2
};

} /* entity */
} /* datastructure */
} /* db */
} /* dlna */
#endif /* SORT_ORDER_H */
