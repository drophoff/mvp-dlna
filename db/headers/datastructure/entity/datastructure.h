#ifndef DATA_STRUCTURE_H
#define DATA_STRUCTURE_H

#include "changelogentity.h"
#include "type.h"
#include <string>

namespace dlna {
namespace db {
namespace datastructure {
namespace entity {

/**
 * \file datastructure.h
 * \ingroup group_db
 */

/**
 * \brief This class represents a data structure on a filesystem
 * like a folder or file.
 */
class DataStructure : public dlna::db::changelog::entity::ChangeLogEntity {
public:
  /**
   * Default Constructor
   */
  DataStructure();

  
  void setParentId(long int parentId);

  
  long int getParentId() const;

  
  void setName(std::string name);

  
  std::string getName() const;

  
  long int getTypeId() const;
  
  
  void setTypeId(long int typeId);
  
  
  long int getMetaInfoId() const;
  
  
  void setMetaInfoId(long int metaInfoId);
    
private:
  /**
   * The unique identification number of the parent
   * data structure entry.
   */
  long int m_parentId;


  /**
   * The name of the current data structure entry.
   */
  std::string m_name;


  /**
   *Contains the reference to the type table 
   */
  long int m_typeId;
  
  /**
   * Contains the reference to the meta info table
   */
  long int m_metaInfoId;
};

} /* entity */
} /* datastructure */
} /* db */
} /* dlna */
#endif /* DATA_STRUCTURE_H */
