#ifndef SEARCH_SORT_DS_H
#define SEARCH_SORT_DS_H

#include "sortorder.h"

namespace dlna {
namespace db {
namespace datastructure {
namespace entity {

/**
 * \file searchsorting.h
 * \ingroup group_db
 */

class SearchSortingDS {
public:
  SearchSortingDS(SortOrder title);

  SortOrder getTitle() const;

private:
  SortOrder m_title;
};

} /* entity */
} /* datastructure */
} /* db */
} /* dlna */
#endif /* SEARCH_SORT_DS_H */
