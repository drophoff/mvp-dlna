#ifndef DATA_STRUCTURE_UTIL_H
#define DATA_STRUCTURE_UTIL_H

#include "datastructure.h"

namespace dlna {
namespace db {
namespace datastructure {
namespace entity {
namespace DataStructureUtil {

/**
 * \file datastructureutil.h
 * \ingroup group_db
 */

bool areEqual(const dlna::db::datastructure::entity::DataStructure left, const dlna::db::datastructure::entity::DataStructure right);

} /* DataStructureUtil */
} /* entity */
} /* datastructure */
} /* db */
} /* dlna */
#endif /* DATA_STRUCTURE_UTIL_H */
