#ifndef DATA_STRUCTURE_DTO_H
#define DATA_STRUCTURE_DTO_H

#include "datatransportobject.h"
#include "typedto.h"
#include "metainfodto.h"

#include <string>

namespace dlna {
namespace db {
namespace datastructure {
namespace entity {

/**
 * \file datastructuredto.h
 * \ingroup group_db
 */

/**
 * This class represents a data structure on a filesystem
 * like a folder or file.
 */
class DataStructureDTO : public dlna::db::foundation::entity::DataTransportObject {
public:
  /**
   * Default Constructor
   */
  DataStructureDTO();

  
  void setParentId(long int parentId);

  
  long int getParentId() const;

  
  void setName(std::string name);

  
  std::string getName() const;

  
  dlna::db::type::entity::TypeDTO getType();
  
  
  void setType(dlna::db::type::entity::TypeDTO type);

  
  void setChildCount(long int childCount);

  
  long int getChildCount() const;
  
  
  dlna::db::metainfo::entity::MetaInfoDTO getMetaInfo() const;
  
  
  void setMetaInfo(dlna::db::metainfo::entity::MetaInfoDTO metaInfo);

private:
  long int m_parentId;

  
  std::string m_name;

  
  dlna::db::type::entity::TypeDTO m_type;
  
  
  dlna::db::metainfo::entity::MetaInfoDTO m_metaInfo;

  
  long int m_childCount;
};

} /* entity */
} /* datastructure */
} /* db */
} /* dlna */
#endif /* DATA_STRUCTURE_DTO_H */
