#ifndef DATA_STRUCTURE_INITIALIZE_DAO_H
#define DATA_STRUCTURE_INITIALIZE_DAO_H

#include "baseinitializedao.h"

namespace dlna {
namespace db {
namespace datastructure {
namespace dao {

/**
 * \file datastructureinitializedao.h
 * \ingroup group_db
 */

/**
 * This class provides functionality to initialize the
 * table for the entity named 'DataStructure'.
 */
class DataStructureInitializeDAO : public dlna::db::foundation::dao::BaseInitializeDAO {
public:
  /**
   * Creates the 'DATA_STRUCTURE' table if not exist.
   */
  void initialize();
};

} /* dao */
} /* datastructure */
} /* db */
} /* dlna */
#endif /* DATA_STRUCTURE_INITIALIZE_DAO_H */
