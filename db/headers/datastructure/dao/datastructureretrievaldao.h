#ifndef DATA_STRUCTURE_RETRIEVAL_DAO_H
#define DATA_STRUCTURE_RETRIEVAL_DAO_H

#include "basedao.h"
#include "datastructure.h"
#include "searchwindow.h"
#include "searchsorting.h"

#include <string>
#include <vector>
#include <memory>

namespace dlna {
namespace db {
namespace datastructure {
namespace dao {

/**
 * \file datastructureretrievaldao.h
 * \ingroup group_db
 */

/**
 * This class provides functionality to read data stored
 * as DataStructure.
 */
class DataStructureRetrievalDAO : public dlna::db::foundation::dao::BaseDAO {
public:
  std::vector<dlna::db::datastructure::entity::DataStructure> findAllByIdentificationNumber(std::shared_ptr<dlna::db::datastructure::entity::SearchWindowDS> window, std::shared_ptr<dlna::db::datastructure::entity::SearchSortingDS> sorting);

  long retrieveNumberOfEntriesByIdentificationNumber(std::shared_ptr<dlna::db::datastructure::entity::SearchWindowDS> window);

  dlna::db::datastructure::entity::DataStructure findByBusinessKey(long int id, long int parendId);

private:
  const std::string buildAllSearchQuery(std::shared_ptr<dlna::db::datastructure::entity::SearchWindowDS> window, std::shared_ptr<dlna::db::datastructure::entity::SearchSortingDS> sorting);

  const std::string buildNumberCountQuery(std::shared_ptr<dlna::db::datastructure::entity::SearchWindowDS> window);

  const std::string buildBusinessKeySearchQuery(long int id, long int parendId) const;
};

} /* dao */
} /* datastructure */
} /* db */
} /* dlna */
#endif /* DATA_STRUCTURE_RETRIEVAL_DAO_H */
