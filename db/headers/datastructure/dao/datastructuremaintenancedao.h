#ifndef DATA_STRUCTURE_MAINTENANCE_DAO_H
#define DATA_STRUCTURE_MAINTENANCE_DAO_H

#include "basedao.h"
#include "datastructureretrievaldao.h"

#include <string>

namespace dlna {
namespace db {
namespace datastructure {
namespace dao {

/**
 * \file datastructuremaintenancedao.h
 * \ingroup group_db
 */


/**
 * This class provides functionality to update and create
 * DataStructure records within the database.
 */
class DataStructureMaintenanceDAO : public dlna::db::foundation::dao::BaseDAO {
public:
  /**
   * This method creates a new one or updates an existing DataStructure.
   * 
   * The attribute 'ID' is never touched or changed by this method.
   * The attribute 'ID' of the provided DataStructure is always
   * overwritten on the basis of the last state from the database.
   * 
   * \param dataStructure that should be updated.
   * \return true in case the DataStructure could be updated, otherwise false.
   */
  bool update(dlna::db::datastructure::entity::DataStructure& dataStructure);

private:
  DataStructureRetrievalDAO m_dataStructureRetrievalDAO;
};

} /* dao */
} /* datastructure */
} /* db */
} /* dlna */
#endif /* DATA_STRUCTURE_MAINTENANCE_DAO_H */
