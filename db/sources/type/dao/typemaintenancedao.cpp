#include "typemaintenancedao.h"

#include <cstring>

namespace dlna {
namespace db {
namespace type {
namespace dao {

bool TypeMaintenanceDAO::update(dlna::db::type::entity::Type& type) {
    std::string statement = "";

    dlna::db::type::entity::Type foundType = this->m_typeRetrievalDAO.findByName( type.getName() );
    bool isAlreadyExisting = (foundType.getID() >= 0) ? false : true;
    if (!isAlreadyExisting) {
        statement += "INSERT INTO TYPE (ID, NAME, PROTOCOL) VALUES (";
        statement += std::to_string( type.getID() );
        statement += ", ";
        statement += "\'" + type.getName() + "\'";
        statement += ", ";
        statement += "\'" + type.getProtocol() + "\'";
        statement += ");";
    }

    bool successful = execute( statement, nullptr, nullptr);
    if (successful && !isAlreadyExisting) {
        // Determine new ID on insert
        foundType = this->m_typeRetrievalDAO.findByName( type.getName() );
        isAlreadyExisting = (foundType.getID() >= 0) ? false : true;
        if (isAlreadyExisting) {
            type.setID( foundType.getID() );
        } else {
            successful = false;
        }
    }

    return successful;
}


} /* dao */
} /* type */
} /* db */
} /* dlna */
