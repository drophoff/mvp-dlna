#include "typeretrievaldao.h"

#include <cstring>
#include <cstdlib>
#include <memory>

namespace dlna {
namespace db {
namespace type {
namespace dao {


static int callback(void* data, int numberOfColumns, char** columnValues, char** columnNames) {
    dlna::db::type::entity::Type* type = reinterpret_cast<dlna::db::type::entity::Type*>(data);

    for (int index = 0; index < numberOfColumns; index++) {
	const char* columnValue = (columnValues[index]) ? columnValues[index] : "NULL";
	const char* columnName = columnNames[index];

	if (strcmp("NULL", columnValue) != 0) {
	    if (strcmp(columnName, "ID") == 0) {
		long int id = atol( columnValue  );
		type->setID( id );
	    }

	    if (strcmp(columnName, "NAME") == 0) {
		type->setName( columnValue );
	    }
	    
	    if (strcmp(columnName, "PROTOCOL") == 0) {
		type->setProtocol( columnValue );
	    }
	}

    }

    return 0;
}


dlna::db::type::entity::Type TypeRetrievalDAO::findByIdentificationNumber(long int id) {
    std::string statement = "SELECT * FROM TYPE WHERE ID = ";
    statement += std::to_string( id ) + ";";

    dlna::db::type::entity::Type type;
    execute( statement, &callback, &type );

    return type;
}

dlna::db::type::entity::Type TypeRetrievalDAO::findByName(std::string name) {
    std::string statement = "SELECT * FROM TYPE WHERE NAME LIKE \'";
    statement += name + "\';";

    dlna::db::type::entity::Type type;
    execute( statement, &callback, &type );

    return type;
}


} /* dao */
} /* type */
} /* db */
} /* dlna */
