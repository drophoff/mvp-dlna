#include "typeinitializedao.h"

#include <string>

namespace dlna {
namespace db {
namespace type {
namespace dao {

void TypeInitializeDAO::initialize() {
    std::string statement = "CREATE TABLE IF NOT EXISTS TYPE (ID INTEGER PRIMARY KEY DESC NOT NULL, NAME TEXT UNIQUE NOT NULL, PROTOCOL TEXT NOT NULL ) WITHOUT ROWID;";
    execute(statement);

    statement = "CREATE UNIQUE INDEX IF NOT EXISTS UN_IDX_TYPE_ID ON TYPE ( ID );";
    execute(statement);

    statement = "CREATE UNIQUE INDEX IF NOT EXISTS UN_IDX_TYPE_NA ON TYPE ( NAME );";
    execute(statement);
}


} /* dao */
} /* type */
} /* db */
} /* dlna */
