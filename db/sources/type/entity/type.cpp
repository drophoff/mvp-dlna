#include "type.h"

namespace dlna {
namespace db {
namespace type {
namespace entity {

Type::Type() :
    Entity(),
    m_name(""),
    m_protocol("") {
}

void Type::setName(std::string name) {
    this->m_name = name;
}

std::string Type::getName() const {
    return this->m_name;
}


void Type::setProtocol(std::string protocol) {
    this->m_protocol = protocol;
}

std::string Type::getProtocol() const {
    return this->m_protocol;
}

} /* entity */
} /* type */
} /* db */
} /* dlna */
