#include "typedto.h"

namespace dlna {
namespace db {
namespace type {
namespace entity {

TypeDTO::TypeDTO() :
    DataTransportObject(),
    m_name(""),
    m_protocol("") {
}

void TypeDTO::setName(const std::string name) {
    this->m_name = name;
}


const std::string TypeDTO::getName() const {
    return this->m_name;
}

void TypeDTO::setProtocol(const std::string protocol) {
    this->m_protocol = protocol;
}


const std::string TypeDTO::getProtocol() const {
    return this->m_protocol;
}

} /* entity */
} /* type */
} /* db */
} /* dlna */
