#include "propertyinitializedao.h"

#include <string>

namespace dlna {
namespace db {
namespace property {
namespace dao {

void PropertyInitializeDAO::initialize() {
    std::string statement = "CREATE TABLE IF NOT EXISTS PROPERTY (ID INTEGER PRIMARY KEY NOT NULL, NAME TEXT UNIQUE NOT NULL, VALUE TEXT NOT NULL );";
    execute(statement);

    statement = "CREATE UNIQUE INDEX IF NOT EXISTS IDX_PROP_ID ON PROPERTY ( ID );";
    execute(statement);

    statement = "CREATE UNIQUE INDEX IF NOT EXISTS IDX_PROP_NAME ON PROPERTY ( NAME );";
    execute(statement);
}

} /* dao */
} /* property */
} /* db */
} /* dlna */
