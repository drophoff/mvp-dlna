#include "propertyretrievaldao.h"

#include <cstring>

namespace dlna {
namespace db {
namespace property {
namespace dao {

static int callback(void* data, int numberOfColumns, char** columnValues, char** columnNames) {
    dlna::db::property::entity::Property* property = reinterpret_cast<dlna::db::property::entity::Property*>(data);

    for (int index = 0; index < numberOfColumns; index++) {
        const char* columnValue = (columnValues[index]) ? columnValues[index] : "NULL";
        const char* columnName = columnNames[index];

        if (strcmp("NULL", columnValue) != 0) {
            if (strcmp(columnName, "ID") == 0) {
                long int id = atol( columnValue  );
                property->setID( id );
            }

            if (strcmp(columnName, "NAME") == 0) {
                property->setName( columnValue );
            }

            if (strcmp(columnName, "VALUE") == 0) {
                property->setValue( columnValue );
            }
        }

    }

    return 0;
}


dlna::db::property::entity::Property PropertyRetrievalDAO::findByName(std::string name) {

    std::string statement = "SELECT * FROM PROPERTY WHERE NAME LIKE \'";
    statement += name + "\';";

    dlna::db::property::entity::Property property;
    execute( statement, &callback, &property );


    return property;
}

} /* dao */
} /* property */
} /* db */
} /* dlna */
