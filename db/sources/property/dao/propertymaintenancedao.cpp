#include "propertymaintenancedao.h"

#include <cstring>
#include <cstdlib>

namespace dlna {
namespace db {
namespace property {
namespace dao {

bool PropertyMaintenanceDAO::update(dlna::db::property::entity::Property& property) {
    std::string statement = "";

    dlna::db::property::entity::Property foundProperty = this->m_propertyRetrievalDAO.findByName( property.getName() );

    bool isAlreadyExisting = (foundProperty.getID() > 0) ? true : false;
    if (isAlreadyExisting) {
        statement += "UPDATE PROPERTY SET VALUE = \'" + property.getValue() + "\' WHERE NAME LIKE \'" + property.getName()  + "\' ;";
    } else {
        statement += "INSERT INTO PROPERTY (NAME, VALUE) VALUES ( ";
        statement += "\'" + property.getName() + "\', '" + property.getValue() + "\'" + ");";
    }

    bool successful = execute( statement, nullptr, nullptr);
    if (successful) {
        // Determine new ID on insert or update
        foundProperty = this->m_propertyRetrievalDAO.findByName( property.getName() );
        if (foundProperty.getID() > 0) {
            property.setID( foundProperty.getID() );
        } else {
            successful = false;
        }
    }

    return successful;
}

} /* dao */
} /* property */
} /* db */
} /* dlna */
