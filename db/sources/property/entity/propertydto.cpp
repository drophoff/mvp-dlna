#include "propertydto.h"

namespace dlna {
namespace db {
namespace property {
namespace entity {

PropertyDTO::PropertyDTO() :
    DataTransportObject(),
    m_value("") {
}


void PropertyDTO::setValue(const std::string value) {
    this->m_value = value;
}

const std::string PropertyDTO::getValue() const {
    return this->m_value;
}

} /* entity */
} /* property */
} /* db */
} /* dlna */
