#include "property.h"

namespace dlna {
namespace db {
namespace property {
namespace entity {

Property::Property() :
    Entity(),
    m_name(""),
    m_value("") {
}


void Property::setName(std::string name) {
    this->m_name = name;
}

std::string Property::getName() const {
    return this->m_name;
}

void Property::setValue(std::string value) {
    this->m_value = value;
}

std::string Property::getValue() const {
    return this->m_value;
}
} /* entity */
} /* property */
} /* db */
} /* dlna */
