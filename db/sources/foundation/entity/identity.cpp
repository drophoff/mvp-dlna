#include "identity.h"

namespace dlna {
namespace db {
namespace foundation {
namespace entity {

Identity::Identity() :
    m_id(0) {
}

void Identity::setID(long int id) {
    this->m_id = id;
}

long int Identity::getID() const {
    return this->m_id;
}

} /* entity */
} /* foundation */
} /* db */
} /* dlna */
