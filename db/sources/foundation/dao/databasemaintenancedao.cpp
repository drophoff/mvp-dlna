#include <dlna/common/logutil.h>

#include "databasemaintenancedao.h"

#include <string>
#include <stdlib.h>

namespace dlna {
namespace db {
namespace foundation {
namespace dao {

static int integerBasedStatisticCallback(void* data, int numberOfColumns, char** columnValues, char** columnNames) {
    if (numberOfColumns == 1) {
        int* statValue = reinterpret_cast<int*>(data);

        const char* columnValue = columnValues[0];
        *statValue = std::atoi( columnValue );
    }

    return 0;
}

void DatabaseMaintenanceDAO::setupDatabaseConnection(sqlite3* connection) {
    enableWriteAheadLogging(connection);
    enableForeignKeyConstraints(connection);
    enableMemoryTempStore(connection);
    setCacheSize(connection, -50000);
}

void DatabaseMaintenanceDAO::enableForeignKeyConstraints(sqlite3* connection) {
    execute("PRAGMA foreign_keys = true;", nullptr, nullptr, connection);
}

void DatabaseMaintenanceDAO::enableWriteAheadLogging(sqlite3* connection) {
    execute("PRAGMA journal_mode = WAL;", nullptr, nullptr, connection);
}

void DatabaseMaintenanceDAO::enableMemoryTempStore(sqlite3* connection) {
    execute("PRAGMA temp_store = MEMORY;", nullptr, nullptr, connection);
}

void DatabaseMaintenanceDAO::setCacheSize(sqlite3* connection, int cacheSize) {
    std::string statement;

    statement  = "PRAGMA cache_size = ";
    statement += std::to_string(cacheSize);
    statement += ";";

    execute(statement, nullptr, nullptr, connection);
}

void DatabaseMaintenanceDAO::updateStatistics() {
    execute("PRAGMA optimize;");
}

void DatabaseMaintenanceDAO::shrinkDatabase() {
    execute("VACUUM;");
}

int DatabaseMaintenanceDAO::retrieveUnusedPageCount() {
    int unusedPageCount = 0;
    execute("PRAGMA freelist_count;", &integerBasedStatisticCallback, &unusedPageCount);

    return unusedPageCount;
}

int DatabaseMaintenanceDAO::retrieveOccupiedPageCount() {
    int occupiedPageCount = 0;
    execute("PRAGMA page_count;", &integerBasedStatisticCallback, &occupiedPageCount);

    return occupiedPageCount;
}

} /* dao */
} /* foundation */
} /* db */
} /* dlna */
