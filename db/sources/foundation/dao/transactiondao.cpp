#include "transactiondao.h"

namespace dlna {
namespace db {
namespace foundation {
namespace dao {

void TransactionDAO::startTransaction() {
    execute("BEGIN TRANSACTION", nullptr, nullptr);
}

void TransactionDAO::endTransaction() {
    execute("END TRANSACTION;", nullptr, nullptr);
}

} /* dao */
} /* foundation */
} /* db */
} /* dlna */
