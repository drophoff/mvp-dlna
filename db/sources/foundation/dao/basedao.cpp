#include "basedao.h"
#include "connectionmanager.h"

#include <dlna/common/logutil.h>


namespace dlna {
namespace db {
namespace foundation {
namespace dao {


bool BaseDAO::execute(std::string statement, int(*callbackFunctionPtr)(void* data, int numberOfColumns, char** columnValues, char** columnNames), void* data, sqlite3* connection) {
    char* errorMessage;
    sqlite3* useConnection = (connection != nullptr) ? connection : dlna::db::foundation::logic::ConnectionManager::getInstance().getConnection();
    int ret = sqlite3_exec( useConnection, statement.c_str(), callbackFunctionPtr, data, &errorMessage);
    if (ret != SQLITE_OK) {
        std::string messageStatement = "Execution of the following SQL statement fails [";
        messageStatement += statement;
        messageStatement += "]";

        std::string messageDb = "Database returns the following error [";
        messageDb += errorMessage;
        messageDb += "]";


        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::DEBUG, messageStatement);
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, messageDb);

        sqlite3_free(errorMessage);
    }

    return (ret == SQLITE_OK) ? true : false;
}

} /* dao */
} /* foundation */
} /* db */
} /* dlna */
