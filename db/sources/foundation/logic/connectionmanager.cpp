#include "connectionmanager.h"
#include "connectionsettings.h"

#include <dlna/common/logutil.h>
#include <dlna/common/configreader.h>
#include <dlna/common/fatalexception.h>

#include <sqlite3.h>
#include <string>


namespace dlna {
namespace db {
namespace foundation {
namespace logic {


ConnectionManager::ConnectionManager() : Singleton() {
    dlna::common::configuration::Configuration config = dlna::common::configuration::ConfigReader::retrieve();
    std::string database;
    database += config.getPath();
    database += "/database.db";

    int ret = sqlite3_open(database.c_str(), &(this->m_dbConnection));
    if (ret != SQLITE_OK) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Could not open " + database);

        throw dlna::common::exception::FatalException();
    } else {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::DEBUG, "Open database connection");
    }

    /**
     * Configure the previous created database connection
     */
    ConnectionSettings settings;
    settings.setupConnection( this->m_dbConnection );
}


ConnectionManager::~ConnectionManager() {
    int ret = sqlite3_close(this->m_dbConnection);
    if (ret == SQLITE_BUSY) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Could not close database");
    } else {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::DEBUG, "Close database connection");
    }
}


sqlite3* ConnectionManager::getConnection() const {
    return this->m_dbConnection;
}


} /* logic */
} /* foundation */
} /* db */
} /* dlna */
