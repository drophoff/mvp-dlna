#include "filefilter.h"

#include <algorithm>

namespace dlna {
namespace db {
namespace foundation {
namespace logic {


FileFilter::FileFilter(std::string fileSuffix) :
    m_fileSuffix( fileSuffix ) {

    std::transform(m_fileSuffix.begin(), m_fileSuffix.end(), m_fileSuffix.begin(), ::tolower);
}


bool FileFilter::isImage() {
   bool isImage = false;

   if (m_fileSuffix == "jpeg" ||
       m_fileSuffix == "jpg"  ||
       m_fileSuffix == "png"  ||
       m_fileSuffix == "bmp" ) {

       isImage = true;
   }

   return isImage;
}


bool FileFilter::isAudio() {
    return (m_fileSuffix == "mp3") ? true : false;
}


bool FileFilter::isPlaylist() {
    return (m_fileSuffix == "m3u") ? true : false;
}

} /* logic */
} /* foundation */
} /* db */
} /* dlna */
