#include "filesystemseeker.h"

#include <dlna/common/fatalexception.h>
#include <dlna/common/fileutil.h>
#include <dlna/common/logutil.h>

#include <dirent.h>
#include <string>
#include <cstring>
#include <unistd.h>

namespace dlna {
namespace db {
namespace foundation {
namespace logic {


FilesystemSeeker::FilesystemSeeker(void (*dataStructureCallbackPtr)(std::string directory, std::string name, std::string suffix, long long size, std::string date, long inodeIndex, long parentInodeIndex, bool isDirectory), bool (*acceptFilePtr)(std::string suffix)) {
    if (dataStructureCallbackPtr == nullptr) {
        throw dlna::common::exception::FatalException();
    } else {
        this->m_dataStructureCallback = dataStructureCallbackPtr;
    }
    this->m_acceptFile = acceptFilePtr;
}


static long retrieveInodeNumberForParentDirectory(std::string directory) {
    long inodeIndex = 0;

    DIR* dirHandle = opendir( directory.c_str() );
    if ( dirHandle != nullptr) {
        for(struct dirent* dirEntry = readdir( dirHandle ); dirEntry != nullptr; dirEntry = readdir( dirHandle ) ) {
            if (strcmp( dirEntry->d_name, "."  ) == 0) {
                inodeIndex = dirEntry->d_ino;
            }
        }
        closedir( dirHandle );
    }

    return inodeIndex;
}

void FilesystemSeeker::process(std::string directory) {
    // the current directory
    std::size_t pos = directory.rfind("/");
    if (pos != std::string::npos) {
        std::string currentFolder = directory.substr(0, pos);
        std::string name = directory.substr(++pos);
        long inodeIndex = retrieveInodeNumberForParentDirectory( directory );

        this->m_dataStructureCallback(currentFolder, name, "", 0, "", inodeIndex, 0, true );

        // All sub directories
        processRecursive( directory );
    }
}

void FilesystemSeeker::processRecursive(std::string directory) {
    DIR* dirHandle = opendir( directory.c_str() );
    if ( dirHandle != nullptr) {
        try {
          for(struct dirent* dirEntry = readdir( dirHandle ); dirEntry != nullptr; dirEntry = readdir( dirHandle ) ) {
              if (strcmp( dirEntry->d_name, "."  ) == 0 ||
                      strcmp( dirEntry->d_name, ".." ) == 0 ) {
                  continue;
              }

              bool isDirectory = false;
              if (dirEntry->d_type == DT_UNKNOWN ) {
                  /**
                   * Unsupported 'old kernel' or 'filesystem'!
                   * Use separate stat call and the makro S_ISDIR
                   * to retrieve the directory information.
                   */
                  throw dlna::common::exception::FatalException();
              } else if (dirEntry->d_type == DT_DIR ) {
                  isDirectory = true;
              }

              long inodeIndex = dirEntry->d_ino;
              long parentInodeIndex = retrieveInodeNumberForParentDirectory( directory );
              std::string directoryName = directory + "/";

              if (isDirectory) {
                  // Recursion call
                  this->m_dataStructureCallback(directoryName, dirEntry->d_name, "", 0, "", inodeIndex, parentInodeIndex, true );
                  processRecursive( directoryName + dirEntry->d_name );
              } else {
                  std::string fileName;
                  std::string suffix;
                  std::string modDate;
                  off_t fileSize;
                  bool fileCouldBeLoaded = true;
                  try {
                      dlna::common::util::FileUtil fileUtil(directoryName + dirEntry->d_name);

                      fileName = fileUtil.getPrefix();;
                      suffix = fileUtil.getSuffix();
                      modDate = fileUtil.getTimeFormatted();
                      fileSize = fileUtil.getSize();
                  } catch (dlna::common::exception::FatalException& e) {
                      // One file could not be loaded
                      fileCouldBeLoaded = false;
                  }
                  // Filter found files, if a filer is available
                  if (fileCouldBeLoaded) {
                      if (this->m_acceptFile != nullptr) {
                          if (this->m_acceptFile( suffix )) {
                              this->m_dataStructureCallback(directoryName, fileName, suffix, fileSize, modDate, inodeIndex, parentInodeIndex, false);
                          }
                      } else {
                          this->m_dataStructureCallback(directoryName, fileName, suffix, fileSize, modDate, inodeIndex, parentInodeIndex, false );
                      }
                  }
              }
          }
        } catch (dlna::common::exception::FatalException& e) {
            closedir( dirHandle );
            throw dlna::common::exception::FatalException();
        }

        closedir( dirHandle );
    }
}

} /* logic */
} /* foundation */
} /* db */
} /* dlna */
