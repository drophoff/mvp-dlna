#include <dlna/common/logutil.h>

#include "databasemaintenance.h"

#include <iomanip>
#include <sstream>
#include <ctime>
#include <string>

namespace dlna {
namespace db {
namespace foundation {
namespace logic {


DatabaseMaintenance::DatabaseMaintenance() {
    this->m_performMaintenanceTasks = false;
}


void DatabaseMaintenance::performMaintenanceTasks() {
    std::time_t timestamp;
    std::time(&timestamp);
    std::tm local_time;

    struct tm* time = localtime_r( &timestamp, &local_time );

    /**
     * Use a wide range to support long task delays, otherwise the logic would
     * not be executed in case the interval spans only one hour but the task
     * delay is set to 90 minutes.
     */
    if (!this->m_performMaintenanceTasks && time->tm_hour >= 1 && time->tm_hour < 4) {
        this->m_performMaintenanceTasks = true;

        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Perform Maintenance Tasks");
        this->m_maintainDAO.updateStatistics();

        float free = 0;
        float used = 100;
        int unusedPageCount = this->m_maintainDAO.retrieveUnusedPageCount();
        if (unusedPageCount > 0) {
            int occupiedPageCount = this->m_maintainDAO.retrieveOccupiedPageCount();
            free = ((float)unusedPageCount / occupiedPageCount) * 100;
            used = 100 - free;
        }

        std::stringstream freeStr;
        freeStr << "Free pages: " << std::fixed << std::setprecision(2) << free << " percent";

        std::stringstream usedStr;
        usedStr << "Used pages: " << std::fixed << std::setprecision(2) << used << " percent";

        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, freeStr.str());
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, usedStr.str());
        if (free >= 30) {
            this->m_maintainDAO.shrinkDatabase();
        }
    } else if (this->m_performMaintenanceTasks && time->tm_hour >= 4) {
        this->m_performMaintenanceTasks = false;
    }
}

} /* logic */
} /* foundation */
} /* db */
} /* dlna */
