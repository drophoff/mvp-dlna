#include "dbinitializer.h"
#include "propertyinitializedao.h"
#include "typeinitializedao.h"
#include "datastructureinitializedao.h"
#include "metainfoinitializedao.h"
#include "datastructuremaintenancedao.h"
#include "typemaintenancedao.h"
#include "propertymaintenancedao.h"
#include "metainfomaintenancedao.h"
#include "connectionmanager.h"

#include <dlna/common/configreader.h>
#include <dlna/common/logutil.h>
#include <dlna/common/constant.h>

namespace dlna {
namespace db {
namespace foundation {
namespace logic {

bool DbInitializer::isInitialized() {
    bool isInitialized = true;
    std::string statement = "SELECT * FROM PROPERTY;";

    int ret = sqlite3_exec(ConnectionManager::getInstance().getConnection(), statement.c_str(), nullptr, nullptr, nullptr);
    if (ret != SQLITE_OK) {
        isInitialized = false;
    }

    return isInitialized;
}

void DbInitializer::performReset() {
    /**
     * Reset the property to avoid that the first file scan marks (within the
     * change detection logic) all existing 'unchanged' entries as 'changed'.
     */
    dlna::db::property::dao::PropertyRetrievalDAO propertyRetrievalDAO;

    dlna::db::property::entity::Property containterUpdateIDs = propertyRetrievalDAO.findByName( dlna::common::configuration::Constant::PROPERTY_CONTAINER_UPDATE_ID );
    containterUpdateIDs.setValue( "" );

    dlna::db::property::dao::PropertyMaintenanceDAO propertyMaintenanceDAO;
    propertyMaintenanceDAO.update( containterUpdateIDs );
}

void DbInitializer::performInitialization() {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Setup internal database");

    // Initialize tables within the database
    dlna::db::property::dao::PropertyInitializeDAO initProperty;
    dlna::db::datastructure::dao::DataStructureInitializeDAO initDataStructure;
    dlna::db::type::dao::TypeInitializeDAO initType;
    dlna::db::metainfo::dao::MetaInfoInitializeDAO initMetaInfo;

    // Init DDL
    initProperty.initialize();
    initType.initialize();
    initMetaInfo.initialize();
    initDataStructure.initialize();

    // Insert DML
    dlna::db::type::entity::Type typeStorage;
    typeStorage.setID( 1 );
    typeStorage.setName( "object.container.storageFolder" );
    typeStorage.setProtocol( "" );

    dlna::db::type::entity::Type typeMusicTrack;
    typeMusicTrack.setID( 2 );
    typeMusicTrack.setName( "object.item.audioItem.musicTrack" );
    typeMusicTrack.setProtocol( "http-get:*:audio/mpeg:DLNA.ORG_PN=MP3;DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01700000000000000000000000000000" );

    dlna::db::type::entity::Type typeImage;
    typeImage.setID( 3 );
    typeImage.setName( "object.item.imageItem.photo" );
    //typeImage.setProtocol( "http-get:*:image/jpeg:DLNA.ORG_PN=JPEG_LRG;DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=00F00000000000000000000000000000" );
    typeImage.setProtocol( "http-get:*:image/jpeg:*" );

    dlna::db::type::entity::Type typeVideo;
    typeVideo.setID( 4 );
    typeVideo.setName( "object.item.videoItem.movie" );
    typeVideo.setProtocol( "http-get:*:video/ogg:*" );

    dlna::db::type::dao::TypeMaintenanceDAO typeDAO;
    typeDAO.update( typeStorage );
    typeDAO.update( typeMusicTrack );
    typeDAO.update( typeImage );
    typeDAO.update( typeVideo );

    // Insert DataStructure
    dlna::db::datastructure::entity::DataStructure root;
    root.setID( 0 );
    root.setParentId( -1 );
    root.setName( "root" );
    root.setTypeId( 1 );
    root.setChangeLog( dlna::db::changelog::entity::ChangeLog::VIRTUAL );

    dlna::db::datastructure::dao::DataStructureMaintenanceDAO dataStructureDAO;
    dataStructureDAO.update( root );

    /**
     * Allow virtual playlist folder only when the strict mode is
     * disabled.
     */
    dlna::common::configuration::Configuration config = dlna::common::configuration::ConfigReader::retrieve();
    if (!config.isStrictModeOn()) {
        dlna::db::datastructure::entity::DataStructure playlist;
        playlist.setID( 1 );
        playlist.setParentId( 0 );
        playlist.setName( "playlist" );
        playlist.setTypeId( 1 );
        playlist.setChangeLog( dlna::db::changelog::entity::ChangeLog::VIRTUAL );

        dataStructureDAO.update( playlist );
    }

    // Insert Property
    dlna::db::property::entity::Property systemUpdate;
    systemUpdate.setName( dlna::common::configuration::Constant::PROPERTY_SYSTEM_UPDATE_ID );
    systemUpdate.setValue( "0" );

    dlna::db::property::entity::Property containerUpdate;
    containerUpdate.setName( dlna::common::configuration::Constant::PROPERTY_CONTAINER_UPDATE_ID );
    containerUpdate.setValue( "" );

    dlna::db::property::entity::Property contentScanned;
    contentScanned.setName( dlna::common::configuration::Constant::PROPERTY_INITIAL_SCAN_PERFORMED );
    contentScanned.setValue( "false" );

    dlna::db::property::entity::Property strictMode;
    strictMode.setName( dlna::common::configuration::Constant::PROPERTY_STRICT_MODE );
    if (config.isStrictModeOn()) strictMode.setValue( "true" );
    else strictMode.setValue( "false" );

    dlna::db::property::dao::PropertyMaintenanceDAO propertyDAO;
    propertyDAO.update( systemUpdate );
    propertyDAO.update( containerUpdate );
    propertyDAO.update( contentScanned );
    propertyDAO.update( strictMode );
}

} /* logic */
} /* foundation */
} /* db */
} /* dlna */

