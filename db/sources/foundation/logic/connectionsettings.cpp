#include "connectionsettings.h"

#include <dlna/common/logutil.h>

namespace dlna {
namespace db {
namespace foundation {
namespace logic {


void ConnectionSettings::setupConnection(sqlite3* connection) {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::DEBUG, "Configure database connection");

    this->m_maintainDAO.setupDatabaseConnection(connection);
}

} /* logic */
} /* foundation */
} /* db */
} /* dlna */

