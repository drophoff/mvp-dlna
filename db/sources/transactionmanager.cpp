#include "transactionmanager.h"
#include "connectionmanager.h"

#include <sqlite3.h>


namespace dlna {
namespace db {

TxManager::TxManager() {
    /**
     * Called to initialize the connection. All functions within the
     * connection manager constructor are executed outside of any transaction.
     */
    dlna::db::foundation::logic::ConnectionManager::getInstance();

    beginTx();
}


TxManager::~TxManager() {
    endTx();
}


void TxManager::flush() {
    endTx();
    beginTx();
}


void TxManager::beginTx() {
    this->m_txDAO.startTransaction();
}


void TxManager::endTx() {
    this->m_txDAO.endTransaction();
}

} /* db */
} /* dlna */
