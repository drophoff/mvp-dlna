#include "processdb.h"
#include "filesystemseeker.h"
#include "dbinitializer.h"
#include "datastructuremaintenancedao.h"
#include "metainfomaintenancedao.h"
#include "datastructureretrievaldao.h"
#include "metainforetrievaldao.h"
#include "changelogdetector.h"
#include "transactionmanager.h"
#include "filefilter.h"

#include <dlna/common/logutil.h>
#include <dlna/common/fileutil.h>
#include <dlna/common/fatalexception.h>
#include <dlna/common/configreader.h>

#include <fstream>
#include <ios>

namespace dlna {
namespace db {


ProcessDb::ProcessDb() {
    this->m_mq = nullptr;
    this->m_dbMaintenance = nullptr;
}

ProcessDb::~ProcessDb() {
    if (this->m_mq != nullptr) {
        delete this->m_mq;
        this->m_mq = nullptr;
    }

    if (this->m_dbMaintenance != nullptr) {
        delete this->m_dbMaintenance;
        this->m_dbMaintenance = nullptr;
    }
}


static bool acceptRegularFile(std::string fileName) {
    dlna::db::foundation::logic::FileFilter fileFilter( fileName );
    return (fileFilter.isImage() || fileFilter.isAudio()) ? true : false;
}


static void regularFileCallback(std::string directory, std::string name, std::string suffix, long long size, std::string date, long inodeIndex, long parentInodeIndex, bool isDirectory) {
    dlna::db::datastructure::dao::DataStructureRetrievalDAO dataStructureRetrievalDAO;

    dlna::db::datastructure::entity::DataStructure dataStructure = dataStructureRetrievalDAO.findByBusinessKey( inodeIndex, parentInodeIndex );
    dataStructure.setID( inodeIndex );
    dataStructure.setParentId( parentInodeIndex );
    dataStructure.setName( name );

    if (isDirectory) {
        dataStructure.setTypeId( 1 );
    } else {
        dlna::db::foundation::logic::FileFilter fileFilter( suffix );

        if ( fileFilter.isAudio() ) dataStructure.setTypeId( 2 );
        else if ( fileFilter.isImage() ) dataStructure.setTypeId( 3 );

        dlna::db::metainfo::dao::MetaInfoRetrievalDAO metaInfoRetrievalDAO;

        dlna::db::metainfo::entity::MetaInfo metaInfo = metaInfoRetrievalDAO.retrieveByIdentificationNumber( dataStructure.getMetaInfoId() );
        metaInfo.setSize( size );
        metaInfo.setDate( date );
        metaInfo.setPath( directory + name + "." + suffix );

        dlna::db::metainfo::dao::MetaInfoMaintenanceDAO metaInfoDAO;
        metaInfoDAO.update(metaInfo);

        dataStructure.setMetaInfoId( metaInfo.getID() );
    }

    dlna::db::datastructure::dao::DataStructureMaintenanceDAO dataStructureDAO;
    dataStructureDAO.update( dataStructure );
}


static bool acceptPlaylistFile(std::string fileName) {
    dlna::db::foundation::logic::FileFilter fileFilter( fileName );
    return ( fileFilter.isPlaylist() ) ? true : false;
}

static void playlistFileCallback(std::string directory, std::string name, std::string suffix, long long size, std::string date, long inodeIndex, long parentInodeIndex, bool isDirectory) { 
    if (!isDirectory) {
        dlna::db::datastructure::dao::DataStructureRetrievalDAO dataStructureRetrievalDAO;
        dlna::db::datastructure::dao::DataStructureMaintenanceDAO dataStructureDAO;

        dlna::db::datastructure::entity::DataStructure playlistFile = dataStructureRetrievalDAO.findByBusinessKey( inodeIndex, 1 );
        playlistFile.setID( inodeIndex );
        playlistFile.setParentId( 1 );
        playlistFile.setName( name );
        playlistFile.setTypeId( 1 );

        /**
         * Create an entry for the playlist file itself, which will be
         * used as parent for the files within the playlist.
         */
        dataStructureDAO.update( playlistFile );

        std::string line;
        std::string playlistFileName = directory + name + "." + suffix;
        std::ifstream playlist (playlistFileName);
        while (playlist.good() && std::getline(playlist, line) ) {
            std::string startWith  = line.substr(0, 1);
            if (startWith != "#") {
                std::string playlistEntryFileName = directory + line;
                try {
                    dlna::common::util::FileUtil fileUtil(playlistEntryFileName);

                    /**
                     * Tried to find the playlist entry under the parent playlist.
                     * This is the case, when the file has not been changed
                     * and a previous scan has happened.
                     */
                    dlna::db::datastructure::entity::DataStructure playlistEntry = dataStructureRetrievalDAO.findByBusinessKey( fileUtil.getInode(), inodeIndex );
                    if (playlistEntry.getID() <= 0) {
                        /**
                         * The playlist is handled the first time (first scan). Because
                         * of this the entry exist only under the original folder.
                         */
                        playlistEntry = dataStructureRetrievalDAO.findByBusinessKey( fileUtil.getInode(), parentInodeIndex );
                        if (playlistEntry.getID() > 0) {
                            /**
                             * Copy the existing 'data structure reference' and move it to the
                             * virtual playlist folder.
                             */
                            playlistEntry.setParentId( playlistFile.getID() );
                        }
                    }

                    /**
                     * No matter if it is the first or any other
                     * scan, we should find at least one entry.
                     */
                    if (playlistEntry.getID() > 0) {
                        dataStructureDAO.update( playlistEntry );
                    } else {
                        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::DEBUG, "File \'" + playlistEntryFileName + "\' exists, but could not found in the internal database!");
                    }
                } catch (dlna::common::exception::FatalException& e) {
                    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::DEBUG, "Skip playlist \'"+name + "." + suffix+"\' entry, please check file name.");
                }
            }
        }
    }
}


void ProcessDb::startDbBasedScanner() {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Update of internal database started");

    dlna::db::TxManager transactionManager;

    dlna::db::changedetection::logic::ChangeLogDetector changeDetector;
    changeDetector.prepareChangeDetection();

    dlna::common::configuration::Configuration config = dlna::common::configuration::ConfigReader::retrieve();
    for( unsigned int index = 0; index < config.getScanDirectories().size(); index++) {
        std::string folderToBeScanned = config.getScanDirectories().at( index );

        /**
         * Scan the files and create the database
         */
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Scan for regular files: " + folderToBeScanned);
        dlna::db::foundation::logic::FilesystemSeeker regularFileSeeker( &regularFileCallback, &acceptRegularFile );
        regularFileSeeker.process( folderToBeScanned );

        if (!config.isStrictModeOn()) {
            /**
             * The files within a playlist can reference files that are not scanned
             * right now. Due to this the playlist can not be processed within the first
             * scan run.
             * All files within the playlist are read and the corressponding entry from
             * the database is determined and added to the virtual 'playlist' folder under
             * the name of the playlist file name.
             */
            dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Scan for virtual playlist files: " + folderToBeScanned);
            dlna::db::foundation::logic::FilesystemSeeker playlistFileSeeker( &playlistFileCallback, &acceptPlaylistFile );
            playlistFileSeeker.process( folderToBeScanned );
        }
    }

    changeDetector.evaluateChanges();

    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Update of internal database finished");
}


void ProcessDb::execute() {
    // Check if a Notify message has been received
    if (this->m_mq->receive(dlna::common::daemon::Message::NOTIFY)) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Message received: Notify");

        startDbBasedScanner();
    }

    // Perform regular clean up tasks
    this->m_dbMaintenance->performMaintenanceTasks();
}

unsigned short ProcessDb::getDelay() const {
    return 180;
}

const std::string ProcessDb::getName() const {
    return "db";
}


void ProcessDb::executeOneTime() {
    dlna::db::foundation::logic::DbInitializer db;
    dlna::db::TxManager transactionManager;

    if (!db.isInitialized()) {
        db.performInitialization();
    } else {
        db.performReset();
    }

    if (this->m_mq == nullptr) {
        this->m_mq = new dlna::common::daemon::MessageQueue(dlna::common::daemon::MessageQueue::QUEUE_FILE_CHANGE);
    }

    if (this->m_dbMaintenance == nullptr) {
        this->m_dbMaintenance = new dlna::db::foundation::logic::DatabaseMaintenance();
    }
}

} /* db */
} /* dlna */
