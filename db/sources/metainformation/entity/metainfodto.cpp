#include "metainfodto.h"

namespace dlna {
namespace db {
namespace metainfo {
namespace entity {

MetaInfoDTO::MetaInfoDTO() :
    DataTransportObject(),
    m_size(0),
    m_date("") {
}


void MetaInfoDTO::setSize(long long size) {
    this->m_size = size;
}

long long MetaInfoDTO::getSize() const {
    return this->m_size;
}


void MetaInfoDTO::setDate(std::string date) {
    this->m_date = date;
}


std::string MetaInfoDTO::getDate() const {
    return this->m_date;
}

} /* entity */
} /* metainfo */
} /* db */
} /* dlna */
