#include "metainfoutil.h"

namespace dlna {
namespace db {
namespace metainfo {
namespace entity {
namespace MetaInfoUtil {

bool areEqual(const dlna::db::metainfo::entity::MetaInfo left, const dlna::db::metainfo::entity::MetaInfo right) {
    bool areEqual = true;

    if (left.getID() != right.getID()) areEqual = false;
    if (left.getSize() != right.getSize()) areEqual = false;
    if (left.getDate() != right.getDate()) areEqual = false;

    return areEqual;
}

} /* MetaInfoUtil */
} /* entity */
} /* metainfo */
} /* db */
} /* dlna */

