#include "metainfo.h"

#include <string>

namespace dlna {
namespace db {
namespace metainfo {
namespace entity {

MetaInfo::MetaInfo() :
    ChangeLogEntity(dlna::db::changelog::entity::ChangeLog::INITIAL),
    m_size(0),
    m_date("1900-01-01"),
    m_path("") {
}


void MetaInfo::setSize(long long size) {
    this->m_size = size;
}


long long MetaInfo::getSize() const {
    return this->m_size;
}


void MetaInfo::setDate(std::string date) {
    this->m_date = date;
}

  
std::string MetaInfo::getDate() const {
    return this->m_date;
}


void MetaInfo::setPath(std::string path) {
    this->m_path = path;
}

  
std::string MetaInfo::getPath() const {
    return this->m_path;
}

} /* entity */
} /* metainfo */
} /* db */
} /* dlna */
