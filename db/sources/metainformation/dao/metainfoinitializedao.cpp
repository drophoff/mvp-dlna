#include "metainfoinitializedao.h"

#include <string>

namespace dlna {
namespace db {
namespace metainfo {
namespace dao {

void MetaInfoInitializeDAO::initialize() {
    std::string statement = "CREATE TABLE IF NOT EXISTS META_INFO (ID INTEGER PRIMARY KEY DESC NOT NULL, SIZE INTEGER NOT NULL, DATE TEXT NOT NULL, PATH TEXT NOT NULL, CHANGE_LOG TEXT NOT NULL) WITHOUT ROWID;";
    execute(statement);

    statement = "CREATE INDEX IF NOT EXISTS IDX_MINF_CL ON META_INFO ( CHANGE_LOG );";
    execute(statement);

    statement = "CREATE UNIQUE INDEX IF NOT EXISTS UN_IDX_MINF_ID ON META_INFO ( ID );";
    execute(statement);
}

} /* dao */
} /* metainfo */
} /* db */
} /* dlna */
