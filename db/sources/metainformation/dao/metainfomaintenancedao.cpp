#include "metainfomaintenancedao.h"
#include "changelogutil.h"
#include "metainfoutil.h"

#include <dlna/common/stringutil.h>


namespace dlna {
namespace db {
namespace metainfo {
namespace dao {

bool MetaInfoMaintenanceDAO::update(dlna::db::metainfo::entity::MetaInfo& metaInfo) {
    std::string statement = "";

    dlna::db::metainfo::entity::MetaInfo foundMetaInfo = this->m_metaInfoRetrievalDAO.retrieveByIdentificationNumber( metaInfo.getID() );

    dlna::common::util::StringUtil stringUtil( metaInfo.getPath() );
    std::string path = stringUtil.escapeSQL().getString();

    bool isAlreadyExisting = (foundMetaInfo.getID() > 0) ? true : false;
    if (isAlreadyExisting) {
        bool areEqual = dlna::db::metainfo::entity::MetaInfoUtil::areEqual(metaInfo, foundMetaInfo);
	std::string changeLog = dlna::db::changelog::entity::ChangeLogUtil::performUpdateTransition( metaInfo, areEqual );
	
        statement += "UPDATE META_INFO SET ";
	statement += "SIZE = " + std::to_string( metaInfo.getSize() ) + " ";
	statement += ", ";
	statement += "DATE = \'" + metaInfo.getDate() + "\' ";
	statement += ", ";
	statement += "CHANGE_LOG = \'" + changeLog + "\'";
	statement += ", ";	
	statement += "PATH = \'" + path + "\' ";
	statement += "WHERE ID = " + std::to_string( metaInfo.getID() )  + " ;";
    } else {
	int newID = this->m_metaInfoRetrievalDAO.retrieveNextIdentificationNumber();
	metaInfo.setID( newID );
	
	std::string changeLog = dlna::db::changelog::entity::ChangeLogUtil::performCreateTransition( metaInfo );

        statement += "INSERT INTO META_INFO (ID, SIZE, DATE, PATH, CHANGE_LOG) VALUES ( ";
	statement += std::to_string( metaInfo.getID() );
	statement += ", ";
        statement += std::to_string( metaInfo.getSize() );
	statement += ", ";
        statement += "\'" + metaInfo.getDate() + "\' ";
	statement += ", ";
        statement += "\'" + path + "\' ";
	statement += ", ";
	statement += "\'" +changeLog+ "\'";
	statement += " );";
    }

    bool successful = execute(statement);
    return successful;
}

} /* dao */
} /* metainfo */
} /* db */
} /* dlna */
