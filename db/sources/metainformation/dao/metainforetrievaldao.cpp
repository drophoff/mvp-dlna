#include "metainforetrievaldao.h"
#include "changelogutil.h"
#include <cstdlib>
#include <cstring>

namespace dlna {
namespace db {
namespace metainfo {
namespace dao {

/**
 * \ingroup group_db
 */


static int callbackNextID(void* data, int numberOfColumns, char** columnValues, char** columnNames) {
  long int* currentId = reinterpret_cast<long int*>(data);
  
  for (int index = 0; index < numberOfColumns; index++) {
	const char* columnValue = (columnValues[index]) ? columnValues[index] : "NULL";
	const char* columnName = columnNames[index];

	if (strcmp("", columnValue) != 0) {
	  if (strcmp(columnName, "CUR_ID") == 0) {
		long int id = atol( columnValue  );
		*currentId=id;
	    }
	}
  }
  
  return 0;
}


static int callback(void* data, int numberOfColumns, char** columnValues, char** columnNames) {
    dlna::db::metainfo::entity::MetaInfo* metainfo = reinterpret_cast<dlna::db::metainfo::entity::MetaInfo*>(data);

    for (int index = 0; index < numberOfColumns; index++) {
	const char* columnValue = (columnValues[index]) ? columnValues[index] : "NULL";
	const char* columnName = columnNames[index];

	if (strcmp("NULL", columnValue) != 0 && strcmp("", columnValue) != 0) {
	    if (strcmp(columnName, "ID") == 0) {
		long int id = atol( columnValue  );
		metainfo->setID( id );
	    }

	    if (strcmp(columnName, "SIZE") == 0) {
		long long size = std::stoll( columnValue, nullptr, 0 );
		metainfo->setSize( size );
	    }
	    
	    if (strcmp(columnName, "DATE") == 0) {
		 metainfo->setDate( columnValue );
	    }
	    
	    if (strcmp(columnName, "PATH") == 0) {
		metainfo->setPath( columnValue );
	    }

            if (strcmp(columnName, "CHANGE_LOG") == 0) {
                dlna::db::changelog::entity::ChangeLog changeLog = dlna::db::changelog::entity::ChangeLogUtil::valueOf( columnValue );
                metainfo->setChangeLog( changeLog );
            }
	}

    }

    return 0;
}

long int MetaInfoRetrievalDAO::retrieveNextIdentificationNumber() {
  std::string statement = "SELECT MAX( ID ) AS CUR_ID FROM META_INFO";
  
  long int currentId = 0;
  execute( statement, &callbackNextID, &currentId );
  
  return ++currentId;
}

dlna::db::metainfo::entity::MetaInfo MetaInfoRetrievalDAO::retrieveByIdentificationNumber(long int id) {
    std::string statement = "SELECT * FROM META_INFO WHERE ID = ";
    statement += std::to_string (id) + ";";

    dlna::db::metainfo::entity::MetaInfo metaInfo;
    execute( statement, &callback, &metaInfo );


    return metaInfo;
}

} /* dao */
} /* metainfo */
} /* db */
} /* dlna */