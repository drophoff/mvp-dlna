#include "changeidentifierds.h"

namespace dlna {
namespace db {
namespace changedetection {
namespace entity {

ChangeIdentifierDS::ChangeIdentifierDS() :
  DataTransportObject(),
  m_parentId(0),
  m_counter(0) {
}


void ChangeIdentifierDS::setParentId(long int parentId) {
    this->m_parentId = parentId;
}


long int ChangeIdentifierDS::getParentId() const {
    return this->m_parentId;
}


void ChangeIdentifierDS::setCounter(long int counter) {
    this->m_counter=counter;
}


long int ChangeIdentifierDS::getCounter() const {
    return this->m_counter;
}

} /* entity */
} /* changedetection */
} /* db */
} /* dlna */
