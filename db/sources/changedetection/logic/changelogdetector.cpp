#include "changelogdetector.h"
#include "changelogmaintenancedao.h"
#include "changelogretrievaldao.h"
#include "propertyretrievaldao.h"
#include "propertymaintenancedao.h"

#include <dlna/common/constant.h>

#include <sstream>
#include <limits>

namespace dlna {
namespace db {
namespace changedetection {
namespace logic {

void ChangeLogDetector::prepareChangeDetection() {
    /**
     * Reset the change log flags, for all entries with the 
     * exception of the root entry, back to the initial state.
     */
    dlna::db::changedetection::dao::ChangeLogMaintenanceDAO changeLogMaintenanceDAO;

    changeLogMaintenanceDAO.removeChangeInformation();
}


void ChangeLogDetector::evaluateChanges() {
    dlna::db::changedetection::dao::ChangeLogMaintenanceDAO changeLogMaintenanceDAO;
    dlna::db::changedetection::dao::ChangeLogRetrievalDAO changeLogRetrievalDAO;
    dlna::db::property::dao::PropertyRetrievalDAO propertyRetrievalDAO;
    dlna::db::property::dao::PropertyMaintenanceDAO propertyMaintenanceDAO;


    std::vector<dlna::db::changedetection::entity::ChangeIdentifierDS> changedContainerIDs = changeLogRetrievalDAO.findAllChangedEntries();
    if (changedContainerIDs.size() > 0) {
        /**
         * Check if this is the initial scan where files have been
         * processed to avoid a not necessary update processing (increment
         * system update id and set container update ids).
         * 
         * Because on the initial scan all files are marked/interpreted
         * as changed.
         */
        dlna::db::property::entity::Property initialScanPerformed = propertyRetrievalDAO.findByName( dlna::common::configuration::Constant::PROPERTY_INITIAL_SCAN_PERFORMED );
        if ( initialScanPerformed.getID() > 0 ) {
            int imax = std::numeric_limits<int>::max() - 100000000;
            if ( initialScanPerformed.getValue() == "false" ) {
                /**
                 * This is the initial Scan
                 */
                initialScanPerformed.setValue("true");

                propertyMaintenanceDAO.update( initialScanPerformed );
            } else {
                /**
                 * Update the SystemUpdateID only in case we detect a change
                 * on the file system (database) and it is not the initial scan.
                 */
                dlna::db::property::entity::Property propertySystemUpdateID = propertyRetrievalDAO.findByName( dlna::common::configuration::Constant::PROPERTY_SYSTEM_UPDATE_ID );
                if (propertySystemUpdateID.getID() > 0) {
                    /**
                     * Perform an overflow in case the system update id reaches
                     * the max value, otherwise only increment the existing value.
                     */
                    int systemUpdateID = stoi( propertySystemUpdateID.getValue() );
                    if (systemUpdateID >= imax) {
                        systemUpdateID = 1;
                    } else {
                        systemUpdateID++;
                    }

                    propertySystemUpdateID.setValue( std::to_string( systemUpdateID ));
                    propertyMaintenanceDAO.update( propertySystemUpdateID );
                }

                /**
                 * Increase the ContainerUpdateIDs based on the previous determined
                 * Container IDs that have been changed.
                 */
                std::stringstream containerUpdateIDsAsCSV;
                bool firstEntry = true;
                for (unsigned int index = 0; index < changedContainerIDs.size(); index++) {
                    dlna::db::changedetection::entity::ChangeIdentifierDS container = changedContainerIDs.at( index );

                    /**
                      * Increase the existing container update id counter
                      * and then added it to the csv list of updated ids.
                      */
                    int currentValue = container.getCounter();
                    if (currentValue > imax) {
                        currentValue = 1;
                    } else {
                      currentValue++;
                    }

                    container.setCounter( currentValue );
                    changeLogMaintenanceDAO.updateCounter( container );

                    if (firstEntry) {
                        firstEntry = false;
                    } else {
                        containerUpdateIDsAsCSV << ",";
                    }

                    containerUpdateIDsAsCSV << std::to_string( container.getID() );
                    containerUpdateIDsAsCSV << ",";
                    containerUpdateIDsAsCSV << std::to_string( container.getCounter() );
                }

                dlna::db::property::entity::Property propertyContainerUpdateIDs = propertyRetrievalDAO.findByName( dlna::common::configuration::Constant::PROPERTY_CONTAINER_UPDATE_ID );
                if (propertyContainerUpdateIDs.getID() > 0) {
                    if (containerUpdateIDsAsCSV.str().size() <= 16384) {
                        propertyContainerUpdateIDs.setValue( containerUpdateIDsAsCSV.str() );
                    } else {
                        propertyContainerUpdateIDs.setValue( "" );
                    }
                    propertyMaintenanceDAO.update( propertyContainerUpdateIDs );
                }
            }
        }
    }

    /**
     * At this point the change log values have been the following meaning.
     *
     * All entries with an initial flag have not been processed. This
     * means the corresponding files have been deleted.
     *
     * All entries with the created flag are new files that were
     * not part of a previous scan.
     *
     * All entries with the updated flag are changed files that were
     * part of a previous scan.
     *
     * All entries with the processed flag are unchanged but existing
     * entries, which are part of a previous scan.
     *
     * The root entry with the special virtual flag should be
     * considered during the change log handling and counting.
     * 
     *      Deleted: Flag I
     *      Updated: Flag U
     *      Created: Flag C
     *    Unchanged: Flag P
     * Total Number: Flag I + Flag U + Flag C + Flag P
     *      Virtual: Flag V (for example root and playlist folder
     *
     * Delete all entries that have the flag I because initial
     * entries have not been changed (updated, created, processed)
     * and we assume their deletion.
     */
    changeLogMaintenanceDAO.deleteAllNotChangedEntities();
}

} /* logic */
} /* changedetection */
} /* db */
} /* dlna */

