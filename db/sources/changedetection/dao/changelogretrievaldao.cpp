#include "changelogretrievaldao.h"
#include "changelogutil.h"

#include <cstring>
#include <sstream>

namespace dlna {
namespace db {
namespace changedetection {
namespace dao {

static int changedAllCallback(void* data, int numberOfColumns, char** columnValues, char** columnNames) {
    std::vector<dlna::db::changedetection::entity::ChangeIdentifierDS>* changedContainerIDs = reinterpret_cast<std::vector<dlna::db::changedetection::entity::ChangeIdentifierDS>*>(data);

    dlna::db::changedetection::entity::ChangeIdentifierDS identifer;
    for (int index = 0; index < numberOfColumns; index++) {
        const char* columnValue = (columnValues[index]) ? columnValues[index] : "NULL";
        const char* columnName = columnNames[index];

        if (strcmp("NULL", columnValue) != 0) {
            if (strcmp(columnName, "ID") == 0) {
                long int id = atol( columnValue  );
                identifer.setID( id );
            }

            if (strcmp(columnName, "PARENT_ID") == 0) {
                long int parentId = atol( columnValue  );
                identifer.setParentId( parentId );
            }

            if (strcmp(columnName, "COUNTER") == 0) {
                long int counter = atol( columnValue  );
                identifer.setCounter( counter );
            }
        }
    }
    changedContainerIDs->push_back( identifer );

    return 0;
}


static int changedEntryCallback(void* data, int numberOfColumns, char** columnValues, char** columnNames) {
    dlna::db::changedetection::entity::ChangeIdentifierDS* identifer = reinterpret_cast<dlna::db::changedetection::entity::ChangeIdentifierDS*>(data);

    for (int index = 0; index < numberOfColumns; index++) {
        const char* columnValue = (columnValues[index]) ? columnValues[index] : "NULL";
        const char* columnName = columnNames[index];

        if (strcmp("NULL", columnValue) != 0) {
            if (strcmp(columnName, "ID") == 0) {
                long int id = atol( columnValue  );
                identifer->setID( id );
            }

            if (strcmp(columnName, "PARENT_ID") == 0) {
                long int parentId = atol( columnValue  );
                identifer->setParentId( parentId );
            }

            if (strcmp(columnName, "CONTAINER_UPDATE_COUNTER") == 0) {
                long int counter = atol( columnValue  );
                identifer->setCounter( counter );
            }
        }
    }

    return 0;
}


std::vector<dlna::db::changedetection::entity::ChangeIdentifierDS> ChangeLogRetrievalDAO::findAllChangedEntries() {
  std::string statement = buildFindAllChangedQuery();

  std::vector<dlna::db::changedetection::entity::ChangeIdentifierDS> changedContainerIDs;
  execute(statement, &changedAllCallback, &changedContainerIDs);

  return changedContainerIDs;
}


std::string ChangeLogRetrievalDAO::buildFindAllChangedQuery() {
  std::string processed = dlna::db::changelog::entity::ChangeLogUtil::toString( dlna::db::changelog::entity::ChangeLog::PROCESSED );
  std::string virt = dlna::db::changelog::entity::ChangeLogUtil::toString( dlna::db::changelog::entity::ChangeLog::VIRTUAL );
  std::stringstream statement;

  statement << "SELECT ";
  statement <<     "DISTINCT ";
  statement <<        "( ";
  statement <<            "SELECT ";
  statement <<                "CASE DA.TYPE_ID ";
  statement <<                    "WHEN 1 THEN DA.ID ";
  statement <<                    "ELSE DA.PARENT_ID ";
  statement <<                "END ";
  statement <<        ") ID, ";
  statement <<        "( ";
  statement <<            "SELECT ";
  statement <<                "CASE DA.TYPE_ID ";
  statement <<                    "WHEN 1 THEN DA.PARENT_ID ";
  statement <<                    "ELSE ( ";
  statement <<                         "SELECT ";
  statement <<                             "SUB.PARENT_ID ";
  statement <<                         "FROM ";
  statement <<                             "DATA_STRUCTURE SUB ";
  statement <<                         "WHERE ";
  statement <<                             "SUB.ID = DA.PARENT_ID ";
  statement <<                     ") ";
  statement <<                "END ";
  statement <<        ") PARENT_ID, ";
  statement <<        "( ";
  statement <<            "SELECT ";
  statement <<                "CASE DA.TYPE_ID ";
  statement <<                    "WHEN 1 THEN DA.CONTAINER_UPDATE_COUNTER ";
  statement <<                    "ELSE ( ";
  statement <<                         "SELECT ";
  statement <<                             "SUB.CONTAINER_UPDATE_COUNTER ";
  statement <<                         "FROM ";
  statement <<                             "DATA_STRUCTURE SUB ";
  statement <<                         "WHERE ";
  statement <<                             "SUB.ID = DA.PARENT_ID ";
  statement <<                     ") ";
  statement <<                "END ";
  statement <<        ") COUNTER ";
  statement << "FROM ";
  statement <<     "DATA_STRUCTURE DA ";
  statement <<     "LEFT OUTER JOIN META_INFO MI ON MI.ID = DA.META_INFO_ID ";
  statement << "WHERE ";
  statement <<     "DA.CHANGE_LOG NOT LIKE \'"+virt+"\' ";
  statement << "AND ( ";
  statement <<     "DA.CHANGE_LOG NOT LIKE \'"+processed+"\' ";
  statement <<         "OR ";
  statement <<     "( MI.CHANGE_LOG IS NOT NULL AND MI.CHANGE_LOG NOT LIKE \'"+processed+"\' ) ";
  statement << ");";

  return statement.str();
}


dlna::db::changedetection::entity::ChangeIdentifierDS ChangeLogRetrievalDAO::findChangedEntry(long int id, long int parentId) {
    std::string statement = buildFindChangedQuery(id, parentId);

    dlna::db::changedetection::entity::ChangeIdentifierDS changeIdentifier;
    execute(statement, &changedEntryCallback, &changeIdentifier);

    return changeIdentifier;
}


std::string ChangeLogRetrievalDAO::buildFindChangedQuery(long int id, long int parentId) {
    std::string statement;

    statement += "SELECT ";
    statement +=     "* ";
    statement += "FROM ";
    statement +=     "DATA_STRUCTURE ";
    statement += "WHERE ";
    statement +=     "ID = " + std::to_string( id ) + " ";
    statement += "AND ";
    statement +=     "PARENT_ID = "+ std::to_string( parentId );
    statement += ";";

    return statement;
}

} /* dao */
} /* changedetection */
} /* db */
} /* dlna */

