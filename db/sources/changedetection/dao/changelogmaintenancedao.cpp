#include "changelogmaintenancedao.h"
#include "changelogutil.h"

namespace dlna {
namespace db {
namespace changedetection {
namespace dao {

void ChangeLogMaintenanceDAO::updateCounter(dlna::db::changedetection::entity::ChangeIdentifierDS identifier) {
    std::string statement;

    statement =  "UPDATE DATA_STRUCTURE SET ";
    statement += "CONTAINER_UPDATE_COUNTER = " + std::to_string( identifier.getCounter() ) + " ";
    statement += "WHERE ID = " + std::to_string( identifier.getID() ) + " ";
    statement += "AND PARENT_ID = " + std::to_string( identifier.getParentId() );
    statement += ";";

    execute(statement);
}


void ChangeLogMaintenanceDAO::removeChangeInformation() {
    std::string changeLogInitial = dlna::db::changelog::entity::ChangeLogUtil::toString( dlna::db::changelog::entity::ChangeLog::INITIAL );
    std::string changeLogVirtual = dlna::db::changelog::entity::ChangeLogUtil::toString( dlna::db::changelog::entity::ChangeLog::VIRTUAL );

    std::string statement = "UPDATE DATA_STRUCTURE SET CHANGE_LOG = \'" + changeLogInitial + "\' WHERE CHANGE_LOG NOT LIKE \'" + changeLogVirtual + "\';";
    execute(statement, nullptr, nullptr);

    statement = "UPDATE META_INFO SET CHANGE_LOG = \'" + changeLogInitial + "\' WHERE CHANGE_LOG NOT LIKE \'" + changeLogVirtual + "\';";
    execute(statement);
}


void ChangeLogMaintenanceDAO::deleteAllNotChangedEntities() {
    std::string changeLog = dlna::db::changelog::entity::ChangeLogUtil::toString( dlna::db::changelog::entity::ChangeLog::INITIAL );

    std::string statement = "DELETE FROM DATA_STRUCTURE WHERE CHANGE_LOG LIKE \'" + changeLog + "\';";
    execute(statement);

    statement = "DELETE FROM META_INFO WHERE CHANGE_LOG LIKE \'" +changeLog+ "\';";
    execute(statement);
}

} /* dao */
} /* changedetection */
} /* db */
} /* dlna */

