#include "datastructureinitializedao.h"

#include <string>

namespace dlna {
namespace db {
namespace datastructure {
namespace dao {

void DataStructureInitializeDAO::initialize() {
    std::string statement = "CREATE TABLE IF NOT EXISTS DATA_STRUCTURE (ID INTEGER NOT NULL, PARENT_ID INTEGER NOT NULL, NAME TEXT NOT NULL, CONTAINER_UPDATE_COUNTER INTEGER, CHANGE_LOG TEXT NOT NULL, PRIMARY KEY (ID, PARENT_ID)) WITHOUT ROWID;";
    execute(statement);

    statement = "CREATE INDEX IF NOT EXISTS IDX_DASC_ID ON DATA_STRUCTURE ( ID );";
    execute(statement);

    statement = "CREATE INDEX IF NOT EXISTS IDX_DASC_PA ON DATA_STRUCTURE ( PARENT_ID );";
    execute(statement);

    statement = "CREATE INDEX IF NOT EXISTS IDX_DASC_CL ON DATA_STRUCTURE ( CHANGE_LOG );";
    execute(statement);

    statement = "ALTER TABLE DATA_STRUCTURE ADD COLUMN META_INFO_ID INTEGER REFERENCES META_INFO ( ID );";
    execute(statement);

    statement = "ALTER TABLE DATA_STRUCTURE ADD COLUMN TYPE_ID INTEGER REFERENCES TYPE ( ID );";
    execute(statement);

    statement = "CREATE INDEX IF NOT EXISTS IDX_DASC_MINF_ID ON DATA_STRUCTURE ( META_INFO_ID );";
    execute(statement);

    statement = "CREATE INDEX IF NOT EXISTS IDX_DASC_TY_ID ON DATA_STRUCTURE ( TYPE_ID );";
    execute(statement);
}

} /* dao */
} /* datastructure */
} /* db */
} /* dlna */
