#include "datastructureretrievaldao.h"
#include "changelogutil.h"

#include <cstring>
#include <sstream>
#include <string>

namespace dlna {
namespace db {
namespace datastructure {
namespace dao {

static int numberOfEntriesCallback(void* data, int numberOfColumns, char** columnValues, char** columnNames) {
    long int* numberOfEntries = reinterpret_cast<long int*>(data);

    for (int index = 0; index < numberOfColumns; index++) {
        const char* columnValue = (columnValues[index]) ? columnValues[index] : "NULL";
        const char* columnName = columnNames[index];

        if (strcmp("NULL", columnValue) != 0) {
            if (strcmp(columnName, "CNT") == 0) {
                long int cnt = atol( columnValue  );
                *numberOfEntries = cnt;
            }
        }
    }

    return 0;
}


static int dataStructureCallback(void* data, int numberOfColumns, char** columnValues, char** columnNames) {
    dlna::db::datastructure::entity::DataStructure* dataStructure = reinterpret_cast<dlna::db::datastructure::entity::DataStructure*>(data);

    for (int index = 0; index < numberOfColumns; index++) {
        const char* columnValue = (columnValues[index]) ? columnValues[index] : "NULL";
        const char* columnName = columnNames[index];

        if (strcmp("NULL", columnValue) != 0) {
            if (strcmp(columnName, "ID") == 0) {
                long int id = atol( columnValue  );
                dataStructure->setID( id );
            }

            if (strcmp(columnName, "NAME") == 0) {
                dataStructure->setName( columnValue );
            }

            if (strcmp(columnName, "PARENT_ID") == 0) {
                long int id = atol( columnValue  );
                dataStructure->setParentId( id );
            }

            if (strcmp(columnName, "TYPE_ID") == 0) {
                long int id = atol( columnValue  );
                dataStructure->setTypeId( id );
            }

            if (strcmp(columnName, "META_INFO_ID") == 0) {
                long int id = atol( columnValue  );
                dataStructure->setMetaInfoId( id );
            }

            if (strcmp(columnName, "CHANGE_LOG") == 0) {
                dlna::db::changelog::entity::ChangeLog changeLog = dlna::db::changelog::entity::ChangeLogUtil::valueOf( columnValue );
                dataStructure->setChangeLog( changeLog );
            }
        }
    }

    return 0;
}



static int allDataStructureCallback(void* data, int numberOfColumns, char** columnValues, char** columnNames) {
    std::vector<dlna::db::datastructure::entity::DataStructure>* dataStructures = reinterpret_cast<std::vector<dlna::db::datastructure::entity::DataStructure>*>(data);

    dlna::db::datastructure::entity::DataStructure dataStructure;
    for (int index = 0; index < numberOfColumns; index++) {
        const char* columnValue = (columnValues[index]) ? columnValues[index] : "NULL";
        const char* columnName = columnNames[index];

        if (strcmp("NULL", columnValue) != 0) {
            if (strcmp(columnName, "ID") == 0) {
                long int id = atol( columnValue  );
                dataStructure.setID( id );
            }

            if (strcmp(columnName, "NAME") == 0) {
                dataStructure.setName( columnValue );
            }

            if (strcmp(columnName, "PARENT_ID") == 0) {
                long int id = atol( columnValue  );
                dataStructure.setParentId( id );
            }

            if (strcmp(columnName, "TYPE_ID") == 0) {
                long int id = atol( columnValue  );
                dataStructure.setTypeId( id );
            }

            if (strcmp(columnName, "META_INFO_ID") == 0) {
                long int id = atol( columnValue  );
                dataStructure.setMetaInfoId( id );
            }

            if (strcmp(columnName, "CHANGE_LOG") == 0) {
                dlna::db::changelog::entity::ChangeLog changeLog = dlna::db::changelog::entity::ChangeLogUtil::valueOf( columnValue );
                dataStructure.setChangeLog( changeLog );
            }
        }
    }

    dataStructures->push_back( dataStructure );

    return 0;
}


const std::string DataStructureRetrievalDAO::buildAllSearchQuery(std::shared_ptr<dlna::db::datastructure::entity::SearchWindowDS> window, std::shared_ptr<dlna::db::datastructure::entity::SearchSortingDS> sorting) {
    std::stringstream statement;

    if (window) {
        std::string column;
        if (window->isItself()) column = "ID";
        else column = "PARENT_ID";

        statement << "SELECT ";
        statement <<     "RDS.ID, ";
        statement <<     "RDS.NAME, ";
        statement <<     "RDS.PARENT_ID, ";
        statement <<     "RDS.TYPE_ID, ";
        statement <<     "RDS.META_INFO_ID ";
        statement << "FROM ";
        statement <<     "DATA_STRUCTURE AS RDS ";
        statement << "WHERE ";
        statement <<     "RDS." << column << " = " << std::to_string( window->getObjectId() ) << " ";

        if (sorting) {
            std::string titleSorting;

            if (sorting->getTitle() == dlna::db::datastructure::entity::SortOrder::ASCENDING) {
                titleSorting = "ASC";
            } else if (sorting->getTitle() == dlna::db::datastructure::entity::SortOrder::DESCENDING) {
                titleSorting = "DESC";
            }
            if (sorting->getTitle() != dlna::db::datastructure::entity::SortOrder::NOT_DEFINED) {
                statement << "ORDER BY RDS.NAME " << titleSorting << " ";
            }
        }
        statement << "LIMIT " << std::to_string( window->getRequestedCount() ) << " OFFSET " << std::to_string( window->getStartIndex() );
    }

    return statement.str();
}


const std::string DataStructureRetrievalDAO::buildNumberCountQuery(std::shared_ptr<dlna::db::datastructure::entity::SearchWindowDS> window) {
    std::stringstream statement;

    if (window) {
        std::string column;
        if (window->isItself()) column = "ID";
        else column = "PARENT_ID";

        statement << "SELECT ";
        statement <<     "COUNT(*) AS CNT ";
        statement << "FROM ";
        statement <<     "DATA_STRUCTURE AS RDS ";
        statement << "WHERE ";
        statement <<     "RDS." << column << " = " << std::to_string( window->getObjectId() ) << " ";
    }

    return statement.str();
}

const std::string DataStructureRetrievalDAO::buildBusinessKeySearchQuery(long int id, long int parentId) const {
    std::string statement;
    statement += "SELECT ";
    statement +=     "* ";
    statement += "FROM ";
    statement +=     "DATA_STRUCTURE ";
    statement += "WHERE ";
    statement +=     "ID = " + std::to_string( id ) + " ";
    statement += "AND ";
    statement +=     "PARENT_ID = "+ std::to_string( parentId );
    statement += ";";
    
    return statement;
}

long int DataStructureRetrievalDAO::retrieveNumberOfEntriesByIdentificationNumber(std::shared_ptr<dlna::db::datastructure::entity::SearchWindowDS> window) {
    std::string statement = buildNumberCountQuery(window);

    long int numberOfEntries;
    execute( statement, &numberOfEntriesCallback, &numberOfEntries );

    return numberOfEntries;
}


std::vector<dlna::db::datastructure::entity::DataStructure> DataStructureRetrievalDAO::findAllByIdentificationNumber(std::shared_ptr<dlna::db::datastructure::entity::SearchWindowDS> window, std::shared_ptr<dlna::db::datastructure::entity::SearchSortingDS> sorting) {
    std::string statement = buildAllSearchQuery(window, sorting);

    std::vector<dlna::db::datastructure::entity::DataStructure> dataStructure;
    execute( statement, &allDataStructureCallback, &dataStructure );

    return dataStructure;
}


dlna::db::datastructure::entity::DataStructure DataStructureRetrievalDAO::findByBusinessKey(long int id, long int parentId) {
    std::string statement = buildBusinessKeySearchQuery(id, parentId);

    dlna::db::datastructure::entity::DataStructure dataStructure;
    execute( statement, &dataStructureCallback, &dataStructure );

    return dataStructure;
}

} /* dao */
} /* datastructure */
} /* db */
} /* dlna */
