#include "datastructuremaintenancedao.h"
#include "changelog.h"
#include "changelogutil.h"
#include "datastructureutil.h"

#include <dlna/common/stringutil.h>

namespace dlna {
namespace db {
namespace datastructure {
namespace dao {

bool DataStructureMaintenanceDAO::update(dlna::db::datastructure::entity::DataStructure& dataStructure) {
    dlna::db::datastructure::entity::DataStructure existingDataStructure = this->m_dataStructureRetrievalDAO.findByBusinessKey( dataStructure.getID(), dataStructure.getParentId() );
    bool isAlreadyExisting = (existingDataStructure.getID() > 0) ? true : false;

    std::string typeId = (dataStructure.getTypeId() > 0) ? std::to_string( dataStructure.getTypeId() ) : "NULL";
    std::string metaInfoId = (dataStructure.getMetaInfoId() > 0) ? std::to_string( dataStructure.getMetaInfoId() ) : "NULL";

    dlna::common::util::StringUtil stringUtil(dataStructure.getName());
    std::string name = stringUtil.escapeSQL().getString();

    std::string statement;
    if (isAlreadyExisting) {
        bool areEqual = dlna::db::datastructure::entity::DataStructureUtil::areEqual(dataStructure, existingDataStructure);
        std::string changeLog = dlna::db::changelog::entity::ChangeLogUtil::performUpdateTransition( dataStructure, areEqual );

        statement = "UPDATE DATA_STRUCTURE SET ";
        statement += "NAME = \'" + name + "\'";
        statement += ", ";
        statement += "CHANGE_LOG = \'" + changeLog + "\'";
        statement += ", ";
        statement += "TYPE_ID = \'" + typeId + "\'";
        if (metaInfoId != "NULL") {
            statement += ", ";
            statement += "META_INFO_ID = \'" + metaInfoId + "\'";
        }
        statement += " ";
        statement += "WHERE ID = " + std::to_string( dataStructure.getID() ) + " ";
        statement += "AND PARENT_ID = " + std::to_string( dataStructure.getParentId() );
        statement += ";";
    } else {
        std::string changeLog = dlna::db::changelog::entity::ChangeLogUtil::performCreateTransition( dataStructure );

        statement = "INSERT INTO DATA_STRUCTURE (ID, PARENT_ID, NAME, TYPE_ID, META_INFO_ID, CHANGE_LOG) VALUES (";
        statement += std::to_string( dataStructure.getID() );
        statement += ", ";
        statement += std::to_string( dataStructure.getParentId() );
        statement += ", ";
        statement += "\'" + name + "\'";
        statement += ", ";
        statement += typeId;
        statement += ", ";
        statement += metaInfoId;
        statement += ", ";
        statement += "\'" +changeLog+ "\'";
        statement += " );";
    }

    execute(statement);
    return false;
}

} /* dao */
} /* datastructure */
} /* db */
} /* dlna */
