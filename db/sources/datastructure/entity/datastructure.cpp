#include "datastructure.h"

#include <string>

namespace dlna {
namespace db {
namespace datastructure {
namespace entity {

DataStructure::DataStructure() :
    ChangeLogEntity(dlna::db::changelog::entity::ChangeLog::INITIAL),
    m_parentId(0),
    m_name(""),
    m_typeId(0),
    m_metaInfoId(0)
    {
}

void DataStructure::setParentId(long int parentId) {
    this->m_parentId = parentId;
}

long int DataStructure::getParentId() const {
    return this->m_parentId;
}

void DataStructure::setName(std::string name) {
    this->m_name = name;
}

std::string DataStructure::getName() const {
    return this->m_name;
}

long int DataStructure::getTypeId() const {
    return this->m_typeId;
}

void DataStructure::setTypeId(long int typeId) {
    this->m_typeId = typeId;
}


long int DataStructure::getMetaInfoId() const {
    return this->m_metaInfoId;
}

void DataStructure::setMetaInfoId(long int metaInfoId) {
    this->m_metaInfoId = metaInfoId;
}

} /* entity */
} /* datastructure */
} /* db */
} /* dlna */
