#include "datastructuredto.h"

#include <string>

namespace dlna {
namespace db {
namespace datastructure {
namespace entity {

DataStructureDTO::DataStructureDTO() :
    DataTransportObject(),
    m_parentId(0),
    m_type(),
    m_metaInfo(),
    m_childCount(0) {
}


void DataStructureDTO::setParentId(long int parentId) {
    this->m_parentId = parentId;
}

long int DataStructureDTO::getParentId() const {
    return this->m_parentId;
}

void DataStructureDTO::setName(std::string name) {
    this->m_name = name;
}

std::string DataStructureDTO::getName() const {
    return this->m_name;
}

dlna::db::type::entity::TypeDTO DataStructureDTO::getType() {
    return this->m_type;
}

void DataStructureDTO::setType(dlna::db::type::entity::TypeDTO type) {
    this->m_type = type;
}


dlna::db::metainfo::entity::MetaInfoDTO DataStructureDTO::getMetaInfo() const {
    return this->m_metaInfo;
}
  
  
void DataStructureDTO::setMetaInfo(dlna::db::metainfo::entity::MetaInfoDTO metaInfo) {
    this->m_metaInfo = metaInfo;
}


void DataStructureDTO::setChildCount(long int childCount) {
    this->m_childCount = childCount;
}

long int DataStructureDTO::getChildCount() const {
    return this->m_childCount;
}

} /* entity */
} /* datastructure */
} /* db */
} /* dlna */
