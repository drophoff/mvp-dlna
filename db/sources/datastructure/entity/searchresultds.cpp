#include "searchresultds.h"

namespace dlna {
namespace db {
namespace datastructure {
namespace entity {

/**
 * \ingroup group_db
 */

SearchResultDS::SearchResultDS(std::vector<DataStructureDTO> data, long int numberOfEntries, long int containerUpdated) :
  SearchResult(data, numberOfEntries),
  m_containerUpdated(containerUpdated) {
}


long int SearchResultDS::getContainerUpdated() const {
    return this->m_containerUpdated;
}

} /* entity */
} /* datastructure */
} /* db */
} /* dlna */

