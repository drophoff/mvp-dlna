#include "searchwindow.h"

namespace dlna {
namespace db {
namespace datastructure {
namespace entity {

SearchWindowDS::SearchWindowDS(long int objectId, long int startIndex, long int requestedCount, bool itself) :
  m_objectId(objectId),
  m_startIndex(startIndex),
  m_requestedCount(requestedCount),
  m_itself(itself) {
}


long int SearchWindowDS::getObjectId() const {
  return this->m_objectId;
}

long int SearchWindowDS::getStartIndex() const {
  return this->m_startIndex;
}

long int SearchWindowDS::getRequestedCount() const {
  return this->m_requestedCount;
}

bool SearchWindowDS::isItself() const {
  return this->m_itself;
}

} /* entity */
} /* datastructure */
} /* db */
} /* dlna */

