#include "searchsorting.h"

namespace dlna {
namespace db {
namespace datastructure {
namespace entity {

SearchSortingDS::SearchSortingDS(SortOrder title) :
  m_title(title) {
}

SortOrder SearchSortingDS::getTitle() const {
  return this->m_title;
}

} /* entity */
} /* datastructure */
} /* db */
} /* dlna */

