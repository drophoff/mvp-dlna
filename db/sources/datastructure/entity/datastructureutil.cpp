#include "datastructureutil.h"

namespace dlna {
namespace db {
namespace datastructure {
namespace entity {
namespace DataStructureUtil {

bool areEqual(const dlna::db::datastructure::entity::DataStructure left, const dlna::db::datastructure::entity::DataStructure right) {
    bool areEqual = true;

    if (left.getID() != right.getID()) areEqual = false;
    if (left.getParentId() != right.getParentId()) areEqual = false;
    if (left.getName() != right.getName()) areEqual = false;
    if (left.getTypeId() != right.getTypeId()) areEqual = false;
    if (left.getMetaInfoId() != right.getMetaInfoId()) areEqual = false;

    return areEqual;
}

} /* DataStructureUtil */
} /* entity */
} /* datastructure */
} /* db */
} /* dlna */

