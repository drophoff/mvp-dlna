#include "changelogutil.h"

namespace dlna {
namespace db {
namespace changelog {
namespace entity {
namespace ChangeLogUtil {


ChangeLog valueOf(const std::string changeLog) {
    ChangeLog change = ChangeLog::INITIAL;

    if (changeLog == "C") {
        change = ChangeLog::CREATE;
    } else if (changeLog == "U") {
        change = ChangeLog::UPDATE;
    } else if (changeLog == "P") {
        change = ChangeLog::PROCESSED;
    } else if (changeLog == "V") {
        change = ChangeLog::VIRTUAL;
    }

    return change;
}


std::string toString(const ChangeLog changeLog) {
    std::string returnValue;

    switch (changeLog) {
    case ChangeLog::CREATE:
        returnValue = "C";
        break;
    case ChangeLog::UPDATE:
        returnValue = "U";
        break;
    case ChangeLog::PROCESSED:
        returnValue = "P";
        break;
    case ChangeLog::VIRTUAL:
        returnValue = "V";
        break;
    default:
        returnValue =  "I";
        break;
    }

    return returnValue;
}

std::string performCreateTransition(ChangeLogEntity& changeLog) {
    ChangeLog result;
    if (changeLog.getChangeLog() == ChangeLog::VIRTUAL) {
        result = ChangeLog::VIRTUAL;
    } else {
        result = ChangeLog::CREATE;
    }

    return toString( result );
}

std::string performUpdateTransition(ChangeLogEntity& changeLog, bool isEqual) {
    ChangeLog result = ChangeLog::INITIAL;

    if (changeLog.getChangeLog() == ChangeLog::VIRTUAL) {
      result = ChangeLog::VIRTUAL;
    }

    if (changeLog.getChangeLog() == ChangeLog::INITIAL) {
        if (isEqual) {
            result = ChangeLog::PROCESSED;
        } else {
            result = ChangeLog::UPDATE;
        }
    }

    return toString( result );
}


} /* ChangeLogUtil */
} /* logic */
} /* changelog */
} /* db */
} /* dlna */

