#include "changelogentity.h"

namespace dlna {
namespace db {
namespace changelog {
namespace entity {


ChangeLogEntity::ChangeLogEntity(ChangeLog changeLog) :
  Entity(),
  m_changeLog(changeLog) {

}


void ChangeLogEntity::setChangeLog(ChangeLog changeLog) {
    this->m_changeLog=changeLog;
}


ChangeLog ChangeLogEntity::getChangeLog() const {
    return this->m_changeLog;
}

} /* entity */
} /* changelog */
} /* db */
} /* dlna */

