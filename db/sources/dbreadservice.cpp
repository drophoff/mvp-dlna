#include "dbreadservice.h"
#include "changelogretrievaldao.h"

#include <dlna/common/constant.h>
#include <dlna/common/stringutil.h>

#include <limits>

namespace dlna {
namespace db {


dlna::db::datastructure::entity::SearchResultDS DbReadService::find(std::shared_ptr<dlna::db::datastructure::entity::SearchWindowDS> window, std::shared_ptr<dlna::db::datastructure::entity::SearchCriterionDS> criterion, std::shared_ptr<dlna::db::datastructure::entity::SearchSortingDS> sorting) {
    long int totalNumber = 0;
    std::vector<dlna::db::datastructure::entity::DataStructure> found;
    if (window) {
        totalNumber = this->m_dataStructureRetrievalDAO.retrieveNumberOfEntriesByIdentificationNumber( window );
        found = this->m_dataStructureRetrievalDAO.findAllByIdentificationNumber( window, sorting);
    }

    dlna::db::changedetection::dao::ChangeLogRetrievalDAO changeLogRetrievalDAO;
    long int updatedId = 0;

    std::vector<dlna::db::datastructure::entity::DataStructureDTO> data;
    for (unsigned int index = 0; index < found.size(); index++) {
        dlna::db::datastructure::entity::DataStructure    unmapped = found.at( index );
        dlna::db::datastructure::entity::DataStructureDTO mapped;
        dlna::common::util::StringUtil stringUtil(unmapped.getName());

        if (index == 0) {
            /**
             * Use the container update id in case it is a browse search
             * (window is valid). In case it is search request the formal
             * parameter named window is not provided and the param criterion
             * is provided and valid. Take the system update id on search requests.
             */
            if (window) {
                dlna::db::changedetection::entity::ChangeIdentifierDS changedEntry = changeLogRetrievalDAO.findChangedEntry( unmapped.getID(), unmapped.getParentId() );
                updatedId = changedEntry.getCounter();
            } else {
                std::string systemUpdateID = findPropertyByKey( dlna::common::configuration::Constant::PROPERTY_SYSTEM_UPDATE_ID );
                updatedId = std::stol( systemUpdateID ) ;
            }
        }

        mapped.setID( unmapped.getID() );
        mapped.setParentId( unmapped.getParentId() );
        mapped.setName( stringUtil.replaceXmlTags().getString() );

        dlna::db::type::entity::Type relatedType = this->m_typeRetrievalDAO.findByIdentificationNumber( unmapped.getTypeId() );
        dlna::db::type::entity::TypeDTO relatedTypeDto;
        relatedTypeDto.setID( relatedType.getID() );
        relatedTypeDto.setName( relatedType.getName() );
        relatedTypeDto.setProtocol( relatedType.getProtocol() );
        mapped.setType(relatedTypeDto);

        dlna::db::metainfo::entity::MetaInfo relatedMetaInfo = this->m_metaInfoRetrievalDAO.retrieveByIdentificationNumber( unmapped.getMetaInfoId() );
        dlna::db::metainfo::entity::MetaInfoDTO relatedMetaInfoDto;
        relatedMetaInfoDto.setID( relatedMetaInfo.getID() );
        relatedMetaInfoDto.setSize( relatedMetaInfo.getSize() );
        relatedMetaInfoDto.setDate( relatedMetaInfo.getDate() );
        mapped.setMetaInfo( relatedMetaInfoDto );

        std::shared_ptr<dlna::db::datastructure::entity::SearchWindowDS> countSubChildWindow = std::shared_ptr<dlna::db::datastructure::entity::SearchWindowDS>(new dlna::db::datastructure::entity::SearchWindowDS(unmapped.getID(), 0, std::numeric_limits<long int>::max() - 1, false));
        long int numberOfChilds = this->m_dataStructureRetrievalDAO.retrieveNumberOfEntriesByIdentificationNumber( countSubChildWindow );
        mapped.setChildCount( numberOfChilds );

        data.push_back( mapped );
    }

    dlna::db::datastructure::entity::SearchResultDS result(data, totalNumber, updatedId);

    return result;
}


std::string DbReadService::findFilenameById(long int id) {
    dlna::db::metainfo::entity::MetaInfo relatedMetaInfo = this->m_metaInfoRetrievalDAO.retrieveByIdentificationNumber( id );
    return relatedMetaInfo.getPath();
}


std::string DbReadService::findPropertyByKey(const std::string key) {
    dlna::db::property::entity::Property property = this->m_propertyRetrievalDAO.findByName( key );
    return property.getValue();
}


} /* db */
} /* dlna */
