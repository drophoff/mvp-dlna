# Introduction

This application provides a minimal content directoy server that is not certified and does not meet the full DLNA specification.
The application has been created to show the usage of the underlaying libsqlite and libupnp libraries.
The goal was never to implement a full blown DLNA-compliant server (Media Server).
The project has a more protypical character.
Therefore the usage of complex third party libraries like boost are avoided as much as possible in case the implementation effort is not to high.

## Documentation

An official documentation of this project can be found [here](https://drophoff.gitlab.io/mvp-dlna).
There you will find a description of how the project can be compiled and what requirements must be met for it.
A short installation guide can also be found there.

This [page](https://drophoff.gitlab.io/mvp-dlna/pages.html) gives an overview of existing sub-pages on which internal topics are described.
The topics cover design decisions, the structure of the database, the currently supported features or an overview of the development history.
A description of the permitted configuration settings can also be found there.

The application consists of loosely coupled independent modules.
The respective modules and their areas of responsibility are documented [here](https://drophoff.gitlab.io/mvp-dlna/modules.html).
