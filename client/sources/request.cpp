#include "request.h"

namespace dlna {
namespace client {

Request::Request(const dlna::client::store::Device& device) :
    Communication(device),
    m_ObjectID(0),
    m_Data(true),
    m_StartingIndex(0),
    m_RequestedCount(10) {
}

long int Request::getObjectID() const {
    return this->m_ObjectID;
}

void Request::setObjectID(long int objectID) {
    this->m_ObjectID = objectID;
}

bool Request::isData() const {
    return this->m_Data;
}

void Request::setData(bool data) {
    this->m_Data = data;
}

long int Request::getStartingIndex() const {
    return this->m_StartingIndex;
}

void Request::setStartingIndex(long int start) {
    this->m_StartingIndex = start;
}

long int Request::getRequestedCount() const {
    return this->m_RequestedCount;
}

void Request::setRequestedCount(long int count) {
    this->m_RequestedCount = count;
}

} /* client */
} /* dlna */
