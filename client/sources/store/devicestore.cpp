#include "devicestore.h"

namespace dlna {
namespace client {
namespace store {


DeviceStore::DeviceStore() {
}


DeviceStore::~DeviceStore() {
}


void DeviceStore::update(const std::string& uniqueDeviceNumber, const Device& device) {
    std::lock_guard<std::mutex> lockGuard( this->m_Mutex );

    try {
        this->m_Map.at( uniqueDeviceNumber ) = device;
    } catch (const std::out_of_range& oor) {
        std::pair<std::string, Device> pair( uniqueDeviceNumber, device);
        this->m_Map.insert( pair );
    }
}


void DeviceStore::remove(const std::string& uniqueDeviceNumber) {
    std::lock_guard<std::mutex> lockGuard( this->m_Mutex );

    this->m_Map.erase( uniqueDeviceNumber );
}

const std::unordered_map<std::string, Device> DeviceStore::getStore() {
    std::lock_guard<std::mutex> lockGuard( this->m_Mutex );

    return this->m_Map;
}


void DeviceStore::removeExpiredDevices() {
    std::lock_guard<std::mutex> lockGuard( this->m_Mutex );

    std::time_t timestamp;
    std::time( &timestamp );

    auto iter = this->m_Map.begin();
    while (iter != this->m_Map.end()) {
        const Device device = iter->second;

        double difference = std::difftime( device.getExpire(), timestamp );
        if (difference <= 0) {
            iter = this->m_Map.erase( iter );
        } else {
            iter++;
        }
    }
}

} /* store */
} /* client */
} /* dlna */
