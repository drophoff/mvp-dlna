#include "device.h"

namespace dlna {
namespace client {
namespace store {

Device::Device(std::string name, std::string actionURL, std::string serviceType, std::string udn, std::time_t expire) :
    m_Name(name),
    m_ActionURL(actionURL),
    m_ServiceType(serviceType),
    m_udn(udn),
    m_Expire(expire) {
}

std::string Device::getName() const {
    return this->m_Name;
}

std::string Device::getActionURL() const {
    return this->m_ActionURL;
}

std::string Device::getServiceType() const {
    return this->m_ServiceType;
}

std::string Device::getUniqueDeviceNumber() const {
    return this->m_udn;
}

std::time_t Device::getExpire() const {
    return this->m_Expire;
}

} /* store */
} /* client */
} /* dlna */
