#include "communication.h"

namespace dlna {
namespace client {

Communication::Communication(const dlna::client::store::Device& device) :
    m_Device( device ) {
}

const dlna::client::store::Device& Communication::getDevice() const {
    return this->m_Device;
}

} /* client */
} /* dlna */
