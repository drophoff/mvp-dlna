#include "xmldescriptionparser.h"

#include <dlna/common/xmlutil.h>

namespace dlna {
namespace client {
namespace xml {

XmlDescriptionParser::XmlDescriptionParser(IXML_Document* document) :
    XmlAbstractParser<XmlDescriptionResult>(document),
    m_ContentDirectoryFound(false) {
}

XmlDescriptionParser::~XmlDescriptionParser() {
    ixmlDocument_free( this->m_document );
}

void XmlDescriptionParser::handleElement(IXML_Node* node) {
    if (getElementName() == "deviceList") {
        // Parsing new embedded device
        if (!isContentDirectory()) {
            this->m_T.setFriendlyName( "" );
            this->m_T.setUniqueDeviceNumber( "" );
            this->m_T.setServiceID( "" );
            this->m_T.setServiceType( "" );
            this->m_T.setControlURL( "" );
        }
    }
}

void XmlDescriptionParser::handleElementValue(std::string value) {
    if (getElementName() == "serviceType") {
        if (value.find("ContentDirectory") != std::string::npos) {
            setContentDirectory( true );
        }
        this->m_T.setServiceType( value );
    }

    if (getElementName() == "serviceId") {
        this->m_T.setServiceID( value );
    }

    if (getElementName() == "UDN") {
        this->m_T.setUniqueDeviceNumber( value );
    }

    if (getElementName() == "friendlyName") {
        this->m_T.setFriendlyName( value );
    }

    if (getElementName() == "controlURL") {
        this->m_T.setControlURL( value );
    }
}

bool XmlDescriptionParser::isContentDirectory() const {
    return this->m_ContentDirectoryFound;
}

void XmlDescriptionParser::setContentDirectory(bool found) {
    this->m_ContentDirectoryFound = found;
}

bool XmlDescriptionParser::handleResult() {
    if (!isContentDirectory()) {
        this->m_T.setFriendlyName( "" );
        this->m_T.setUniqueDeviceNumber( "" );
        this->m_T.setServiceID( "" );
        this->m_T.setServiceType( "" );
        this->m_T.setControlURL( "" );
    }

    return isContentDirectory();
}

} /* xml */
} /* client */
} /* dlna */
