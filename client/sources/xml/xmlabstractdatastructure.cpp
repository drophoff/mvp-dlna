#include "xmlabstractdatastructure.h"

namespace dlna {
namespace client {
namespace xml {

XmlAbstractDataStructure::XmlAbstractDataStructure() :
    m_ObjectID(0),
    m_ParentID(0),
    m_Title("") {
}

XmlAbstractDataStructure::~XmlAbstractDataStructure() {
}

long int XmlAbstractDataStructure::getObjectID() const {
    return this->m_ObjectID;
}

void XmlAbstractDataStructure::setObjectID(long int id) {
     this->m_ObjectID = id;
}

long int XmlAbstractDataStructure::getParentID() const {
    return this->m_ParentID;
}

void XmlAbstractDataStructure::setParentID(long int parentID) {
    this->m_ParentID = parentID;
}

std::string XmlAbstractDataStructure::getTitle() const {
    return this->m_Title;
}

void XmlAbstractDataStructure::setTitle(std::string title) {
    this->m_Title = title;
}

} /* xml */
} /* client */
} /* dlna */
