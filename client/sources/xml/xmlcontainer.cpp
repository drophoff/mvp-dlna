#include "xmlcontainer.h"

namespace dlna {
namespace client {
namespace xml {

XmlContainer::XmlContainer() :
    XmlAbstractDataStructure(),
    m_ChildCount(0) {
}

XmlContainer::~XmlContainer() {
}

long int XmlContainer::getChildCount() const {
    return this->m_ChildCount;
}

void XmlContainer::setChildCount(long int childCount) {
    this->m_ChildCount = childCount;
}

std::string XmlContainer::getType() const {
    return "CNT";
}

} /* xml */
} /* client */
} /* dlna */
