#include "xmlbrowseparser.h"
#include "xmlcontainer.h"
#include "xmlitem.h"


#include <cstdlib>

namespace dlna {
namespace client {
namespace xml {

XmlBrowseParser::XmlBrowseParser(IXML_Document* document) :
    XmlAbstractParser<XmlBrowseResult>(document),
    m_Parsing(nullptr) {
}

XmlBrowseParser::~XmlBrowseParser() {
    ixmlDocument_free( this->m_document);
    /**
     * m_Parsing will be released/handled via the returned vector due
     * to this no delete necessary!
     */
}

void XmlBrowseParser::handleElement(IXML_Node* node) {
    if (getElementName() == "container") {
        m_Parsing = new XmlContainer();
    } else if (getElementName() == "item") {
        m_Parsing = new XmlItem();
    }

    if (getElementName() == "container" || getElementName() == "item") {
        this->m_T.add( m_Parsing );
    }

    IXML_NamedNodeMap* attributes = ixmlNode_getAttributes( node );
    if (attributes != nullptr) {
        for (unsigned long index = 0; index < ixmlNamedNodeMap_getLength( attributes ); index++) {
            IXML_Node* attrNode = ixmlNamedNodeMap_item( attributes, index );
            if (attrNode != nullptr) {
                const DOMString attrName = ixmlNode_getNodeName( attrNode );
                std::string attrNameStr(attrName);

                const DOMString childAttributeValue = ixmlNode_getNodeValue( attrNode );
                std::string attrValueStr(childAttributeValue);

                if (m_Parsing != nullptr) {
                    if (attrNameStr == "id") {
                        long int id = std::atol( childAttributeValue );
                        m_Parsing->setObjectID(id);
                    }

                    if (attrNameStr == "parentID") {
                        long int parentID = std::atol( childAttributeValue );
                        m_Parsing->setParentID( parentID );
                    }

                    if (attrNameStr == "childCount") {
                        long int childCount = std::atol( childAttributeValue );
                        ((XmlContainer*)m_Parsing)->setChildCount( childCount );
                    }
                    if (attrNameStr == "protocolInfo") {
                        ((XmlItem*)m_Parsing)->setProtocol( attrValueStr );
                    }
                }
            }
        }
        ixmlNamedNodeMap_free( attributes );
    }
}

void XmlBrowseParser::handleElementValue(std::string value) {
    if (getElementName() == "NumberReturned") {
        long int numberReturned = std::atol( value.c_str() );
        this->m_T.setNumberReturned( numberReturned );
    }

    if (getElementName() == "TotalMatches") {
        long int totalMatches = std::atol( value.c_str() );
        this->m_T.setTotalMatches( totalMatches );
    }

    if (getElementName() == "UpdateID") {
        long int updateID = std::atol( value.c_str() );
        this->m_T.setUpdateID( updateID );
    }

    if (getElementName() == "date") {
        ((XmlItem*)m_Parsing)->setDate( value );
    }

    if (getElementName() == "title" && m_Parsing != nullptr) {
        m_Parsing->setTitle( value );
    }

    if (getElementName() == "res") {
        ((XmlItem*)m_Parsing)->setResourceURI( value );
    }
}

bool XmlBrowseParser::handleResult() {
    return true;
}

} /* xml */
} /* client */
} /* dlna */
