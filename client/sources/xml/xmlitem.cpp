#include "xmlitem.h"

namespace dlna {
namespace client {
namespace xml {

XmlItem::XmlItem() : 
    XmlAbstractDataStructure(),
    m_ResourceURI(""),
    m_Date(""),
    m_Protocol("") {
}

XmlItem::~XmlItem() {
}

std::string XmlItem::getResourceURI() const {
    return this->m_ResourceURI;
}

void XmlItem::setResourceURI(std::string resource) {
    this->m_ResourceURI = resource;
}

std::string XmlItem::getType() const {
    return "ITM";
}

void XmlItem::setDate(std::string date) {
    this->m_Date = date;
}

std::string XmlItem::getDate() const {
    return this->m_Date;
}

void XmlItem::setProtocol(std::string protocol) {
    this->m_Protocol = protocol;
}

std::string XmlItem::getProtocol() const {
    return this->m_Protocol;
}

} /* xml */
} /* client */
} /* dlna */
