#include "xmldescriptionresult.h"

namespace dlna {
namespace client {
namespace xml {

XmlDescriptionResult::XmlDescriptionResult() :
    m_friendlyName(""),
    m_udn(""),
    m_ServiceID(""),
    m_ServiceType(""),
    m_ControlURL("") {
}

std::string XmlDescriptionResult::getFriendlyName() const {
    return this->m_friendlyName;
}

void XmlDescriptionResult::setFriendlyName(std::string friendlyName) {
    this->m_friendlyName = friendlyName;
}

std::string XmlDescriptionResult::getUniqueDeviceNumber() const {
    return this->m_udn;
}

void XmlDescriptionResult::setUniqueDeviceNumber(std::string udn) {
    this->m_udn = udn;
}

void XmlDescriptionResult::setServiceID(std::string serviceID) {
    this->m_ServiceID = serviceID;
}

std::string XmlDescriptionResult::getServiceID() const {
    return this->m_ServiceID;
}

void XmlDescriptionResult::setServiceType(std::string serviceType) {
    this->m_ServiceType = serviceType;
}

std::string XmlDescriptionResult::getServiceType() const {
    return this->m_ServiceType;
}

void XmlDescriptionResult::setControlURL(std::string controlURL) {
    this->m_ControlURL = controlURL;
}

std::string XmlDescriptionResult::getControlURL() const {
    return this->m_ControlURL;
}

} /* xml */
} /* client */
} /* dlna */
