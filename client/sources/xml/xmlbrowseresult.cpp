#include "xmlbrowseresult.h"

namespace dlna {
namespace client {
namespace xml {

XmlBrowseResult::XmlBrowseResult() {
}

long int XmlBrowseResult::getNumberReturned() const {
    return this->m_NumberReturned;
}

void XmlBrowseResult::setNumberReturned(long int numberReturned) {
    this->m_NumberReturned = numberReturned;
}

long int XmlBrowseResult::getTotalMatches() const {
    return this->m_TotalMatches;
}

void XmlBrowseResult::setTotalMatches(long int totalMatches) {
    this->m_TotalMatches = totalMatches;
}

long int XmlBrowseResult::getUpdateID() const {
    return this->m_UpdateID;
}

void XmlBrowseResult::setUpdateID(long int updateID) {
    this->m_UpdateID = updateID;
}


void XmlBrowseResult::add(XmlAbstractDataStructure* dataStructure) {
    std::shared_ptr<XmlAbstractDataStructure> dataStructureSharedPtr(dataStructure);
    this->m_DataStructure.push_back( dataStructureSharedPtr );
}

const std::vector<std::shared_ptr<XmlAbstractDataStructure>>& XmlBrowseResult::getDataStructure() const {
    return this->m_DataStructure;
}

} /* xml */
} /* client */
} /* dlna */
