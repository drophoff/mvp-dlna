#include "searchtimeoutrequest.h"

#include <dlna/common/logutil.h>


namespace dlna {
namespace client {
namespace request {

bool SearchTimeoutRequest::isResponsibleFor(Upnp_EventType event) {
   return (event == UPNP_DISCOVERY_SEARCH_TIMEOUT) ? true : false;
}

void SearchTimeoutRequest::handle(void* event) {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::TRACE, "Handle Request: Search Timeout");

    /**
     * Nothing to do, this event handler exists only to handle this
     * kind of events and to avoid "unknown event" message, which would
     * be created in case this class is not registered as a known event
     * at the event handler callback.
     */
}

} /* request */
} /* client */
} /* dlna */
