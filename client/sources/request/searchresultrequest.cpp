#include "searchresultrequest.h"
#include "devicehandler.h"

#include <dlna/common/logutil.h>


namespace dlna {
namespace client {
namespace request {

bool SearchResultRequest::isResponsibleFor(Upnp_EventType event) {
   return (event == UPNP_DISCOVERY_SEARCH_RESULT) ? true : false;
}

void SearchResultRequest::handle(void* event) {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::TRACE, "Handle Request: Search Result");

    struct Upnp_Discovery* discovery = (struct Upnp_Discovery*) event;

    DeviceHandler deviceHandler;
    deviceHandler.handleFoundDevice( discovery );
}

} /* request */
} /* client */
} /* dlna */
