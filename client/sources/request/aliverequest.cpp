#include "aliverequest.h"
#include "devicehandler.h"

#include <dlna/common/logutil.h>

namespace dlna {
namespace client {
namespace request {

bool AliveRequest::isResponsibleFor(Upnp_EventType event) {
   return (event == UPNP_DISCOVERY_ADVERTISEMENT_ALIVE) ? true : false;
}

void AliveRequest::handle(void* event) {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::TRACE, "Handle Request: Advertisment");

    struct Upnp_Discovery* discovery = (struct Upnp_Discovery*) event;

    DeviceHandler deviceHandler;
    deviceHandler.handleFoundDevice( discovery );
}

} /* request */
} /* client */
} /* dlna */
