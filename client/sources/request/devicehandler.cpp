#include "devicehandler.h"
#include "devicestore.h"
#include "device.h"
#include "xmldescriptionparser.h"

#include <dlna/common/logutil.h>

#include <string>
#include <ctime>

namespace dlna {
namespace client {
namespace request {


void DeviceHandler::handleFoundDevice(struct Upnp_Discovery* discovery) {
    if (isDiscoveryValid( discovery )) {
        IXML_Document* xmlDoc = nullptr;
        int ret = UpnpDownloadXmlDoc(discovery->Location, &xmlDoc);
        if ( ret != UPNP_E_SUCCESS  ) {
            dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::WARN, "Download service description failed!");
        } else {
            // Parse the description of the remote controlled device.
            dlna::client::xml::XmlDescriptionParser docParser( xmlDoc );
            const dlna::client::xml::XmlDescriptionResult * const descriptionResult = docParser.parse();
            if (descriptionResult != nullptr) {
                // Calulate the expire time
                std::time_t timestamp;
                std::time( &timestamp );
                timestamp += discovery->Expires;

                // Determine the formerly known (UPnP 1.0) baseURL.
                std::string baseURL(discovery->Location);
                std::size_t pos = baseURL.find( "/", 8 );
                if ( pos != std::string::npos) {
                    baseURL = baseURL.substr(0, pos);
                }

                /**
                 * Store the relevant parameter from the found content directory
                 * service providing controlled device within an internal store.
                 *
                 * The key is the unique device number. The stored parameter are
                 * needed to communicate with the controlled device (browse).
                 */
                const dlna::client::store::Device device( descriptionResult->getFriendlyName(),
                                                          baseURL + descriptionResult->getControlURL(),
                                                          descriptionResult->getServiceType(),
                                                          descriptionResult->getUniqueDeviceNumber(),
                                                          timestamp );
                dlna::client::store::DeviceStore::getInstance().update( descriptionResult->getUniqueDeviceNumber(), device );
            }
        }
    }
}

void DeviceHandler::handleShutdownDevice(struct Upnp_Discovery* discovery) {
    if (isDiscoveryValid( discovery )) {
        const std::string uniqueDeviceNumber(discovery->DeviceId);

        dlna::client::store::DeviceStore::getInstance().remove( uniqueDeviceNumber );
    }
}

bool DeviceHandler::isDiscoveryValid(struct Upnp_Discovery* discovery) {
    bool isValid = false;

    if (discovery != nullptr && discovery->ErrCode == UPNP_E_SUCCESS) {
        std::string serviceType(discovery->ServiceType);
        if (serviceType.find("ContentDirectory") != std::string::npos) {
            isValid = true;
        }
    }

    return isValid;
}

} /* request */
} /* client */
} /* dlna */
