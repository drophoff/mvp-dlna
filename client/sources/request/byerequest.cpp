#include "byerequest.h"
#include "devicehandler.h"

#include <dlna/common/logutil.h>


namespace dlna {
namespace client {
namespace request {

bool ByeRequest::isResponsibleFor(Upnp_EventType event) {
   return (event == UPNP_DISCOVERY_ADVERTISEMENT_BYEBYE) ? true : false;
}

void ByeRequest::handle(void* event) {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::TRACE, "Handle Request: Bye Bye");

    struct Upnp_Discovery* discovery = (struct Upnp_Discovery*) event;

    DeviceHandler deviceHandler;
    deviceHandler.handleShutdownDevice( discovery );
}

} /* request */
} /* client */
} /* dlna */
