#include "response.h"

namespace dlna {
namespace client {

Response::Response(const dlna::client::store::Device& device) :
     Communication(device) {
}

void Response::setResult(const dlna::client::xml::XmlBrowseResult& data) {
    this->m_Data = data;
}

const dlna::client::xml::XmlBrowseResult& Response::getResult() const {
   return this->m_Data;
}

} /* client */
} /* dlna */
