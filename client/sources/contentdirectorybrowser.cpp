#include "contentdirectorybrowser.h"
#include "devicestore.h"
#include "xmlbrowseparser.h"
#include "xmlcontainer.h"
#include "xmlitem.h"

#include <dlna/common/deviceprovider.h>
#include <dlna/common/logutil.h>
#include <dlna/common/fatalexception.h>
#include <dlna/common/xmlutil.h>

#include <sys/wait.h>
#include <upnp/upnp.h>
#include <string>

namespace dlna {
namespace client {

ContentDirectoryBrowser::ContentDirectoryBrowser() {
    this->m_upnpSubsystem = nullptr;
}

ContentDirectoryBrowser::~ContentDirectoryBrowser() {
     if (this->m_upnpSubsystem != nullptr) {
        delete this->m_upnpSubsystem;
        this->m_upnpSubsystem = nullptr;
     }
}


void ContentDirectoryBrowser::initialize() {
    if (this->m_upnpSubsystem == nullptr) {
        this->m_upnpSubsystem = new dlna::client::upnp::Client();

        this->m_upnpSubsystem->searchAll();
    }
}


const std::vector<Response> ContentDirectoryBrowser::retrieveDeviceList() const {
    std::vector<Response> deviceList;

    if (this->m_upnpSubsystem != nullptr) {
        dlna::client::store::DeviceStore::getInstance().removeExpiredDevices();
        const std::unordered_map<std::string, dlna::client::store::Device> registeredDevices = dlna::client::store::DeviceStore::getInstance().getStore();
        for (auto iter = registeredDevices.cbegin(); iter != registeredDevices.cend(); iter++) {
            const dlna::client::store::Device device = iter->second;
            // Retrieve Metadata from the device for his root folder
            Request tmpRequest( device );
            tmpRequest.setObjectID( 0 );
            tmpRequest.setData( false );

            Response tmpResponse = browse( tmpRequest );

            // Change the root folder name to his own device name
            if (tmpResponse.getResult().getDataStructure().size() > 0) {
                std::shared_ptr<dlna::client::xml::XmlAbstractDataStructure> rootDataStructure = tmpResponse.getResult().getDataStructure().at( 0 );
                rootDataStructure->setTitle( device.getName() );
            }

            deviceList.push_back( tmpResponse );
        }
    } else {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::WARN, "UPnP subsystem is not initialized!");
    }

    return deviceList;
}


const Response ContentDirectoryBrowser::browse(const Request& request) const {
    Response response( request.getDevice() );

    if (this->m_upnpSubsystem != nullptr) {
        IXML_Document* requestDoc = createBrowseRequest( request );
        if (requestDoc != nullptr) {
            IXML_Document* responseDoc = sendBrowseRequest( requestDoc, request.getDevice() );
            if (responseDoc != nullptr) {
                dlna::client::xml::XmlBrowseParser parser( responseDoc );
                response.setResult( *parser.parse() );
            }
            ixmlDocument_free( requestDoc );
        }
    } else {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::WARN, "UPnP subsystem is not initialized!");
    }

    return response;
}


IXML_Document* ContentDirectoryBrowser::createBrowseRequest(const Request& request) const {
    IXML_Document* requestDoc = nullptr;

    std::string documentElement("Browse");
    std::string objectElement("ObjectID");
    std::string objectValue( std::to_string( request.getObjectID() ) );
    std::string browseFlagElement("BrowseFlag");
    std::string browseFlagValue = (request.isData()) ? "BrowseDirectChildren" : "BrowseMetadata";
    std::string filterElement("Filter");
    std::string filterValue("");
    std::string startIndexElement("StartingIndex");
    std::string startIndexValue( std::to_string( request.getStartingIndex() )  );
    std::string requestCountElement("RequestedCount");
    std::string requestCountValue( std::to_string( request.getRequestedCount() ) );
    std::string sortCriteriaElement("SortCriteria");
    std::string sortCriteriaValue("");

    requestDoc = UpnpMakeAction(documentElement.c_str(), request.getDevice().getServiceType().c_str(), 0, nullptr);

    if (requestDoc != nullptr) {
        UpnpAddToAction(&requestDoc, documentElement.c_str(), request.getDevice().getServiceType().c_str(), objectElement.c_str(), objectValue.c_str());
        UpnpAddToAction(&requestDoc, documentElement.c_str(), request.getDevice().getServiceType().c_str(), browseFlagElement.c_str(), browseFlagValue.c_str());
        UpnpAddToAction(&requestDoc, documentElement.c_str(), request.getDevice().getServiceType().c_str(), filterElement.c_str(), filterValue.c_str());
        UpnpAddToAction(&requestDoc, documentElement.c_str(), request.getDevice().getServiceType().c_str(), startIndexElement.c_str(), startIndexValue.c_str());
        UpnpAddToAction(&requestDoc, documentElement.c_str(), request.getDevice().getServiceType().c_str(), requestCountElement.c_str(), requestCountValue.c_str());
        UpnpAddToAction(&requestDoc, documentElement.c_str(), request.getDevice().getServiceType().c_str(), sortCriteriaElement.c_str(), sortCriteriaValue.c_str());

        dlna::common::xml::XmlUtil::log(requestDoc);
    } else {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Creating browse request failed!");
    }

    return requestDoc;
}


IXML_Document* ContentDirectoryBrowser::sendBrowseRequest(IXML_Document* requestDoc, const dlna::client::store::Device& device) const {
    IXML_Document* responseDoc = nullptr;
    int ret = UpnpSendAction(*dlna::common::http::DeviceProvider::getInstance().getDevice(), device.getActionURL().c_str(), device.getServiceType().c_str(), device.getUniqueDeviceNumber().c_str(), requestDoc, &responseDoc);
    if (ret == UPNP_E_SUCCESS) {
        dlna::common::xml::XmlUtil::log(responseDoc);
    } else {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Sending browse request failed! ");
    }

    return (ret == UPNP_E_SUCCESS) ? responseDoc: nullptr;
}


} /* client */
} /* dlna */