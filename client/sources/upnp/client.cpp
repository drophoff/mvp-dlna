#include "client.h"
#include "aliverequest.h"
#include "byerequest.h"
#include "searchresultrequest.h"
#include "searchtimeoutrequest.h"

#include <dlna/common/deviceprovider.h>
#include <dlna/common/eventcallbackhandler.h>
#include <dlna/common/constant.h>
#include <dlna/common/logutil.h>
#include <dlna/common/fatalexception.h>
#include <dlna/common/configreader.h>

#include <memory>


namespace dlna {
namespace client {
namespace upnp {


Client::Client() {
    /**
     * Initialize the UPnP subsystem
     */
    dlna::common::configuration::Configuration config = dlna::common::configuration::ConfigReader::retrieve();
    int ret = UpnpInit(config.getIP().c_str(), config.getPort());
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::FATAL, "Cannot initialize UPnP subsystem");

        throw dlna::common::exception::FatalException();
    }

    ret = UpnpSetMaxContentLength(dlna::common::configuration::Constant::MAX_CONTENT_LENGTH);
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::WARN, "Cannot increase max content size!");
    }

    typedef std::shared_ptr<dlna::common::http::EventHandler> eventHandler;
    dlna::common::http::EventCallbackHandler::registerEventHandler( eventHandler( new dlna::client::request::AliveRequest() ));
    dlna::common::http::EventCallbackHandler::registerEventHandler( eventHandler( new dlna::client::request::ByeRequest() ));
    dlna::common::http::EventCallbackHandler::registerEventHandler( eventHandler( new dlna::client::request::SearchResultRequest() ));
    dlna::common::http::EventCallbackHandler::registerEventHandler( eventHandler( new dlna::client::request::SearchTimeoutRequest() ));

    ret = UpnpRegisterClient( dlna::common::http::EventCallbackHandler::handleEvent, nullptr, dlna::common::http::DeviceProvider::getInstance().getDevice());
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::FATAL, "Cannot register client device");

        throw dlna::common::exception::FatalException();
    }

    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Internal UPnP subsystem initialized");
}


Client::~Client() {
    int ret = UpnpUnRegisterClient(*dlna::common::http::DeviceProvider::getInstance().getDevice());
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Cannot unregister client device");
    }

    ret = UpnpFinish();
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Cannot stop UPnP subsystem");
    }

    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Internal UPnP subsystem stopped");
}


void Client::searchAll() {
    const char searchType[9] = { 's', 's', 'd', 'p', ':', 'a', 'l', 'l', '\0' };
    int ret = UpnpSearchAsync( *dlna::common::http::DeviceProvider::getInstance().getDevice(), 3, searchType, nullptr );
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Search for all services and devices failed!");
    }
}

} /* upnp */
} /* client */
} /* dlna */
