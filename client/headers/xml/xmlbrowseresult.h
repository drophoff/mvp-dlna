#ifndef XML_BROWSE_RESPONSE_H
#define XML_BROWSE_RESPONSE_H

#include "xmlabstractdatastructure.h"

#include <string>
#include <vector>
#include <memory>

namespace dlna {
namespace client {
namespace xml {

/**
 * \file xmlbrowseresult.h
 */


class XmlBrowseResult {
public:
  /**
   * Default constructor
   */
  XmlBrowseResult();

  long int getNumberReturned() const;

  void setNumberReturned(long int number);

  long int getTotalMatches() const;

  void setTotalMatches(long int totalMatches);

  long int getUpdateID() const;

  void setUpdateID(long int updateID);

  void add(XmlAbstractDataStructure* dataStructure);

  const std::vector<std::shared_ptr<XmlAbstractDataStructure>>& getDataStructure() const;

private:
  long int m_NumberReturned;

  long int m_TotalMatches;

  long int m_UpdateID;

  std::vector<std::shared_ptr<XmlAbstractDataStructure>> m_DataStructure;

};

} /* xml */
} /* client */
} /* dlna */
#endif /* XML_BROWSE_RESPONSE_H */
