#ifndef XML_DESCRIPTION_PARSER_H
#define XML_DESCRIPTION_PARSER_H

#include <dlna/common/xmlabstractparser.h>

#include "xmldescriptionresult.h"


namespace dlna {
namespace client {
namespace xml {

/**
 * \file xmldescriptionparser.h
 */

/**
 * \brief This class provides functionality to parse the parameter
 * of a downloaded description 'Document (IXML)' that describes
 * a remote controlled device (aka server).
 *
 * The description itself consists of a root device with a set
 * of properties like name, model and serial number and an optional
 * service list (connection manager, content directory).
 *
 * Each root device can have several sub devices called embedded
 * devices that have the same attributes as the root device.
 *
 * This parser is restricted to parse only the parameter from
 * a root or embedded devices that has provides the content
 * directory service.
 */
class XmlDescriptionParser : public dlna::common::xml::XmlAbstractParser<XmlDescriptionResult> {
public:
  /**
   * Default constructor
   */
  XmlDescriptionParser(IXML_Document* document);

  /**
   * Default destructor
   */
  virtual ~XmlDescriptionParser();

protected:
  bool isContentDirectory() const;

  void setContentDirectory(bool found);

  bool m_ContentDirectoryFound;

private:
  void handleElement(IXML_Node* node);

  void handleElementValue(std::string value);

  bool handleResult();
};

} /* xml */
} /* client */
} /* dlna */
#endif /* XML_DESCRIPTION_PARSER_H */
