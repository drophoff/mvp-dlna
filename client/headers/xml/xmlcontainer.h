#ifndef XML_CONTAINER_OBJECT_H
#define XML_CONTAINER_OBJECT_H

#include "xmlabstractdatastructure.h"

namespace dlna {
namespace client {
namespace xml {

/**
 * \file xmlcontainer.h
 */


class XmlContainer : public XmlAbstractDataStructure {
public:
  /**
   * Default constructor
   */
  XmlContainer();

  /**
   * Default destructor
   */
  virtual ~XmlContainer();

  long int getChildCount() const;

  void setChildCount(long int childCount);

  std::string getType() const;

private:
  long int m_ChildCount;
};

} /* xml */
} /* client */
} /* dlna */
#endif /* XML_CONTAINER_OBJECT_H */
