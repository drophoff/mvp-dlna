#ifndef XML_ITEM_OBJECT_H
#define XML_ITEM_OBJECT_H

#include "xmlabstractdatastructure.h"

namespace dlna {
namespace client {
namespace xml {

/**
 * \file xmlitem.h
 */


class XmlItem : public XmlAbstractDataStructure {
public:
  /**
   * Default constructor
   */
  XmlItem();

  virtual ~XmlItem();

  std::string getResourceURI() const;

  void setResourceURI(std::string resource);

  std::string getType() const;

  void setDate(std::string date);

  std::string getDate() const;

  void setProtocol(std::string protocol);

  std::string getProtocol() const;

private:
   std::string m_ResourceURI;

   std::string m_Date;

   std::string m_Protocol;
};

} /* xml */
} /* client */
} /* dlna */
#endif /* XML_ITEM_OBJECT_H */
