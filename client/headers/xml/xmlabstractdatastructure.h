#ifndef XML_ABSTRACT_DATA_STRUCTURE_H
#define XML_ABSTRACT_DATA_STRUCTURE_H

#include <string>

namespace dlna {
namespace client {
namespace xml {

/**
 * \file xmlabstractdatastructure.h
 */


class XmlAbstractDataStructure {
public:
  /**
   * Default constructor
   */
  XmlAbstractDataStructure();

  /**
   * Default destructor
   */
  virtual ~XmlAbstractDataStructure();

  long int getObjectID() const;

  void setObjectID(long int id);

  long int getParentID() const;

  void setParentID(long int parentID);

  std::string getTitle() const;

  void setTitle(std::string title);

  std::string virtual getType() const = 0;

private:
  long int m_ObjectID;

  long int m_ParentID;

  std::string m_Title;
};

} /* xml */
} /* client */
} /* dlna */
#endif /* XML_ABSTRACT_DATA_STRUCTURE_H */
