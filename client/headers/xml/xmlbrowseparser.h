#ifndef XML_BROWSE_RESPONSE_PARSER_H
#define XML_BROWSE_RESPONSE_PARSER_H

#include "xmlbrowseresult.h"

#include <dlna/common/xmlabstractparser.h>


namespace dlna {
namespace client {
namespace xml {

/**
 * \file xmlbrowseparser.h
 */

/**
 * This class provides functionality to parse the response of a browse
 * request.
 */
class XmlBrowseParser : public dlna::common::xml::XmlAbstractParser<XmlBrowseResult> {
public:
  /**
   * Default constructor
   */
  XmlBrowseParser(IXML_Document* document);

  /**
   * Default destructor
   */
  virtual ~XmlBrowseParser();

private:
  XmlAbstractDataStructure* m_Parsing;

  void handleElement(IXML_Node* node);

  void handleElementValue(std::string value);

  bool handleResult();
};

} /* xml */
} /* client */
} /* dlna */
#endif /* XML_BROWSE_RESPONSE_PARSER_H */
