#ifndef XML_DESCRIPTION_RESULT_H
#define XML_DESCRIPTION_RESULT_H

#include <string>

namespace dlna {
namespace client {
namespace xml {

/**
 * \file xmldescriptionresult.h
 */


/**
 * This class contains the relevant paramters from the controlled device description
 * provided in form of the an IXML document.
 */
class XmlDescriptionResult {
public:
  /**
   * Default constructor
   */
  XmlDescriptionResult();

  /**
   * Returns the friendly name.
   */
  std::string getFriendlyName() const;

  /**
   * Used to set the friendly name.
   */
  void setFriendlyName(std::string friendlyName);

  /**
   * Returns the unique device number.
   */
   std::string getUniqueDeviceNumber() const;

   /**
    * Used to set the unique device number.
    */
   void setUniqueDeviceNumber(std::string udn);

   /**
    * Returns the service id.
    */
   std::string getServiceID() const;

   /**
    * Used to set the service id.
    */
   void setServiceID(std::string serviceID);

   /**
    * Returns the service type.
    */
   std::string getServiceType() const;

   /**
    * Used to set the service type.
    */
   void setServiceType(std::string serviceType);

   /**
    * Returns the control URL
    */
   std::string getControlURL() const;

   /**
    * Used to set the control URL.
    */
   void setControlURL(std::string controlURL);
private:
  /**
   * Contains the corresponding XML element with the name 'friendlyName'.
   */
  std::string m_friendlyName;

  /**
   * Contains the corresponding XML element with the name 'UDN'.
   */
  std::string m_udn;

  /**
    * Contains the corresponding XML element with the name 'serviceId'.
    */
  std::string m_ServiceID;

  /**
   * Contains the corresponding XML element with the name 'serviceType'.
   */
  std::string m_ServiceType;

  /**
   * Contains the corresponding XML element with the name 'controlURL'.
   */
  std::string m_ControlURL;
};

} /* xml */
} /* client */
} /* dlna */
#endif /* XML_DESCRIPTION_RESULT_H */
