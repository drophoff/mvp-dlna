#ifndef COMMUNICATION_REQUEST_H
#define COMMUNICATION_REQUEST_H

#include "communication.h"

namespace dlna {
namespace client {

/**
 * \file request.h
 */

class Request : public Communication {
public:
    /**
     * Default constructor
     */
    Request(const dlna::client::store::Device& device);

    long int getObjectID() const;

    void setObjectID(long int objectID);

    /**
     * Returns the data handling flag.
     *
     * \return true 'BrowseDirectChildren' or false 'BrowseMetaData'
     */
    bool isData() const;

    /**
     * Sets the data handling flag.
     *
     * \param true 'BrowseDirectChildren' or false 'BrowseMetaData'
     */
    void setData(bool data);

    void setStartingIndex(long int start);

    long int getStartingIndex() const;

    void setRequestedCount(long int start);

    long int getRequestedCount() const;

private:
    long int m_ObjectID;

    /**
     * Contains true in case the data structure shall be retrieves, which
     * corresponds to the value 'BrowseDirectChildren' in the 'BrowseFlag'
     * element in the browse request, or false in case only meta information
     * are desired 'BrowseMetaData'. The default value is 'false'.
     */
    bool m_Data;

    long int m_StartingIndex;

    long int m_RequestedCount;
};

} /* client */
} /* dlna */
#endif /* COMMUNICATION_REQUEST_H */
