#ifndef CONTENT_DIRECTORY_BROWSER_H
#define CONTENT_DIRECTORY_BROWSER_H

#include "client.h"
#include "request.h"
#include "response.h"
#include "device.h"

#include <dlna/common/task.h>

#include <string>
#include <memory>
#include <vector>
#include <upnp/upnp.h>

namespace dlna {
namespace client {

/**
 * \file contentdirectorybrowser.h
 */

/**
 * This class is responsible to initialize the UPnP (libupnp)
 * subsystem and interacts with a controlled device (aka server)
 * that provides content directoy service functionality.
 */
class ContentDirectoryBrowser {
public:
  /**
   * Default constructor
   */
  ContentDirectoryBrowser();


  /**
   * Default destructor
   */
  ~ContentDirectoryBrowser();


  /**
   * Initialize the underlaying UPnP subsystem.
   */
  void initialize();

  /**
   * \brief Devices apear and disapear on the local area network due to several
   * reasons (device shutdown, lost network connection, device is under heavy
   * load). Due to these circumstances, the returned list of found devices may
   * be incomplete.
   *
   * It can happen that a found device, after calling this method, is no longer
   * available, because it was taken off the grid shortly after the execution of
   * this method.
   *
   * Or a device was put into operation shortly after the call of this method.
   *
   * It is mandatory that the underlaying subsystem has been initialized before
   * this method gets executed.
   *
   * \return a list of actual known UPnP controlled devices that provides at
   * least a Content Directory service.
   */
  const std::vector<Response> retrieveDeviceList() const;

  /**
   * \brief Performs a browse request based on the provided information.
   *
   * It is mandatory that the underlaying subsystem has been initialized before
   * this method gets executed.
   *
   * \param request that should be executed.
   * \return the requested directory structure
   */
  const Response browse(const Request& request) const;

private:
  IXML_Document* createBrowseRequest(const Request& request) const;

  IXML_Document* sendBrowseRequest(IXML_Document* requestDoc, const dlna::client::store::Device& device) const;

  /**
   * Reference to the UPnP subsystem in the role
   * of a client device (control point).
   */
  dlna::client::upnp::Client* m_upnpSubsystem;
};

} /* client */
} /* dlna */
#endif /* CONTENT_DIRECTORY_BROWSER_H */
