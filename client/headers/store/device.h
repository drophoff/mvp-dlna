#ifndef DEVICE_H
#define DEVICE_H

#include <dlna/common/singleton.h>

#include <string>
#include <ctime>

namespace dlna {
namespace client {
namespace store {

/**
 * \file device.h
 */

class Device {
public:
    Device(std::string name, std::string actionURL, std::string serviceType, std::string udn, std::time_t expire);

    std::string getName() const;

    std::string getActionURL() const;

    std::string getServiceType() const;

    std::string getUniqueDeviceNumber() const;

    std::time_t getExpire() const;

private:
    std::string m_Name;

    std::string m_ActionURL;

    std::string m_ServiceType;

    std::string m_udn;

    std::time_t m_Expire;
};

} /* store */
} /* client */
} /* dlna */

#endif /* DEVICE_H */
