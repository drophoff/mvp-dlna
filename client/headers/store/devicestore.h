#ifndef DEVICE_STORE_H
#define DEVICE_STORE_H

#include "device.h"

#include <dlna/common/singleton.h>

#include <string>
#include <unordered_map>
#include <mutex>

namespace dlna {
namespace client {
namespace store {

/**
 * \file devicestore.h
 */

/**
 * This class provides functionality to store information about the
 * UPnP devices that may be apear and disapear on the local area network
 * over the time. This implmentation must be thread safe because the
 * write operation is triggered by another thread than the read operation.
 */
class DeviceStore : public dlna::common::Singleton<DeviceStore> {
friend class dlna::common::Singleton<DeviceStore>;
private:

  /**
   * Default private constructor to avoid direct object
   * creation
   */
  DeviceStore();


  /**
   * Default private destructor to avoid direct deletion
   * calls.
   */
  ~DeviceStore();


  /**
   * Internal data structure to save the information for a device under its
   * unique device number.
   */
  std::unordered_map<std::string, Device> m_Map;


  /**
   * Semaphore to make the store thread safe
   */
  std::mutex m_Mutex;

public:

  /**
   * Updates any existing entry in the store or create an new one.
   *
   * \param uniqueDeviceNumber identifies the key that should be used to store
   * the provided device.
   * \param device that should be updated 
   */
  void update(const std::string& uniqueDeviceNumber, const Device& device);


  /**
   * Removes the device entry from the store.
   *
   * \param uniqueDeviceNumber identifies the key that should be remove
   */
  void remove(const std::string& uniqueDeviceNumber);


  /**
   * Returns the complete store.
   */
  const std::unordered_map<std::string, Device> getStore();


  /**
   * Checks if each device is valid. Invalid devices are removed from the
   * store. Only valid devices remains in the store. Nevertheless the store
   * may be incomplete.
   */
  void removeExpiredDevices();
};

} /* store */
} /* client */
} /* dlna */
#endif /* DEVICE_STORE_H */
