#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include "device.h"

namespace dlna {
namespace client {

/**
 * \file communication.h
 */

/**
 * Base class for all kind of communication (request, response).
 */
class Communication {
public:
    /**
     * Default constructor
     *
     * \param device that should be used for the communication.
     */
    Communication(const dlna::client::store::Device& device);

    /**
     * Returns the actual device that is used for the communication.
     *
     * \return the communication device.
     */
    const dlna::client::store::Device& getDevice() const;

private:

    /**
     * Holds the device that is used for the communication.
     */
    dlna::client::store::Device m_Device;
};

} /* client */
} /* dlna */
#endif /* COMMUNICATION_H */
