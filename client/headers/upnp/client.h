#ifndef UPNP_CLIENT_H
#define UPNP_CLIENT_H

#include <upnp/upnp.h>

namespace dlna {
namespace client {
namespace upnp {

/**
 * \file client.h
 */

class Client {
public:
  /**
   * Default constructor that intialize the underlaying
   * UPnP subsystem.
   */
  Client();


  /**
   * Default destructor that frees all used resources from
   * the underlaying UPnP subsystem.
   */
  ~Client();

  /**
   * Search for all controlled devices and services.
   */
  void searchAll();
};

} /* upnp */
} /* client */
} /* dlna */
#endif /* UPNP_CLIENT_H */
