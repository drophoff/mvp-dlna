#ifndef SEARCH_RESULT_REQUEST_H
#define SEARCH_RESULT_REQUEST_H

#include <dlna/common/eventhandler.h>

#include <upnp/upnp.h>

namespace dlna {
namespace client {
namespace request {

/**
 * \file searchresultrequest.h
 */

class SearchResultRequest : public dlna::common::http::EventHandler {
public:
  bool isResponsibleFor(Upnp_EventType event);


  void handle(void* event);
};

} /* request */
} /* client */
} /* dlna */
#endif /* SEARCH_RESULT_REQUEST_H */
