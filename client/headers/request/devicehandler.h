#ifndef DEVICE_HANDLER_H
#define DEVICE_HANDLER_H

#include <upnp/upnp.h>

namespace dlna {
namespace client {
namespace request {

/**
 * \file devicehandler.h
 */


/**
 * This class provides functionality to handle devices that appear and disappear
 * in the local area network. The class handles only devices that provides a
 * content directory service.
 */
class DeviceHandler {
public:

  /**
   * Performs the necessary actions when a new device appears in the local
   * area network.
   *
   * \param discovery contains device specific information (ID, Type, 
   * Download Location)
   */
  void handleFoundDevice(struct Upnp_Discovery* discovery);

  /**
   * Performs the necessary actions when an existing device disappears from
   * the local area network.
   *
   * \param discovery contains device specific information (ID, Type, 
   * Download Location)
   */
  void handleShutdownDevice(struct Upnp_Discovery* discovery);

private:
  /**
   * Returns true in case the provided discovery is not null and
   * from type content directory, otherwise false.
   *
   * \param discovery contains device specific information (ID, Type, 
   * Download Location)
   */
  bool isDiscoveryValid(struct Upnp_Discovery* discovery);
};

} /* request */
} /* client */
} /* dlna */
#endif /* DEVICE_HANDLER_H */
