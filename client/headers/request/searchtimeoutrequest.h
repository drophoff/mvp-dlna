#ifndef SEARCH_TIMEOUT_REQUEST_H
#define SEARCH_TIMEOUT_REQUEST_H

#include <dlna/common/eventhandler.h>

#include <upnp/upnp.h>

namespace dlna {
namespace client {
namespace request {

/**
 * \file searchtimeoutrequest.h
 */

class SearchTimeoutRequest : public dlna::common::http::EventHandler {
public:
  bool isResponsibleFor(Upnp_EventType event);


  void handle(void* event);
};

} /* request */
} /* client */
} /* dlna */
#endif /* SEARCH_TIMEOUT_REQUEST_H */
