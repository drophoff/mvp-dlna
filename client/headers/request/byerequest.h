#ifndef BYE_REQUEST_H
#define BYE_REQUEST_H

#include <dlna/common/eventhandler.h>

#include <upnp/upnp.h>

namespace dlna {
namespace client {
namespace request {

/**
 * \file byerequest.h
 */

class ByeRequest : public dlna::common::http::EventHandler {
public:
  bool isResponsibleFor(Upnp_EventType event);


  void handle(void* event);
};

} /* request */
} /* client */
} /* dlna */
#endif /* BYE_REQUEST_H */
