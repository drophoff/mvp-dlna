#ifndef COMMUNICATION_RESPONSE_H
#define COMMUNICATION_RESPONSE_H

#include "communication.h"
#include "xmlbrowseresult.h"

namespace dlna {
namespace client {

/**
 * \file response.h
 */

class Response : public Communication {
public:
    /**
     * Default constructor
     */
    Response(const dlna::client::store::Device& device);

    void setResult(const dlna::client::xml::XmlBrowseResult& data);

    const dlna::client::xml::XmlBrowseResult& getResult() const;

private:
    dlna::client::xml::XmlBrowseResult m_Data;
};

} /* client */
} /* dlna */
#endif /* COMMUNICATION_RESPONSE_H */
