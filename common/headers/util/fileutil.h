#ifndef FILE_UTIL_H
#define FILE_UTIL_H

#include "fatalexception.h"
#include <sys/stat.h>
#include <ctime>
#include <string>

namespace dlna {
namespace common {
namespace util {

/**
 * \file fileutil.h
 * \ingroup group_common
 */

/**
 * This class provides functionality to access common attributes
 * from a file or directory like size, modification time and
 * type (file/directory).
 */
class FileUtil {
public:

  /**
   * Use the given file or directory name to read the desired
   * file/directory.
   */
  FileUtil(std::string filename);

  /**
   * Returns the modification time
   */
  std::time_t getTime();

  /**
   * Returns the time (getTime) formatted as ISO 8601
   */
  std::string getTimeFormatted();

  /**
   * Returns true in case it is a file, otherwise
   * false is returend.
   */
  bool isFile();

  /**
   * Returns the size of the file or directory
   */
  off_t getSize();

  /**
   * Returns the part (extension) after the last dot.
   * An empty string is returned in case the file has not
   * dot in his name.
   */
  std::string getSuffix();

  /**
   * Returns the part before the last dot.
   */
  std::string getPrefix();

  /**
   * Returns the inode number of the file or directory
   */
  ino_t getInode();

private:
  /**
   * Internal structure that holds the file or directory attributes
   */
  struct stat m_fileInfo;

  /**
   * The file or directory name
   */
  std::string m_filename;
};

} /* util */
} /* common */
} /* dlna */
#endif /* FILE_UTIL_H */