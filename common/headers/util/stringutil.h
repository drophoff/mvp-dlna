#ifndef STRING_UTIL_H
#define STRING_UTIL_H

#include <string>

namespace dlna {
namespace common {
namespace util {

/**
 * \file stringutil.h
 * \ingroup group_common
 */

/**
 * This class provides functionality to manipulate string via a
 * fluent api.
 */
class StringUtil {
public:
  /**
   * Default c-tor to intialize the util with the provided
   * string.
   */
  StringUtil(std::string string);

  /**
   * Remove the provided string from the current string.
   */
  StringUtil remove(std::string remove) const;

  /**
   * Returns a string where all ' characters are escaped 
   * with an additonal ' character.
   * 
   * Example: "Don't knock'n twice" will be escaped 
   * to "Don''t knock''n twice".
   */
  StringUtil escapeSQL() const;

  /**
   * Remove all special characters like '&' with a corresponding
   * long string expression like 'and'.
   */
  StringUtil replaceXmlTags() const;

  /**
   * Returns the current string.
   */
  std::string getString() const;

private:
  std::string m_string;
};

} /* util */
} /* common */
} /* dlna */
#endif /* STRING_UTIL_H */