#ifndef LOG_FACTORY_H
#define LOG_FACTORY_H

#include "loglevel.h"
#include "logger.h"

namespace dlna {
namespace common {
namespace logging {
namespace internal {
namespace LogFactory {
#ifdef _DEBUG

/**
\file logfactory.h
\ingroup group_common
*/

/**
 * \brief This function determine and returns a concrete logger instance
 * based on the current configuration settings.
 */
const Logger* create();

#endif
} /* LogFactory */
} /* internal */
} /* logging */
} /* common */
} /* dlna */
#endif /* LOG_FACTORY_H */
