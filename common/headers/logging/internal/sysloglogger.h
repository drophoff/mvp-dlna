#ifndef SYSLOG_OUT_LOGGER_H
#define SYSLOG_OUT_LOGGER_H

#include "logger.h"

namespace dlna {
namespace common {
namespace logging {
namespace internal {
#ifdef _DEBUG

/**
 * \file sysloglogger.h
 * \ingroup group_common
 */

/**
 * Concreate logger which use the syslog daemon to create
 * protocol entries.
 */
class SyslogLogger : public Logger {
public:
  /**
   * Default constructor
   */
  SyslogLogger();


  /**
   * Default destructor
   */
  ~SyslogLogger();


  void log(const dlna::common::logging::LogLevel level, const std::string message) const;
};

#endif
} /* internal */
} /* logging */
} /* common */
} /* dlna */
#endif /* SYSLOG_OUT_LOGGER_H */
