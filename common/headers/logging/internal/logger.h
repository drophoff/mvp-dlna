#ifndef LOGGER_H
#define LOGGER_H

#include "loglevel.h"
#include <string>

namespace dlna {
namespace common {
namespace logging {
namespace internal {

/**
 * \file logger.h
 * \ingroup group_common
 */

/**
 * Abstract class that provides logging functionality. Each
 * concrete logging instance must inherit this class.
 */
class Logger {
public:
  /**
   * Default destructor
   */
  virtual ~Logger();

  /**
   * Creates an entry within the application protocol with the
   * provided message content and log level.
   * 
   * \param level describes the criticality of the protocol entry (see loglevel.h).
   * \param message that is used to create the protocol entry.
   */
  virtual void log(const dlna::common::logging::LogLevel level, const std::string message) const = 0;
};

} /* internal */
} /* logging */
} /* common */
} /* dlna */
#endif /* LOGGER_H */
