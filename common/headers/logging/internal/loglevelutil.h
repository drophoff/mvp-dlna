#ifndef LOG_LEVEL_UTIL_H
#define LOG_LEVEL_UTIL_H

#include "loglevel.h"

namespace dlna {
namespace common {
namespace logging {
namespace internal {
namespace LogLevelUtil {
#ifdef _DEBUG

/**
\file loglevelutil.h
\ingroup group_common
*/

/**
 * \brief Converts the log level that is provided as std::string into the
 * enumeration LogLevel.
 * 
 * The passed log level must be equal to the enumeration values (see loglevel.h).
 */
LogLevel valueOf(const std::string logLevel);


/**
 * \brief Converts the provided log level (see loglevel.h) to std::string.
 */
std::string toString(const LogLevel level);

#endif
} /* LogLevelUtil */
} /* internal */
} /* logging */
} /* common */
} /* dlna */
#endif /* LOG_LEVEL_UTIL_H */
