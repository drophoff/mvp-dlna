#ifndef STANDARD_OUT_LOGGER_H
#define STANDARD_OUT_LOGGER_H

#include "logger.h"

namespace dlna {
namespace common {
namespace logging {
namespace internal {
#ifdef _DEBUG

/**
 * \file stdoutlogger.h
 * \ingroup group_common
 */

/**
 * Concrete logger which use the standard iostream to display
 * protocol entries.
 */
class StdoutLogger : public Logger {
public:
  void log(const dlna::common::logging::LogLevel level, const std::string message) const;
};

#endif
} /* internal */
} /* logging */
} /* common */
} /* dlna */
#endif /* STANDARD_OUT_LOGGER_H */
