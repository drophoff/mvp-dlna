#ifndef LOG_UTIL_H
#define LOG_UTIL_H

#include "logger.h"
#include "singleton.h"

namespace dlna {
namespace common {
namespace logging {

/**
 * \file logutil.h
 * \ingroup group_common
 */

/**
 * This class provides functionality to create application
 * protocol entries.
 */
class LogUtil : public dlna::common::Singleton<LogUtil> {
friend class dlna::common::Singleton<LogUtil>;
private:

  /**
   * Default private constructor to avoid direct object
   * creation
   */
  LogUtil();


  /**
   * Default private destructor to avoid direct deletion
   * calls.
   */
  ~LogUtil();

  /**
   * Contains the concrete 'Logger' that is used within
   * within the application to create the protocoll entries.
   */
  const dlna::common::logging::internal::Logger* m_logger;


  /**
   * Contains the minimum 'LogLevel' that shall be handled
   * and displayed within the application.
   */
  LogLevel m_logLevel;

public:
  /**
   * Creates an entry within the application protocol
   * with the provided message content and log level.
   * 
   * \param level describes the criticality of the protocol entry (see loglevel.h).
   * \param message that is used to create the protocol entry.
   */
  void log(const LogLevel level, const std::string message) const;

  /**
   * Returns true in case the provided LogLevel is enabled,
   * otherwise false will be returned.
   */
  bool isEnabled(const LogLevel level) const;
};

} /* logging */
} /* common */
} /* dlna */
#endif /* LOG_UTIL_H */
