#ifndef LOG_LEVEL_H
#define LOG_LEVEL_H

#include <string>

namespace dlna {
namespace common {
namespace logging {

/**
 * \file loglevel.h
 * \ingroup group_common
 * \enum dlna::common::logging::LogLevel LogLevel
 */

/**
 * \brief Defines a set of standard logging levels that can be used
 * to control the logging output.
 */
enum class LogLevel : short {
  /**
   * The TRACE level designates highly detailed events that are
   * used to log diagnostic information messages.
   */
  TRACE = 0,

  /**
   * The DEBUG level designates fine-grained informational events
   * that are most useful to debug the application.
   */
  DEBUG = 10,


  /**
   * The INFO level designates informational message that highlight
   * the process of the application at coarse-grained level.
   */
  INFO = 20,


  /**
   * The WARN level designates potentially harmful situations.
   */
  WARN = 30,


  /**
   * The ERROR level designates error events that might still
   * allow the appliction to continue running.
   */
  ERROR = 40,


  /**
   * The FATA level designates very servere error events that
   * will presumably lead the application to abort.
   */
  FATAL = 50,


  /**
   * This value should only be used within the configuration
   * file to turn the logging off.
   */
  OFF = 100
};

} /* logging */
} /* common */
} /* dlna */
#endif /* LOG_LEVEL_H */
