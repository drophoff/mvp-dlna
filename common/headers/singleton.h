#ifndef SINGLETON_H
#define SINGLETON_H

namespace dlna {
namespace common {

/**
 * \file singleton.h
 * \ingroup group_common
 */

/**
 * This class implements the singleton pattern.
 * \tparam T type that is returned on the getInstance function
 */
template <class T>
class Singleton {
protected:
  /**
   * Default private constructor to avoid direct object
   * creation
   */
  Singleton() {
  }


  /**
   * Default private destructor to avoid direct deletion
   * calls.
   */
  ~Singleton() {
  }

private:
  /**
   * Avoid the generation of the copy constructor
   */
  Singleton(const Singleton&) = delete;


  /**
   * Avoid the generation of the move constructor
   */
  Singleton(const Singleton&&) = delete;


  /**
   * Avoid the generation of the copy assignment operator
   */
  Singleton& operator=(const Singleton&) = delete;


  /**
   * Avoid the generation of the move assignment operator.
   */
  Singleton& operator=(Singleton&&) = delete;

public:

  /**
   * This method returns in a multi threaded environment
   * always the same instance of this class on a C++11
   * system.
   * 
   * Note: If the instance has already been created, it
   * won't be created again.
   */
  static T& getInstance() {
      static T instance;
      return instance;
  }
};

} /* common */
} /* dlna */
#endif /* SINGLETON_H */
