#ifndef XML_ABSTRACT_PARSER_H
#define XML_ABSTRACT_PARSER_H

#include "xmlutil.h"

#include <upnp/upnptools.h>
#include <string>

namespace dlna {
namespace common {
namespace xml {

/**
 * \file xmlabstractparser.h
 * \ingroup group_common
 */

/**
 * This abstract class provides a minimal XML parser. The type of the returned
 * object can adjusted via the template parameter named 'T'.
 */
template <class T>
class XmlAbstractParser {
public:

  /**
   * Default constructor
   */
  XmlAbstractParser(IXML_Document* document) :
      m_document(document) {
  }

  /**
   * Default destructor that does nothing. Override to release
   * the used memory for example the parsed IXML_Document
   */
  virtual ~XmlAbstractParser () {
  }

  /**
   * Performs the parsing of the XML document and returns
   * the requested attributes that are defined by the concrete
   * template type.
   *
   * The returned pointer is filled only is case the 'handleResult'
   * function returns true. In any other case an nullptr will
   * be returned.
   */
  const T * const parse() {
      if (this->m_document != nullptr) {
          parseImpl(this->m_document);
      }
      return (handleResult()) ? &(this->m_T) : nullptr;
  }


private:
  /**
   * Recursive method to parse the provided document and all embedded
   * (DIDL-Lite) documents.
   */
  void parseImpl(IXML_Document* document) {
      DOMString wildcard = dlna::common::xml::XmlUtil::allocate("*");
      IXML_NodeList* elementList = ixmlDocument_getElementsByTagName(document, wildcard);
      if (elementList != nullptr) {
          for (unsigned long int elementIndex = 0; elementIndex < ixmlNodeList_length( elementList ); elementIndex++) {
              IXML_Node* node = ixmlNodeList_item( elementList, elementIndex );
              if (node != nullptr) {
                  const DOMString nodeName = ixmlNode_getLocalName( node );
                  std::string nodeNameStr(nodeName);
                  setElementName(nodeNameStr);

                  handleElement( node );

                  if (nodeNameStr == "Result" ) {
                      IXML_Document* didlDoc = retrieveEmbeddedDIDLElementAsDocument( document );
                      if (didlDoc != nullptr) {
                          parseImpl( didlDoc );
                      }
                      ixmlDocument_free( didlDoc );
                  } else {
                      IXML_Node* child = ixmlNode_getFirstChild( node );
                      if (child != nullptr) {
                          const DOMString nodeValue = ixmlNode_getNodeValue( child );
                          if ( nodeValue != nullptr) {
                              std::string nodeValueStr(nodeValue);

                              handleElementValue( nodeValueStr );
                          }
                      }
                  }
              }
          }
          ixmlNodeList_free(elementList);
      }
      ixmlFreeDOMString(wildcard);
  }

  /**
   * Extracts the embedded XML document that is from type DIDL-Lite (subset of
   * Digital Item Declaration Language) from the XML element named 'Result' and
   * return it as a standalone IXML_Document.
   *
   * <Result>
   *    <DIDL-Lite xmlns:dc="http://purl.org/dc/elements/1.1">
   *         <container>...</container>
   *    </DIDL-Lite>
   * </Result>
   *
   * The returned pointer of the IXML_Document must be released (delete) by the
   * caller.
   */
  IXML_Document* retrieveEmbeddedDIDLElementAsDocument(IXML_Document* document) {
      IXML_Document* embeddedDoc = nullptr;

      DOMString filterElement = dlna::common::xml::XmlUtil::allocate("Result");
      IXML_NodeList* elementList = ixmlElement_getElementsByTagName((IXML_Element*) document, filterElement );
      if (elementList != nullptr) {
          IXML_Node* node = ixmlNodeList_item( elementList, 0 );
          if (node != nullptr) {
              IXML_Node* textNode = ixmlNode_getFirstChild( node );
              if (textNode != nullptr) {
                  const DOMString embeddedDocAsString = ixmlNode_getNodeValue( textNode );
                  if (embeddedDocAsString != nullptr) {
                      embeddedDoc = ixmlParseBuffer( embeddedDocAsString );
                  }
              }
          }
          ixmlNodeList_free(elementList);
      }
      ixmlFreeDOMString(filterElement);

      return embeddedDoc;
  }

  /**
   * A callback function that is called for every XML Element.
   */
  virtual void handleElement(IXML_Node* node) = 0;

  /**
   * A callback function that is called for every value within a XML Element.
   * The corresponding XML Element can be retrieved via the method
   * 'getElementName'.
   */
  virtual void handleElementValue(std::string value) = 0;

  /**
   * This method is called after parsing the last XML Element to
   * give the caller the oportunity to adjust the return value
   * (succesfuly, not succesfuly) to the caller.
   *
   * \return true in case the parse process was succesful, otherwise false.
   */
  virtual bool handleResult() = 0;

protected:
  /**
   * Returns the name of the actual XML Element.
   *
   * \return name of current XML Element
   */
  std::string getElementName() const {
      return this->m_ElementName;
  }

  /**
   * Sets the provided name as actual XML Element.
   *
   * \parm name that should be used to update the XML Element name.
   */
  void setElementName(std::string name) {
      this->m_ElementName = name;
  }

  /**
   * Contains the name of the actual XML Element that has been privously parsed
   * via 'handleElementName'.
   */
  std::string m_ElementName;

  /**
   * Contains a instance of the object in which the parsed attributes shall be
   * stored during the parsing process.
   */
  T m_T;

  /**
   * The XML document that shall be parsed
   */
  IXML_Document* m_document = nullptr;
};

} /* xml */
} /* common */
} /* dlna */
#endif /* XML_ABSTRACT_PARSER_H
 */
