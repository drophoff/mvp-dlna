#ifndef XML_UTIL_H
#define XML_UTIL_H

#include <string>
#include <upnp/upnp.h>
#include <upnp/upnptools.h>

namespace dlna {
namespace common {
namespace xml {
namespace XmlUtil {

/**
 * \file xmlutil.h
 * \ingroup group_common
 */

/**
 * Returns the value of the provided XML element. In case
 * document is null, elementName is null or the element can
 * not be found an nullptr is returned.
 * 
 * Note: In case 'BrowseFlag' is provided then 'BrowseMetadata'
 * is returned.
 * ...
 * <ObjectID>0</ObjectID>
 * <BrowseFlag>BrowseMetadata</BrowseFlag>
 * ...
 */
const DOMString retrieveValueFromElement(IXML_Document* document, DOMString elementName);


/**
 * Writes the content of the provided document to the application
 * protocol in case the log level is at least 'DEBUG'.
 */
void log(IXML_Document* document);

/**
 * Allocates memory for the provided string and returns the information
 * as DOMString. The caller is responsible to free the allocated memory.
 */
DOMString allocate(std::string string);

} /* XmlUtil */
} /* xml */
} /* common */
} /* dlna */
#endif /* XML_UTIL */
