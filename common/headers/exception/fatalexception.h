#ifndef FATAL_EXCEPTION_H
#define FATAL_EXCEPTION_H

#include <exception>

namespace dlna {
namespace common {
namespace exception {
  
/**
 * \file fatalexception.h
 * \ingroup group_common
 */

class FatalException : public std::exception {
};

} /* exception */
} /* common */
} /* dlna */
#endif /* FATAL_EXCEPTION_H */
