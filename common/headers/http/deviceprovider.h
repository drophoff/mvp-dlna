#ifndef DEVICE_PROVIDER_H
#define DEVICE_PROVIDER_H

#include "singleton.h"

#include <upnp/upnp.h>

namespace dlna {
namespace common {
namespace http {

/**
 * \file deviceprovider.h
 * \ingroup group_common
 */

/**
 * This class provides access to the device handle
 * of the UPnP subsystem.
 */
class DeviceProvider : public dlna::common::Singleton<DeviceProvider> {
friend class dlna::common::Singleton<DeviceProvider>;
private:

  /**
   * Default private constructor to avoid direct
   * object creation
   */
  DeviceProvider();


  /**
   * Default private destructor to avoid direct
   * deletion calls.
   */
  ~DeviceProvider();

  /**
   * Holds the device handle itself.
   */
  UpnpDevice_Handle* m_Device;

public:

  /**
   * Returns a pointer of an UPnP device handle.
   *
   * \return the UPnP device handle
   */
  UpnpDevice_Handle* getDevice() const;
};

} /* http */
} /* common */
} /* dlna */

#endif /* DEVICE_PROVIDER_H */
