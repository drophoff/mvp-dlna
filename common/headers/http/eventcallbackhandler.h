#ifndef EVENT_CALLBACK_HANDLER_H
#define EVENT_CALLBACK_HANDLER_H

#include "eventhandler.h"

#include <upnp/upnp.h>
#include <vector>
#include <memory>

namespace dlna {
namespace common {
namespace http {

/**
 * \file eventcallbackhandler.h
 * \ingroup group_common
 */

class EventCallbackHandler {
public:
  static int handleEvent(Upnp_EventType type, void* event, void* cookie);

  static void registerEventHandler(std::shared_ptr<EventHandler> handler);

private:
  /**
   * Avoid the generation of the copy constructor
   */
  EventCallbackHandler(const EventCallbackHandler&) = delete;

  /**
   * Avoid the generation of the move constructor
   */
  EventCallbackHandler(const EventCallbackHandler&&) = delete;

  /**
   * Avoid the generation of the copy assignment operator
   */
  EventCallbackHandler& operator=(const EventCallbackHandler&) = delete;

  /**
   * Avoid the generation of the move assignment operator
   */
  EventCallbackHandler& operator=(const EventCallbackHandler&&) = delete;


  static std::vector<std::shared_ptr<EventHandler>> m_handler;
};

} /* http */
} /* common */
} /* dlna */
#endif /* EVENT_CALLBACK_HANDLER_H */
