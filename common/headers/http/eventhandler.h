#ifndef EVENT_HANDLER_H
#define EVENT_HANDLER_H

#include <upnp/upnp.h>

namespace dlna {
namespace common {
namespace http {

  
/**
 * \file eventhandler.h
 * \ingroup group_common
 */

class EventHandler {
public:
  virtual bool isResponsibleFor(Upnp_EventType event) = 0;


  virtual void handle(void* event) = 0;
};

} /* http */
} /* common */
} /* dlna */
#endif /* EVENT_HANDLER_H */
