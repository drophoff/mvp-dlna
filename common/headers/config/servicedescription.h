#ifndef SERVICE_DESCRIPTION_H
#define SERVICE_DESCRIPTION_H

#include <string>

namespace dlna {
namespace common {
namespace configuration {

/**
 * \file servicedescription.h
 * \ingroup group_common
 */

/**
 * Immutable class that contains XML element values, which are defined within 
 * the service description file.
 */
class ServiceDescription {
public:
  /**
   * Default constructor.
   */
  ServiceDescription();


  /**
   * Returns the unique device number.
   */
  std::string getUniqueDeviceID() const;

  /**
   * Returns the service type from the content directory service.
   */
  std::string getCDServiceType() const;

  /**
   * Returns the service ID from the content directory service.
   */
  std::string getCDServiceID() const;


protected:

  /**
   * Contains the value from the XML element named 'UDN'.
   */
  std::string m_udn;

  /**
   * Contains the value from the XML element named 'serviceType'.
   */
  std::string m_cdServiceType;

  /**
   * Contains the value from the XML element named 'serviceId'.
   */
  std::string m_cdServiceId;
};

} /* configuration */
} /* common */
} /* dlna */
#endif /* SERVICE_DESCRIPTION_H */
