#ifndef CONSTANT_H
#define CONSTANT_H

#include <string>

namespace dlna {
namespace common {
namespace configuration {

/**
 * \file constant.h
 * \ingroup group_common
 */

/**
 * This file contains global constant that can be used in the whole
 * application.
 */
struct Constant {
  /**
   * The time interval in minutes to search for changed
   * files.
   */
  const static unsigned short DEFAULT_SCAN_INTERVAL;

  /**
   * The path, in which the running application is expected, in
   * case no valid value is provided via the configuration file.
   */
  const static std::string DEFAULT_PATH;

  /**
   * The default port that is used in case no valid value is
   * provided via configuration file.
   */
  const static unsigned short DEFAULT_PORT;

  /**
   * The default IP address is used in case no valid value is
   * provided via configuration file.
   */
  const static std::string DEFAULT_ADDRESS;

  /**
   * The name of the virtual CDS directory that is used within
   * the internal CDS webserver from the libupnp SDK.
   *
   * Note: Provides the CDS browsing
   */
  const static std::string VIRTUAL_CDS_DIR;

  /**
   * The name of the virtual WEB directy that is used within
   * the internal WEB webserver from the libupnp SDK.
   *
   * Note: Provides the web browsing
   */
  const static std::string VIRTUAL_WEB_DIR;

  /**
   * The name of the physical WEB directory on the hard disc
   * that contains (via the virtual web dir) published
   * information.
   */
  const static std::string PHYSICAL_WEB_DIR;

  /**
   * Represents the property 'SystemUpdateID'
   */
  const static std::string PROPERTY_SYSTEM_UPDATE_ID;

  /**
   * Represents the property 'ContainerUpdateIDs'
   */
  const static std::string PROPERTY_CONTAINER_UPDATE_ID;

  /**
   * Represents the property 'InitialScanPerformed'
   */
  const static std::string PROPERTY_INITIAL_SCAN_PERFORMED;

  /**
   * Represents the property 'StrictMode'
   */
  const static std::string PROPERTY_STRICT_MODE;

  /**
   * The current version of this application.
   */
  const static std::string VERSION;

  /**
   * The maximum content length that the underlaying UPnP system will process.
   */
  const static unsigned int MAX_CONTENT_LENGTH;
};

} /* configuration */
} /* common */
} /* dlna */
#endif /* CONSTANT_H */
