#ifndef CONFIGURATION_READER_H
#define CONFIGURATION_READER_H

#include "configuration.h"

namespace dlna {
namespace common {
namespace configuration {
namespace ConfigReader {

/**
\file configreader.h
\ingroup group_common
*/


/**
 * \brief Read and returns the configuration settings from a file
 * named '/etc/dlna.conf'.
 */
Configuration retrieve();


} /* ConfigReader */
} /* configuration */
} /* common */
} /* dlna */
#endif /* CONFIGURATION_READER_H */
