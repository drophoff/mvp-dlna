#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <string>
#include <vector>

namespace dlna {
namespace common {
namespace configuration {

/**
 * \file configuration.h
 * \ingroup group_common
 */

/**
 * Immutable class that contains all existing configuration
 * settings, which are defined within a flat text based file.
 */
class Configuration {
public:
  /**
   * Default constructor.
   */
  Configuration();


  /**
   * Contains 'true' in case the application should be started
   * as a Daemon. Otherwise the application is started in the
   * foreground. In this situation the member variable contains
   * the value 'false'.
   */
  bool isDaemon() const;


  /**
   * Indicates whether the DNLA protocol is strictly adhered. In
   * this mode the attribute contains the value 'true'. Otherwise,
   * minor deviations from the protocol are allowed 'false'.
   */
  bool isStrictModeOn() const;


  /**
   * Contains the log level that shall be used to restrict the
   * displyed log message.
   * 
   * All log messages, which are greater or equal than the current
   * value (log level) are displayed. The related log level
   * is defined at the enumeration named 'LogLevel' (see loglevel.h).
   */
  std::string getLogLevel() const;


  /**
   * Returns the name of the folder with the dlna executable.
   */
  std::string getPath() const;


  /**
   * Returns the IP in a format like this 10.0.0.10 on which
   * the server should listen
   */
  std::string getIP() const;


  /**
   * Returns the PORT from the configuration file or the default
   * one in case the port is not valid. The default port is 49152.
   */
  unsigned short getPort() const;


  /**
   * Returns a list of directories that should be scanned.
   */
  std::vector<std::string> getScanDirectories() const;

  /**
   * Returns the time interval in minutes in which to search
   * for changed files.
   */
  unsigned short getScanInterval() const;

protected:
  /**
   * Contains the corresponding value from the configuration
   * file named DAEMON.
   */
  bool m_daemon;


  /**
   * Contains the corresponding value from the configuration
   * file named STRICT.
   */
  bool m_strictMode;


  /**
   * Contains the corresponding value from the configuration
   * file named LOG_LEVEL.
   */
  std::string m_logLevel;


  /**
   * Contains the corresponding value from the configuration
   * file named PATH.
   */
  std::string m_path;


  /**
   * Contains the corresponding value from the configuration
   * file named LISTEN.
   */
  std::string m_ip;


  /**
   * Contains the corresponding value from the configiration
   * file named PORT. Only ports above 49152 are considered
   * as valid.
   */
  unsigned short m_port;


  /**
   * Contains the corresponding value from the configuration
   * file named SCAN.
   */
  std::vector<std::string> m_scan;

  /**
   * Contains the corresponding value from the configuration
   * file named SCAN_INTERVAL.
   */
  unsigned short m_interval;
};

} /* configuration */
} /* common */
} /* dlna */
#endif /* CONFIGURATION_H */
