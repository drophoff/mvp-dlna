#ifndef DESCRIPTION_READER_H
#define DESCRIPTION_READER_H

#include "servicedescription.h"

namespace dlna {
namespace common {
namespace configuration {
namespace DescReader {

/**
\file descriptionreader.h
\ingroup group_common
*/


/**
 * \brief Read and returns values from the description file
 * named 'description.xml'.
 */
ServiceDescription retrieve();


} /* DescReader */
} /* configuration */
} /* common */
} /* dlna */
#endif /* DESCRIPTION_READER_H */
