#ifndef ADMIN_CONFIGURATION_H
#define ADMIN_CONFIGURATION_H

#include "configuration.h"

namespace dlna {
namespace common {
namespace configuration {
namespace internal {

/**
 * \file adminconfiguration.h
 * \ingroup group_common
 */
  
/**
 * This class extends the base Configuration by adding
 * functionality to change configuration settings. The class
 * should not be used from external namespaces.
 */
class AdminConfiguration : public Configuration {
public:
  /**
   * Used the provided formal parameter to set the operation
   * mode. If 'true' is passed the application will be started
   * as a Daemon. Otherwise the application is started in the
   * forground.
   */
  void setDaemon(bool daemon);

  /**
   * Used the provided formal parameter to set the strict
   * mode. If 'true' the application follows the DLNA specifications.
   * Otherwise minor deviations from the DLNA protocol are allowed.
   */
  void setStrictMode(bool strictMode);


  /**
   * Used the provided formal parameter to set the log level 
   * (see loglevel.h) that shall be used within the application.
   * All log messages, which are greater or equal as the provided
   * log level are displayed.
   */
  void setLogLevel(const std::string& logLevel);


  /**
   * Used to set the folder with the dlna executeable.
   */
  void setPath(const std::string& path);


  /**
   * Used the provided formal parameter to set the IP,
   * on which the server should be listen for requests.
   */
  void setIP(const std::string& listen);

  
  /**
   * Used the provided formal parameter to set the port,
   * on which the server should be listen for requests.
   */
  void setPort(unsigned short port);

  /**
   * Used the provided formal parameter to set the list
   * of directories that should be scanned.
   */
  void setScanDirectories(std::vector<std::string> scan);

  /**
   * Used the provided formal paramter to set the time
   * interval in minutes in which to search for changed
   * files.
   */
  void setScanInterval(unsigned short scanInterval);
};

} /* internal */
} /* configuration */
} /* common */
} /* dlna */
#endif /* ADMIN_CONFIGURATION_H */
