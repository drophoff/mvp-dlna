#ifndef ADMIN_SERVICE_DESCRIPTION_H
#define ADMIN_SERVICE_DESCRIPTION_H

#include "servicedescription.h"

namespace dlna {
namespace common {
namespace configuration {
namespace internal {

/**
 * \file adminservicedescription.h
 * \ingroup group_common
 */
  
/**
 * This class extends the base ServiceDescription by adding
 * functionality to change values. The class should not be
 * used from external namespaces.
 */
class AdminServiceDescription : public ServiceDescription {
public:

  /**
   * Used the provided formal parameter to set the unique device ID
   */
  void setUniqueDeviceID(const std::string& uniqueDeviceID);


  /**
   * Used the provided formal parameter ti set the service type for the
   * content directory.
   */
  void setCDServiceType(const std::string& serviceType);

  /**
   * Used the provided formal parameter ti set the service ID for the
   * content directory.
   */
  void setCDServiceID(const std::string& serviceID);

};

} /* internal */
} /* configuration */
} /* common */
} /* dlna */
#endif /* ADMIN_SERVICE_DESCRIPTION_H */
