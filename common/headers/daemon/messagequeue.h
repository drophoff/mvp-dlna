#ifndef MESSAGE_QUEUE_H
#define MESSAGE_QUEUE_H

#include "message.h"

#include <mqueue.h>
#include <string>

namespace dlna {
namespace common {
namespace daemon {
  
/**
 * \file messagequeue.h
 * \ingroup group_common
 */

/**
 * Provides a message queue for interprocess communication.
 */
class MessageQueue {
public:
    /**
     * Creates a message queue with the defined queue
     * name, size and role (server/client).
     */
    MessageQueue(std::string queueName, bool isServer = false, unsigned short queueSize = 1, unsigned short messageSize = 20);

    /**
     * Default destructor
     */
    ~MessageQueue();

    /**
     * Sends the desired message.
     */
    void send(Message msg);

    /**
     * Returns true in case the provided message has
     * been received, otherwise false.
     */
    bool receive(Message msg);

    /**
     * Contains the name of the data queue
     * 
     * Note: This queue is used to notify the database process
     * about a file changed that has been detected from the monitor
     * process.
     */
    static std::string QUEUE_FILE_CHANGE;

private:
    /**
     * Contains the internal file descriptor for the message queue
     */
    mqd_t m_msg;

    /**
     * Contains the name of the used queue
     */
    std::string m_queue_name;

    /**
     * True in case this instance interact as server, otherwise
     * this is the client (false).
     */
    bool m_isServer;

    /**
     * Contains the maximum allowed number of entries thar are
     * supported within a queue.
     */
    unsigned short m_queue_max_size;

    /**
     * Contains the maximum size in bytes of a message that
     * are transported and supported within this class.
     */
    unsigned short m_msg_max_size;
};

} /* daemon */
} /* common */
} /* dlna */
#endif /* MESSAGE_QUEUE_H */
