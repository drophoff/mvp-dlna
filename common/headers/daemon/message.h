#ifndef MESSAGE_H
#define MESSAGE_H

#include <string>

namespace dlna {
namespace common {
namespace daemon {

/**
 * \file message.h
 * \ingroup group_common
 * \enum dlna::daemon::Message Message
 */


enum class Message : short {
  /**
   * General notification message
   */
  NOTIFY = 0
};

} /* daemon */
} /* common */
} /* dlna */
#endif /* MESSAGE_H */
