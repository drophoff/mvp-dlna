#ifndef PROCESS_UTIL_H
#define PROCESS_UTIL_H

#include "signalhandler.h"
#include "task.h"
#include <vector>
#include <memory>

namespace dlna {
namespace common {
namespace daemon {
  
/**
 * \file processutil.h
 * \ingroup group_common
 */

class ProcessUtil {
public:

  /**
   * Default constructor
   * 
   * Task:
   * A vector of smart pointers of pure virtual classes 
   * from type Task. The provided tasks are executed. 
   */
  ProcessUtil(std::vector<std::shared_ptr<Task> > tasks);


  /**
   * Default destructor that frees all resources that were
   * allocated during the initialization phase of the daemon
   * including the provided task list (see c-tor).
   */
  ~ProcessUtil();


  /**
   * Change the current process to a daemon and afterwards
   * starts the child processes.
   * 
   * Note: Either call damonize Process xor foreground
   * process not both.
   */
  void daemonizeProcess();


  /**
   * Run the process in the foreground and afterwards
   * starts the child process.
   * 
   * Note: Either call damonize Process xor foreground
   * process not both.
   */
  void foregroundProcess();

private:

  /**
   * Contains the signal handler that is used within the
   * daemon and his childs to react on signals.
   */
  dlna::common::daemon::internal::SignalHandler m_handler;


  /**
   * Contains the list of Tasks that shall be created
   * when the function start child processes is called.
   */
  std::vector<std::shared_ptr<Task> > m_tasks;


  /**
   * Create and starts child process.
   */
  void startChildProcesses();
};

} /* daemon */
} /* common */
} /* dlna */
#endif /* PROCESS_UTIL_H */
