#ifndef MESSAGE_UTIL_H
#define MESSAGE_UTIL_H

#include "message.h"

namespace dlna {
namespace common {
namespace daemon {
namespace internal {
namespace MessageUtil {


/**
\file messageutil.h
\ingroup group_common
*/

/**
 * \brief Converts the provided message (see message.h) to std::string.
 */
std::string toString(const dlna::common::daemon::Message message);

} /* MessageUtil */
} /* internal */
} /* daemon */
} /* common */
} /* dlna */
#endif /* MESSAGE_UTIL_H */
