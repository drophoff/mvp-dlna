#ifndef SIGNAL_HANDLER_H
#define SIGNAL_HANDLER_H

namespace dlna {
namespace common {
namespace daemon {
namespace internal {
  
/**
 * \file signalhandler.h
 * \ingroup group_common
 */

/**
 * This class provides functionality to register different kind of signals
 * that should be handled.
 */
class SignalHandler {
public:
  /**
   * Register the provided signal at the signal handler
   * using the internal signal handler function.
   */
  void registerSignal(int signal) const;


  /**
   * Returns true in case an exit signal has been received
   * and handled within this class, otherwise false will be
   * returned.
   */
  bool receivedExitSignal() const;

private:

  /**
   * Contains true in case an exit signal has been received
   * and handled within this class. The attribute is initialized
   * with the value false at default value.
   */
  static bool m_receivedExitSignal;


  /**
   * Internal function that handles the registered signals.
   * The function is passed to the default "signal" function.
   */
  static void handle(int signal);
};

} /* internal */
} /* daemon */
} /* common */
} /* dlna */
#endif /* SIGNAL_HANDLER_H */
