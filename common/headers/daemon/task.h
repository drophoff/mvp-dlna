#ifndef TASK_H
#define TASK_H

#include <string>

namespace dlna {
namespace common {
namespace daemon {

/**
 * \file task.h
 * \ingroup group_common
 */

/**
 * \brief Represents an abstract task, which can be executed.
 */
class Task {
public:
  /**
   * Performs the desired actions at execution. The function
   * is call regulary.
   */
  virtual void execute() = 0;


  /**
   * Returns the name of the task. The function will only
   * be use for logging.
   */
  virtual const std::string getName() const = 0;


  /**
   * Returns the time in seconds that will elapse before
   * the next execution of the 'execute' function. The default
   * delay time is five seconds.
   */
  virtual unsigned short getDelay() const;


  /**
   * Performs the desired actions at execution. The function
   * is called only one-time.
   */
  virtual void executeOneTime();
};

} /* daemon */
} /* common */
} /* dlna */
#endif /* TASK_H */
