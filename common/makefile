DEST         := libcommon
SO_LINK_NAME := $(DEST).so
SO_NAME      := $(SO_LINK_NAME).1
SO_REAL_NAME := $(SO_NAME).0.0
INCLUDE_DIR  := /usr/include/dlna/common
LD_LIBRARY   := /usr/local/lib/
GCCFLAGCOMP  := -D_FILE_OFFSET_BITS=64 -D_DEBUG -c -Wall -Wclobbered -Wignored-qualifiers -Wmissing-field-initializers -Wtype-limits -Wuninitialized -Wno-psabi -std=c++11 -fPIC -O2
GCCFLAGLINK  := -dynamic -lc -lrt -lstdc++ -Wl,-rpath,$(LD_LIBRARY),-soname,$(SO_NAME) -shared
DIRHEADER    := ./headers
DIRSOURCE    := ./sources
DIROBJECT    := ./objects
HEADERINC    := $(shell find $(DIRHEADER) -type d | sed 's|./|-I ./|' | tr '\n' ' ')
DIROBJDIRS   := $(shell find $(DIRSOURCE) -type d | sed 's|sources|objects|')
SOURCES      := $(shell find $(DIRSOURCE) -name "*.cpp" -type f)
HEADERS      := $(shell find $(DIRHEADER) -name "*.h" -type f)
OBJECTS      := $(shell echo $(SOURCES) | sed 's|sources|objects|g' | sed 's|\.cpp|\.o|g')
DEPENDFILE   := make.depend


all:			GCCFLAGCOMP += -s
all:			$(DEST)


#Creates a single file with all dependencies
$(DEPENDFILE):	$(SOURCES) $(HEADERS)
		$(CXX) $(HEADERINC) $(GCCFLAGCOMP) -MM $(SOURCES) > $(DEPENDFILE)


#Include the dependency file and creates a new one if it is necessary
-include $(DEPENDFILE)


# Build the application
$(DEST):		$(OBJECTS)
			$(CXX) $(OBJECTS) $(GCCFLAGLINK) -o $(SO_REAL_NAME)
			ar rcsv $(DEST).a $(OBJECTS)


# Creates all object files
$(DIROBJECT)/%.o: $(DIRSOURCE)/%.cpp | $(DIROBJECT)
			$(CXX) $(HEADERINC) $(GCCFLAGCOMP) $< -o ./$@

analyse:
			mkdir -p reports
			cppcheck . --xml --xml-version=2 2> reports/cppcheck.xml

$(DIROBJECT):
			mkdir $(DIROBJDIRS)


# Clean all created or updated files
clean:
			rm -f $(DEST)
			rm -f -r $(DIROBJECT)/
			rm -f $(DEPENDFILE)
			rm -f -r reports
			rm -f $(DEST).*


install:		$(DEST)
			cp $(DEST).* $(LD_LIBRARY)
			mkdir -p $(INCLUDE_DIR)
			cp $(HEADERS) $(INCLUDE_DIR)
			ln -f -s $(LD_LIBRARY)$(SO_NAME) $(LD_LIBRARY)$(SO_LINK_NAME)
			ln -f -s $(LD_LIBRARY)$(SO_REAL_NAME) $(LD_LIBRARY)$(SO_NAME)


uninstall:
			rm -f $(LD_LIBRARY)/$(DEST).*
			rm -f -r -d $(INCLUDE_DIR)


debug:			GCCFLAGCOMP += -g --coverage
			GCCFLAGLINK += --coverage
debug:			$(DEST)
