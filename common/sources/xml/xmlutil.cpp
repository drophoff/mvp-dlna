#include "xmlutil.h"
#include "logutil.h"

#include <memory>

namespace dlna {
namespace common {
namespace xml {
namespace XmlUtil {


const DOMString retrieveValueFromElement(IXML_Document* document, DOMString elementName) {
    const DOMString returnValue = nullptr;

    if (document != nullptr && elementName != nullptr) {
        IXML_NodeList* elementList = ixmlDocument_getElementsByTagName( document, elementName );
        if (elementList != nullptr) {
            for (unsigned long int nodeIndex = 0; nodeIndex < ixmlNodeList_length( elementList ); nodeIndex++) {
                IXML_Node* elementNode = ixmlNodeList_item( elementList, nodeIndex);
                IXML_Node* childNode = ixmlNode_getFirstChild( elementNode );

                if (childNode != nullptr) {
                    returnValue =  ixmlNode_getNodeValue( childNode );
                }
            }

            ixmlNodeList_free( elementList );
        }
    }

    return returnValue;
};


void log(IXML_Document* doc) {
     if (dlna::common::logging::LogUtil::getInstance().isEnabled(dlna::common::logging::LogLevel::TRACE) && doc != nullptr) {
         DOMString domString = ixmlDocumenttoString( doc );
         dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::TRACE, domString);
         ixmlFreeDOMString( domString );
    }
};


DOMString allocate(std::string string) {
    return ixmlCloneDOMString(string.c_str());
}


} /* XmlUtil */
} /* xml */
} /* common */
} /* dlna */
