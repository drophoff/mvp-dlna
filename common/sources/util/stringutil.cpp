#include "stringutil.h"

#include <sstream>

namespace dlna {
namespace common {
namespace util {

  
StringUtil::StringUtil(std::string string) {
  if (!string.empty()) {
      this->m_string = string;
  } else {
      this->m_string = "";
  }
}


std::string StringUtil::getString() const {
  return this->m_string;
}


StringUtil StringUtil::remove(std::string remove) const {
  std::string returnValue = remove;
  
  if (!remove.empty()) {
      size_t length = remove.length();
      if ((length != std::string::npos) && (this->m_string.length() >= length)) {
          returnValue = this->m_string.substr(length);
      }
  }
  
  return StringUtil(returnValue);
}

StringUtil StringUtil::escapeSQL() const {
  const char* unescaped = this->m_string.c_str();
  std::stringstream sstream;
  
  while (*unescaped != '\0') {    
      char character = *unescaped;
      
      switch (character) {
	
	case '\'':
	  sstream << character << character;
	  break;
	default:
	  sstream << character;
	  break;
      }
      
      unescaped++;
  }
  
  return StringUtil(sstream.str());
}

StringUtil StringUtil::replaceXmlTags() const {
  const char* unescaped = this->m_string.c_str();
  std::stringstream sstream;
  
  while (*unescaped != '\0') {    
      char character = *unescaped;
      
      switch (character) {
        
        case '&':
          sstream << "and";
          break;
        case '<':
          sstream << "less";
          break;
        case '>':
          sstream << "greater";
          break;
        case '\"':
          sstream << "qoute";
          break;
        default:
          sstream << character;
          break;
      }
      
      unescaped++;
  }
  
  return StringUtil(sstream.str());
}

} /* util */
} /* common */
} /* dlna */
