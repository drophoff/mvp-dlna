#include "fileutil.h"
#include "logutil.h"

#include <sys/stat.h>

namespace dlna {
namespace common {
namespace util {

FileUtil::FileUtil(std::string filename) {
    int ret = stat(filename.c_str(), &(this->m_fileInfo));
    if (ret != 0) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::WARN, "Cannot retrieve file information for \'"+filename+"\'");

        throw dlna::common::exception::FatalException();
    }

    this->m_filename = filename;
}


std::time_t FileUtil::getTime() {
    return this->m_fileInfo.st_mtime;
}


std::string FileUtil::getSuffix() {
    std::string suffix = "";
    size_t posOfLastDot = this->m_filename.find_last_of(".");
    if (posOfLastDot != std::string::npos) {
        suffix = this->m_filename.substr(posOfLastDot + 1);
    }

    return suffix;
}


std::string FileUtil::getPrefix() {
    std::string prefix = "";
    size_t posOfLastBackSlashDot = this->m_filename.find_last_of("/");
    if (posOfLastBackSlashDot != std::string::npos) {
        std::string temp = this->m_filename.substr(posOfLastBackSlashDot + 1);
        size_t posOfLastDot = temp.find_last_of(".");
        if (posOfLastDot != std::string::npos) {
            prefix = temp.substr(0, posOfLastDot);
        }
    }

    return prefix;
}


std::string FileUtil::getTimeFormatted() {
    std::time_t modificationTime = getTime();
    std::tm local_time;
    std::tm* ptm = localtime_r( &modificationTime, &local_time );
    const int bufferSize = 20;
    char dateBuffer[bufferSize] = { 'Y', 'Y', 'Y', 'Y', '-', 'M', 'M', '-', 'D', 'D', 'T', 'H', 'H', ':', 'M', 'M', ':', 'S', 'S', '\0'};
    std::string modDate;
    if (strftime(dateBuffer, bufferSize, "%Y-%m-%dT%H:%M:%S", ptm) != 0) {
      modDate.assign(dateBuffer, bufferSize - 1);
    }

    return modDate;
}


bool FileUtil::isFile() {
    return (S_ISDIR(this->m_fileInfo.st_mode));
}
 

off_t FileUtil::getSize() {
    return this->m_fileInfo.st_size;
}

ino_t FileUtil::getInode() {
    return this->m_fileInfo.st_ino;
}


} /* util */
} /* common */
} /* dlna */
