#include "servicedescription.h"

namespace dlna {
namespace common {
namespace configuration {


ServiceDescription::ServiceDescription() :
    m_udn("") {
}


std::string ServiceDescription::getUniqueDeviceID() const {
    return this->m_udn;
}


std::string ServiceDescription::getCDServiceType() const {
    return this->m_cdServiceType;
}


std::string ServiceDescription::getCDServiceID() const {
    return this->m_cdServiceId;
}

} /* configuration */
} /* common */
} /* dlna */
