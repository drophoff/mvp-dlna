#include "configreader.h"
#include "constant.h"
#include "adminconfiguration.h"

#include <sstream>
#include <fstream>
#include <string>
#include <algorithm>

namespace dlna {
namespace common {
namespace configuration {
namespace ConfigReader {


int convert(const std::string& value) {
   int returnValue = 0;

   try {
       returnValue = std::stoi( value );
   } catch (...) {
       /**
        * Ignore the exception and return the default value
        */
   }

   return returnValue;
}


dlna::common::configuration::internal::AdminConfiguration read() {
    static dlna::common::configuration::internal::AdminConfiguration config;

    std::ifstream file("/etc/dlna.conf", std::ios_base::in);
    std::string line;

    while (file.good() && std::getline(file, line)) {
        if (line.length() == 0) continue;
        if (line[0] == '#') continue;
        if (line[0] == ';') continue;

        unsigned long equalPosition = line.find('=');
        if (equalPosition > 0) {
            std::string name  = line.substr(0, equalPosition - 1);
            std::string value = line.substr(equalPosition + 1);

            /**
             * Remove all spaces by shifting all non-space characters to the left
             * and then erasing the extra.
             */
            name.erase(  std::remove(name.begin(),  name.end(),  ' '), name.end()   );
            value.erase( std::remove(value.begin(), value.end(), ' '), value.end() );

            if ( name == "DAEMON" ) {
                if (value == "true" || value == "yes") config.setDaemon( true );
            }

            if ( name == "STRICT" ) {
                if (value == "true" || value == "yes") config.setStrictMode( true );
            }

            if ( name == "LOG_LEVEL" ) {
                config.setLogLevel( value );
            }

            if ( name == "PATH" ) {
                config.setPath( value );
            }

            if ( name == "LISTEN" ) {
                config.setIP( value );
            }

            if ( name == "PORT" ) {
                int configPort = convert( value );

                unsigned short port = (configPort <= Constant::DEFAULT_PORT || configPort >= 65535 ) ? Constant::DEFAULT_PORT : configPort;
                config.setPort( port );
            }

            if ( name == "SCAN" ) {
               std::istringstream stream( value );

               std::string splitValue;
               std::vector<std::string> scanDirectories;
               while (getline( stream, splitValue, ';')) {
                   scanDirectories.push_back( splitValue  );
               }
               config.setScanDirectories( scanDirectories );
            }

            if ( name == "SCAN_INTERVAL" ) {
               int configScanInterval = convert( value );

               unsigned short scanInterval = (configScanInterval < 1 || configScanInterval >= 65535) ? Constant::DEFAULT_SCAN_INTERVAL : configScanInterval;
               config.setScanInterval( scanInterval );
            }
        }
    }

    return config;
}


Configuration retrieve() {
    static Configuration config = (Configuration) read();
    return config;
}

} /* ConfigReader */
} /* configuration */
} /* common */
} /* dlna */
