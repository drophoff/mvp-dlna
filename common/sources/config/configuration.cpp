#include "configuration.h"
#include "constant.h"

namespace dlna {
namespace common {
namespace configuration {


Configuration::Configuration() :
    m_daemon(false),
    m_strictMode(false),
    m_logLevel("ERROR"),
    m_path(Constant::DEFAULT_PATH),
    m_ip(Constant::DEFAULT_ADDRESS),
    m_port(Constant::DEFAULT_PORT),
    m_interval(Constant::DEFAULT_SCAN_INTERVAL) {
}

bool Configuration::isDaemon() const {
    return this->m_daemon;
}

bool Configuration::isStrictModeOn() const {
    return this->m_strictMode;
}

std::string Configuration::getLogLevel() const {
    return this->m_logLevel;
}

std::string Configuration::getPath() const {
    return this->m_path;
}

std::string Configuration::getIP() const {
    return this->m_ip;
}

std::vector<std::string> Configuration::getScanDirectories() const {
    return this->m_scan;
}

unsigned short Configuration::getPort() const {
    return this->m_port;
}

unsigned short Configuration::getScanInterval() const {
   return this->m_interval;
}

} /* configuration */
} /* common */
} /* dlna */
