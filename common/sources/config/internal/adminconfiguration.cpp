#include "adminconfiguration.h"

namespace dlna {
namespace common {
namespace configuration {
namespace internal {

void AdminConfiguration::setDaemon(bool daemon) {
    this->m_daemon = daemon;
}

void AdminConfiguration::setStrictMode(bool strictMode) {
    this->m_strictMode = strictMode;
}

void AdminConfiguration::setLogLevel(const std::string& logLevel) {
    this->m_logLevel = logLevel;
}

void AdminConfiguration::setPath(const std::string& path) {
    this->m_path = path;
}

void AdminConfiguration::setIP(const std::string& listen) {
    this->m_ip = listen;
}

void AdminConfiguration::setPort(unsigned short port) {
    this->m_port = port;
}

void AdminConfiguration::setScanDirectories(std::vector<std::string> scan) {
    this->m_scan = scan;
}

void AdminConfiguration::setScanInterval(unsigned short scanInterval) {
    this->m_interval = scanInterval;
}

} /* internal */
} /* configuration */
} /* common */
} /* dlna */
