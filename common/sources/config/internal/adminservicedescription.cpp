#include "adminservicedescription.h"

namespace dlna {
namespace common {
namespace configuration {
namespace internal {


void AdminServiceDescription::setUniqueDeviceID(const std::string& uniqueDeviceID) {
     this->m_udn = uniqueDeviceID;
}


void AdminServiceDescription::setCDServiceType(const std::string& serviceType) {
     this->m_cdServiceType = serviceType;
}


void AdminServiceDescription::setCDServiceID(const std::string& serviceId) {
     this->m_cdServiceId = serviceId;
}

} /* internal */
} /* configuration */
} /* common */
} /* dlna */
