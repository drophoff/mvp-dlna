#include "descriptionreader.h"
#include "adminservicedescription.h"
#include "configreader.h"
#include "constant.h"

#include <fstream>
#include <string>

namespace dlna {
namespace common {
namespace configuration {
namespace DescReader {


std::string extractXmlElement(const std::string& line, const std::string& xmlElement) {
    std::string returnValue;
    std::string xmlStartElement = "<"  + xmlElement + ">";
    std::string xmlEndElement   = "</" + xmlElement + ">";

    unsigned long startXmlTagPos = line.find( xmlStartElement );
    unsigned long endXmlTagPos = line.find( xmlEndElement );

    if ( startXmlTagPos > 0 && endXmlTagPos > 0) {
        unsigned long startIndex = startXmlTagPos + xmlStartElement.length();
        unsigned long endIndex = endXmlTagPos - startIndex;

        if (startIndex > 0 && startIndex < endIndex) {
            std::string xmlElementValue = line.substr( startIndex, endIndex );
            returnValue = xmlElementValue;
        }
    }

    return returnValue;
}


dlna::common::configuration::internal::AdminServiceDescription read() {
    static dlna::common::configuration::internal::AdminServiceDescription description;

    Configuration config = ConfigReader::retrieve();
    std::ifstream file(config.getPath() + dlna::common::configuration::Constant::PHYSICAL_WEB_DIR + "/description.xml", std::ios_base::in);
    std::string line;

    while(file.good() && std::getline(file, line) ) {
        if (line.length() == 0) continue;

        std::string udn = extractXmlElement(line, "UDN");
        if (udn.length() > 0) {
            description.setUniqueDeviceID( udn );
        }

        std::string serviceType = extractXmlElement(line, "serviceType");
        if (serviceType.length() > 0) {
            description.setCDServiceType( serviceType );
        }

        std::string serviceId = extractXmlElement(line, "serviceId");
        if (serviceId.length() > 0) {
            description.setCDServiceID( serviceId );
        }
    }

    return description;
}


ServiceDescription retrieve() {
     static ServiceDescription instance = (ServiceDescription) read();
     return instance;
}


} /* DescReader */
} /* configuration */
} /* common */
} /* dlna */
