#include "constant.h"

namespace dlna {
namespace common {
namespace configuration {

const unsigned short Constant::DEFAULT_SCAN_INTERVAL = 15;

const unsigned short Constant::DEFAULT_PORT = 49152;

const std::string Constant::DEFAULT_ADDRESS = "127.0.0.1";

const std::string Constant::DEFAULT_PATH = "/opt/dlna";

const std::string Constant::VIRTUAL_CDS_DIR = "/cds_content/";

const std::string Constant::VIRTUAL_WEB_DIR = "/dlna/";

const std::string Constant::PHYSICAL_WEB_DIR = "/webserver";

const std::string Constant::PROPERTY_SYSTEM_UPDATE_ID = "SystemUpdateID";

const std::string Constant::PROPERTY_CONTAINER_UPDATE_ID = "ContainerUpdateIDs";

const std::string Constant::PROPERTY_INITIAL_SCAN_PERFORMED = "InitialScanPerformed";

const std::string Constant::PROPERTY_STRICT_MODE = "StrictMode";

const std::string Constant::VERSION = "1.1.0";

const unsigned int Constant::MAX_CONTENT_LENGTH = 1024 * 1024 * 2;

} /* configuration */
} /* common */
} /* dlna */
