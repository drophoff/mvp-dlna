#include "logutil.h"
#include "loglevelutil.h"
#include "logfactory.h"
#include "configreader.h"

#include <string>
#include <unistd.h>

namespace dlna {
namespace common {
namespace logging {


LogUtil::LogUtil() : Singleton() {
#ifdef _DEBUG
    this->m_logger = dlna::common::logging::internal::LogFactory::create();

    dlna::common::configuration::Configuration config = dlna::common::configuration::ConfigReader::retrieve();
    this->m_logLevel = dlna::common::logging::internal::LogLevelUtil::valueOf( config.getLogLevel() );
#else
    this->m_logger = nullptr;
    this->m_logLevel = dlna::common::logging::LogLevel::OFF;
#endif
}


LogUtil::~LogUtil() {
#ifdef _DEBUG
    delete this->m_logger;
    this->m_logger = nullptr;
#endif
}


void LogUtil::log(const LogLevel level, const std::string message) const {
#ifdef _DEBUG
    if (this->m_logger != nullptr && level != LogLevel::OFF && !message.empty() && level >= this->m_logLevel) {
        std::string prefix = dlna::common::logging::internal::LogLevelUtil::toString( level );
        pid_t processId = getpid();

        std::string process = "<PID "+std::to_string( processId )+"> ";

        this->m_logger->log(level,  prefix + process + message);
    }
#endif
}


bool LogUtil::isEnabled(const LogLevel level) const {
    return (level <= this->m_logLevel) ? true : false;
}

} /* logging */
} /* common */
} /* dlna */
