#include "stdoutlogger.h"

#include <iostream>

namespace dlna {
namespace common {
namespace logging {
namespace internal {
#ifdef _DEBUG

void StdoutLogger::log(const dlna::common::logging::LogLevel level, const std::string message) const {
    std::cout << message << std::endl;
}

#endif
} /* internal */
} /* logging */
} /* common */
} /* dlna */
