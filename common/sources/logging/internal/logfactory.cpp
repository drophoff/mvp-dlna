#include "logfactory.h"
#include "stdoutlogger.h"
#include "sysloglogger.h"
#include "configreader.h"

namespace dlna {
namespace common {
namespace logging {
namespace internal {
namespace LogFactory {
#ifdef _DEBUG

const Logger* create() {
    dlna::common::configuration::Configuration config = dlna::common::configuration::ConfigReader::retrieve();

    Logger* logger = nullptr;
    if (config.isDaemon()) {
        SyslogLogger* syslog = new SyslogLogger();
        logger = syslog;
    } else {
        StdoutLogger* stdlog = new StdoutLogger();
        logger = stdlog;
    }

    return logger;
}

#endif
} /* LogFactory */
} /* internal */
} /* logging */
} /* common */
} /* dlna */
