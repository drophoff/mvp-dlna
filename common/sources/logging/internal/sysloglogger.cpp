#include "sysloglogger.h"

#include <syslog.h>

namespace dlna {
namespace common {
namespace logging {
namespace internal {
#ifdef _DEBUG

SyslogLogger::SyslogLogger() : Logger() {
    openlog("dlna-daemon", LOG_NOWAIT, LOG_USER);
}

SyslogLogger::~SyslogLogger() {
    closelog();
}

void SyslogLogger::log(const dlna::common::logging::LogLevel level, const std::string message) const {
    int logConstant = -1;

    switch (level) {
    case dlna::common::logging::LogLevel::DEBUG:
        logConstant = LOG_DEBUG;
        break;
    case dlna::common::logging::LogLevel::INFO:
        logConstant = LOG_INFO;
        break;
    case dlna::common::logging::LogLevel::WARN:
        logConstant = LOG_WARNING;
        break;
    case dlna::common::logging::LogLevel::ERROR:
        logConstant = LOG_ERR;
        break;
    case dlna::common::logging::LogLevel::FATAL:
        logConstant = LOG_CRIT;
        break;
    default:
        logConstant = LOG_WARNING;
        break;
    }

    syslog(logConstant, message.c_str());
}

#endif
} /* internal */
} /* logging */
} /* common */
} /* dlna */
