#include <loglevelutil.h>

namespace dlna {
namespace common {
namespace logging {
namespace internal {
namespace LogLevelUtil {
#ifdef _DEBUG

LogLevel valueOf(const std::string logLevel) {
    LogLevel level = LogLevel::ERROR;

    if (logLevel == "TRACE") {
        level = LogLevel::TRACE;
    } else if (logLevel == "DEBUG") {
        level = LogLevel::DEBUG;
    } else if (logLevel == "INFO") {
        level = LogLevel::INFO;
    } else if (logLevel == "WARN") {
        level = LogLevel::WARN;
    } else if (logLevel == "ERROR") {
        level = LogLevel::ERROR;
    } else if (logLevel == "FATAL") {
        level = LogLevel::FATAL;
    } else {
        level = LogLevel::ERROR;
    }

    return level;
}


std::string toString(LogLevel level) {
    std::string returnValue;

    switch (level) {
    case dlna::common::logging::LogLevel::TRACE:
        returnValue = "TRACE";
        break;
    case dlna::common::logging::LogLevel::DEBUG:
        returnValue = "DEBUG";
        break;
    case dlna::common::logging::LogLevel::INFO:
        returnValue = "INFO";
        break;
    case dlna::common::logging::LogLevel::WARN:
        returnValue = "WARN";
        break;
    case dlna::common::logging::LogLevel::ERROR:
        returnValue = "ERROR";
        break;
    case dlna::common::logging::LogLevel::FATAL:
        returnValue = "FATAL";
        break;
    default:
        returnValue =  "N/A";
        break;
    }
    returnValue = "<" + returnValue + "> ";

    return returnValue;
}

#endif
} /* LogLevelUtil */
} /* internal */
} /* logging */
} /* common */
} /* dlna */
