#include "processutil.h"
#include "fatalexception.h"
#include "constant.h"
#include "logutil.h"

#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <csignal>
#include <vector>
#include <memory>
#include <sys/wait.h>

namespace dlna {
namespace common {
namespace daemon {

ProcessUtil::ProcessUtil(std::vector<std::shared_ptr<Task>> tasks) {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Start application");

    /**
     * Register the signals that shall be handled
     */
    this->m_handler.registerSignal(SIGFPE);
    this->m_handler.registerSignal(SIGILL);
    this->m_handler.registerSignal(SIGINT);
    this->m_handler.registerSignal(SIGSEGV);
    this->m_handler.registerSignal(SIGTERM);

    this->m_tasks = tasks;

    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::DEBUG, "Version: " + dlna::common::configuration::Constant::VERSION);
}


ProcessUtil::~ProcessUtil() {
    this->m_tasks.clear();
}


void ProcessUtil::daemonizeProcess() {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::DEBUG, "Mode: Daemonized");

    pid_t pid, sid;

    pid = fork();

    if (pid > 0) {
        /**
         * If the parent process continue with a process ID
         * greater than 0.
         *
         * EXIT SUCCESS
         */
        return;
    } else if (pid < 0) {
        /**
         * A process ID lower than 0 indicates a failure in
         * either process.
         *
         * EXIT FAILURE
         */
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::FATAL, "Fork system call at root process failed");

        throw dlna::common::exception::FatalException();
    }

    /**
     * The parent process has now terminated, and the fored child process will
     * continue (the pid of the child process was 0)
     *
     * Since the child process is a daemon, the umask needs to be set so files
     * and logs can be written.
     */
    umask(0);

    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Successfully started dlna-daemon");

    // Generate a session ID for the child process
    sid = setsid();
    if (sid < 0) {
        /**
         * Generation of the session failed.
         *
         * EXIT FAILURE
         */
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::FATAL, "Could not generate session ID for child process");

        throw dlna::common::exception::FatalException();
    }

    // Change the current working directory to a directory guaranteed to exist
    if ( (chdir("/")) < 0) {
        /**
         * Changing the directory failed
         *
         * EXIT FAILURE
         */
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::FATAL, "Could not change working directory to /");

        throw dlna::common::exception::FatalException();
    }

    /**
     * A daemon cannot use the terminal, so close standard file descriptors
     * for security reasons.
     */
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    // Create corresponding child processes
    startChildProcesses();
}

void ProcessUtil::foregroundProcess() {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::DEBUG, "Mode: Foreground");
    startChildProcesses();
}

void ProcessUtil::startChildProcesses() {
    pid_t parentId = getpid();
    std::vector<pid_t> childPids;

    for (unsigned int child = 0; child < this->m_tasks.size(); child++) {
        pid_t pid = fork();

        if (pid < 0) {
            /**
             * Fork failed
             *
             * EXIT FAILURE
             */
            dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::FATAL, "Fork system call at child process failed");

            throw dlna::common::exception::FatalException();
        } else if (pid == 0) {
            /**
             * Child process
             */
            parentId = 0;
            childPids.clear();

            std::shared_ptr<Task> task = this->m_tasks.at(child);

            dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Start task..." + task->getName());

            task->executeOneTime();
            while (!this->m_handler.receivedExitSignal()) {
                sleep( task->getDelay() );

                if (!this->m_handler.receivedExitSignal()) {
                    task->execute();
                }
            }

            dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Stop task..." + task->getName());

            // Childs should not reenter the loop
            break;
        } else if (pid > 0) {
            /**
             * Parent process
             */
            childPids.push_back( pid );
        }
    }

    if (getpid() == parentId) {
        // Run as long as no exit signal occurs
        while (!this->m_handler.receivedExitSignal()) {
            sleep( 600 );
        }
        // exit all childs in case the parent received the exit signal
        for (unsigned int index = 0; index < childPids.size(); index++) {
            pid_t childId = childPids.at(index);

            kill(childId, SIGTERM );
            waitpid(childId, nullptr, 0);
        }

        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::DEBUG, "Stop application");
    }
}

} /* daemon */
} /* common */
} /* dlna */
