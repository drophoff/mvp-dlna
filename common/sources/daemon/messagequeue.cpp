#include "messagequeue.h"
#include "messageutil.h"
#include "logutil.h"

#include <string>
#include <errno.h>

namespace dlna {
namespace common {
namespace daemon {


std::string MessageQueue::QUEUE_FILE_CHANGE = "/data";


MessageQueue::MessageQueue(std::string queueName, bool isServer, unsigned short queueSize, unsigned short messageSize) :
    m_msg(0),
    m_queue_name(queueName),
    m_isServer(isServer),
    m_queue_max_size(queueSize),
    m_msg_max_size(messageSize) {

    struct mq_attr attr;
    attr.mq_flags = 0;
    attr.mq_maxmsg = 1;
    attr.mq_msgsize = this->m_msg_max_size;

    if (this->m_queue_name.length() <= 13) {
        if (this->m_isServer) {
           this->m_msg = mq_open(this->m_queue_name.c_str(), O_CREAT| O_WRONLY | O_NONBLOCK, 0655, &attr );
        } else {
            this->m_msg = mq_open(this->m_queue_name.c_str(), O_CREAT | O_RDONLY | O_NONBLOCK, 0655, &attr );
        }
        if ( this->m_msg == -1) {
            dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Message Queue \'" + this->m_queue_name + "\' open failure!");
        }
    } else {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Message Queue \'" + this->m_queue_name + "\' name is too long!");
    }
}

MessageQueue::~MessageQueue() {
    if ( this->m_msg != -1 && this->m_msg != 0) {
        int ret = mq_close( this->m_msg );
        if ( ret == -1) {
            dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Message Queue \'" + this->m_queue_name + "\' close failure!");
        }

        ret = mq_unlink(this->m_queue_name.c_str());
        if ( ret == -1 && errno != ENOENT) {
            dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Message Queue \'" + this->m_queue_name + "\' unlink failure!");
        }
    }
}

void MessageQueue::send(Message msg) {
    if (this->m_isServer) {
        std::string message = dlna::common::daemon::internal::MessageUtil::toString( msg );
        if (message.length() <= (unsigned int)this->m_msg_max_size - 1) {
            int ret = mq_send( this->m_msg, message.c_str(), message.length() + 1, 0 );
            if ( ret == -1 && errno != EAGAIN) {
                dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Message Queue \'" + this->m_queue_name + "\' send failure!");
            }
        }
    }
}

bool MessageQueue::receive(Message msg) {
    bool messageReceive = false;

    if (!this->m_isServer) {
        unsigned short maxMessageSize = this->m_msg_max_size + 1;
        char message[maxMessageSize];
        for (int index = 0; index < maxMessageSize; index++) {
             if (index == maxMessageSize - 1) message[index]='\0';
             else message[index]=' ';
        }

        long readBytes = mq_receive( this->m_msg, message, this->m_msg_max_size, nullptr);
        if (readBytes != -1) {
            std::string received(message);
            if (dlna::common::daemon::internal::MessageUtil::toString( msg ) == received) {
                messageReceive = true;
            }
        }
    }

    return messageReceive;
}

} /* daemon */
} /* common */
} /* dlna */
