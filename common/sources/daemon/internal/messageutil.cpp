#include <messageutil.h>

namespace dlna {
namespace common {
namespace daemon {
namespace internal {
namespace MessageUtil {


std::string toString(dlna::common::daemon::Message message) {
    std::string returnValue;

    switch (message) {
    case dlna::common::daemon::Message::NOTIFY:
        returnValue = "NOTIFY";
        break;
    default:
        returnValue =  "N/A";
        break;
    }

    return returnValue;
}

} /* MessageUtil */
} /* internal */
} /* daemon */
} /* common */
} /* dlna */
