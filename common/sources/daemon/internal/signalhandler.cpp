#include "signalhandler.h"

#include <csignal>

namespace dlna {
namespace common {
namespace daemon {
namespace internal {

/**
    Default: false
*/
bool SignalHandler::m_receivedExitSignal = false;


void SignalHandler::handle(int signal) {
    SignalHandler::m_receivedExitSignal = true;
}

void SignalHandler::registerSignal(int signal) const {
    std::signal(signal, &handle);
}

bool SignalHandler::receivedExitSignal() const {
    return m_receivedExitSignal;
}

} /* internal */
} /* daemon */
} /* common */
} /* dlna */


