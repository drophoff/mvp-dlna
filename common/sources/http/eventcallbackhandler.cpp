#include "eventcallbackhandler.h"
#include "logutil.h"

#include <string>

namespace dlna {
namespace common {
namespace http {

std::vector<std::shared_ptr<EventHandler>> EventCallbackHandler::m_handler;


void EventCallbackHandler::registerEventHandler(std::shared_ptr<EventHandler> handler) {
    if (handler != nullptr) {
        EventCallbackHandler::m_handler.push_back( handler );
    }
}


int EventCallbackHandler::handleEvent(Upnp_EventType type, void* event, void* cookie) {
    std::vector<std::shared_ptr<EventHandler>> handler = EventCallbackHandler::m_handler;

    bool eventHasHandled = false;
    for (unsigned int handlerIndex = 0; (handlerIndex < handler.size()) && !eventHasHandled; handlerIndex++) {
        std::shared_ptr<EventHandler> eventHandler = handler.at(handlerIndex);
        bool isResponsibleFor = eventHandler->isResponsibleFor( type );

        if (isResponsibleFor) {
            eventHandler->handle( event );
            eventHasHandled = true;
        }
    }

    if (!eventHasHandled) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Unknown Event: " + std::to_string( type ));
    }

    return UPNP_E_SUCCESS;
}


} /* http */
} /* common */
} /* dlna */
