#include "deviceprovider.h"

namespace dlna {
namespace common {
namespace http {


DeviceProvider::DeviceProvider() : Singleton() {
    UpnpDevice_Handle* handle = new UpnpDevice_Handle();
    this->m_Device = handle;
}

DeviceProvider::~DeviceProvider() {
    delete this->m_Device;
    this->m_Device = nullptr;
}


UpnpDevice_Handle* DeviceProvider::getDevice() const {
    return this->m_Device;
}


} /* http */
} /* common */
} /* dlna */
