#include <dlna/common/processutil.h>
#include <dlna/common/fatalexception.h>
#include <dlna/common/logutil.h>
#include <dlna/common/configreader.h>
#include <dlna/db/processdb.h>
#include <dlna/http/processhttp.h>
#include <dlna/monitor/processmonitor.h>

#include <vector>
#include <memory>
#include <stdlib.h>

/*!
    \mainpage Index Page
    \section section_intro Introduction
    This application provides a minimal content directoy server that is not certified and does not meet the full DLNA specification.
    The application has been created to show the usage of the underlaying libsqlite and libupnp libraries.
    The goal was never to implement a full blown DLNA-compliant server (Media Server).
    The project has a more protypical character.
    Therefore the usage of complex third party libraries like boost are avoided as much as possible in case the implementation effort is not to high.

    An overview about the actual implemented features can be found \ref page_features "here". A list of actual
    todo and the long term road map can be found \ref page_overview_dev "here".

    \tableofcontents

    \subsection section_develoment Development
    <b>Compile</b>
    Execute the following command at the prompt to compile a dynamic linked version of the application: *make*. This command produced a minimal version without any debug symbols. In case debug symbols are
    needed then execute *make debug*. The working folder can be clean up by performing *make clean*.

    This project needs a C++ 11 compatible compiler and the following installed third 
	party libraries:

    Common Name                      | Inst. Package Name | Version | Link                         | Comment
    :----                            | :----              | :----   | :----                        | :----
    Portable SDK for UPnP Devices    | libupnp6-dev       | 1.6     | http://pupnp.sourceforge.net | A version greater than 1.6 is not compatible with previous versions
    SQLite C/C++ Interface           | libsqlite3-dev     | 3.0     | https://www.sqlite.org       | -

    <b>Profile/Memory Checks</b>
    The memory has been checked via *valgrind*. The profiling has been made via
    'valgrind --tool=callgrind ./dlna' followed by 'vcallgrind'.

    <b>Static Code Checks</b>
    The static code check was performed with the clang tool. The corresponding command
    was *scan-build make*. Additional checks are performed via cppcheck. Both checks are
    part of the makefile, which can be executed via 'make analyse'.

    <b>Documentation</b>
    Execute the following command at the prompt to generate the documentation: *make doc*. This
    command uses doxygen to produce a html based documentation. As a result of this doxygen
    (v1.8.8) must be installed in case the command should produce valid html pages.</p>
    Currently the PDF generation is disabled within the doxygen.conf (see GENERATE_LATEX). After 
    the generation of the PDF has been activiated, navigate to the Latex folder and perform 'make'
    to build a single PDF document. Keep in mind that for the PDF generation the optional doxygen 
    pdf package must be installed.

    \subsection section_install Installation
    This section contains all steps to install, configure and run the application.

    <b>Step 1: Binary</b>

    Install the required third party libraries and the above mentioned compiler. Then copy the
    source code to a temporary build folder. Subsequent execute *make* on the command prompt to
    generate the main executeable binary and the corresponding shared libraries (db, http,
    monitor).

    <b>Step 2: Webserver</b>

    Copy the web server content (complete folder 'webserver' ) and the executable binary
    to a desired folder. Make a copy of dlna.conf and place it under /etc. Do not forget
    to copy the shared libraries to /urs/local/lib.

    <b>Step 3: Configuration</b>

    Adjust the following configuration settings: *DAEMON*, *LOG_LEVEL*, *LISTEN* and *PATH*.
    Adjust *PATH* to the installation folder like '/opt/dlna' and *DAEMON* to true in case
    the application should be run as a daemon within this folder. The default *LOG_LEVEL*
    should be set to the value ERROR. DEBUG should only be used to determine failures during
    the development phase because it creates a lot of output. INFO is not so chatty and can be
    used in 'production' enviroments in case DEBUG is not suiteable. A detail description of
    the supported configuration settings can be found \ref page_configuration "here".

    <b>Step 4: Start</b>

    Execute *dlna* without any arguments to start the application. In case the application is
    started as daemon then the corresponding log files can be found in the central syslog file.

    \page page_overview_dev Devlopment (Overview)
    \brief Provides a brief overview of the active development activities.
    \tableofcontents
    \section section_history History

    This section contains a short history of the development process to get a quick overview
    about the project status and the supported (already implemented) features.

    Date                 | Version | Comment
    :----                | :----   | :----
    12. August 2017      | 0.1     | Initial version, browsing (Content Directory Service) working, no retrieval of resources possible.
    15. October 2017     | 0.2     | Provide empty callback to handle the incomming retrieval request (no functionality), rework the documention.
    31. October 2017     | 0.3     | CDS supports JPEG and MP3 files
    05. November 2017    | 0.4     | Implement file change including SystemUpdateID and ContainerUpdateIDs
    12. November 2017    | 0.5     | Add Support for m3u playlists and 32bit systems
    31. December 2017    | 0.6     | Start working on sort and filter functions to finish intel validator tests (minimal set)
    03. March 2018       | 0.7     | Minimum test set for browse range and browse filter passed. Abort the work on filter and search funtion.
    11. March 2018       | 0.8     | Start stabilization and testing phase
    19. July 2018        | 0.9     | Extract information from description.xml instead of constant.h to avoid redundant information
    21. August 2018      | 1.0     | Small refactorings, add missing documentation
    23. November 2018    | 1.1.0   | Bug Fixing (handling of deleted files), update documentation
    28. February 2021    | 1.1.0   | Publication on Gitlab


    \section section_roadmap Road map

    At the end of the development process it is planned that the server supports at least the
    content directory service functions for jpeg based images. This would be version 1.0 from
    the point of view of this project.

    The next flexiable aim would be the support of simple mp3 files, which would be 1.1. With the
    version 1.2 the application should support some search and filter functions (only a subset of
    the functionality defined within the sepcification).

    And the currently not scheduled final aim would be a full compliant media server (CDS), which
    more or less fulfills the DLNA specifications. This means it supports the following formats:
    - Video: MPG, MPEG, M2T, M2TS, MP4, ASF, WMV
    - Music: WAV, MP4, WMA
    - Image: JPG, MPO, ARW
    The end of this implementation results in a version like >= 10.0.

    \section section_todo ToDo's

    The list contains only documentation related tasks. Development related task are documented 
    as Issues on the corresponding Gitlab project.

    Type  | Description 
    :---- | :----
    Doc   | design goals
    Doc   | architecture, layers, component coupling

    \page page_features Features
    \brief Provides an overview of the currently implemented functions.

    The following table shows all implemented functions, their corresponding implementation
    status and a optional description.

    Function               | Status      | Description
    :----                  | :----       | :----
    CDS                    | Done        | Minimal implementaion of the content directory service (no write functionality, no transcoding, no caching, no searching) only navigation and content retrieval.
    JPEG, JPG, PNG and BMP | Done        | Additional formats can be added by adjusting filefilter.h.
    MP3                    | Done        | Same as above row.
    Playlist (M3U)         | Done        | -
    Intel Device Validator | In progress | Skipped: Browse Filter, Browse Search, the other tests were successful.


    \page page_configuration Configuration
    \brief This section describes the supported configuration settings.

    The following table contains all settings that can be defined within '/etc/dlna.conf'.
    The default values are used in case they are not defined within the configuration file.

    Configuration Value | Default Value | Example                                     | Description
    :----               | :----         | :----                                       | :----
    DAEMON              | false         | true, false, yes, no                        | True in case the application should be run as background service with separate process instances, otherwise the application runs in the foreground within the current console.
    PATH                | /opt/dlna     | /home/dlna                                  | The path that contains the executeable of the application.
    LISTEN              | 127.0.0.1     | 10.0.0.4                                    | The IP on which the application should be listen.
    PORT                | 49152         | 50500                                       | The port on which the application should be listen. Only ports above 49152 are considered as valid ports. The default port is used in case no valid port is provided.
    SCAN                | none          | /mnt/smb/music;/mnt/smb/pictures            | A list of folders that should be scanned for media files. Multiple folders are allowed and separated via ';'.
    STRICT              | false         | true, false, yes, no                        | True in case the application should only allow and follow the DLNA protocol. Otherwise, minor deviations from the protocol are allowed. A detail description of the concrete differences of the two strict modes can be found \ref anchor_strict_mode "here".
    LOG_LEVEL           | ERROR         | TRACE, DEBUG, INFO, WARN, ERROR, FATAL, OFF | The different log level that can be used within the application. Off disable the logging of application messages. Error shows all error and fatal messages. The most verbose log level is debug, which includes all log messages.
    SCAN_INTERVAL       | 15            | 60 (1 hour), 1440 (1 day)                   | The time interval in minutes in which to search for changed files. The range for valid values is between 1 and 65535.

    ### DLNA Strict Mode ###
    \anchor anchor_strict_mode
    The following table describes the differences between the two DLNA strict modes. The strict mode 'true' is mainly
    used to passed the intel validation checks. 

    Function           | Strict: True                                                                                                                                                                                                                                                                                                                                                                                              | Strict: False
    :----              | :----                                                                                                                                                                                                                                                                                                                                                                                                     | :----
    Sort Order         | The order of the search results is undefined in case the client provides no information in the 'SortCriteria' attribute wihtin the browse request. Example: The ps3 provides no such sort criteria information. Because of this the order of the results is undefined for this device.                                                                                                                    | Use ascending sort order in case the client device provides no sort order.
    Sort Capabilities  | Only full supported sort capabilities are provided.                                                                                                                                                                                                                                                                                                                                                       | Add 'upnp:originalTrackNumber' as virtual sort order that is mapped to track name for noxon devices.
    Date Time Handling | The 'dc:date' attribute in the browse response contains no time attributes. Only the date information is part of the browse response.                                                                                                                                                                                                                                                                     | The browse response contains the time information.
    Playlist           | The object id of each entry is determined based on the inode index, which is an integer. As a result of this a file within a playlist gets the same id as the referenced original file. The intel upnp validation tool throws an error, because it expects one entry for each content directory entry but this application returns two entries. Due to this the playlist feature is disabled in this mode.| The application supports virtual playlist files.

    \page page_datamodel Data Model
    \brief This section describes the data model that is used within the database module.
    \image html er.svg "Data Model"
    The following table shows the relation between the table, class and header file name.
    Follow the link in the column header file name to get a brief description of the persisted information.

    Table Name      | Class Name      | Header File Name
    :----           | :----           | :----
    PROPERTY        | Property        | property.h
    META_INFO       | MetaInfo        | metainfo.h
    TYPE            | Type            | type.h
    DATA_STRUCTURE  | Data Structure  | datastructure.h

    \page page_design Design Decision
    \brief This section describes all relevant design decisions and provides an
    overview about the architecture of this application.
    \tableofcontents

    \section section_filehandling File Handling
    Each file or folder within the database is identified on the base of the
    unique identification number of the under laying inode. As a consequence of
    this the attribute ID within the table DATA_STRUCTURE is equal to struct
    dirent->d_ino. The attribut PARENT_ID is the unique identification number of
    the parent directory. More information about the structure of the internal
    database can be found \ref page_datamodel "here".

    Because of this approach, files and folders can be easily identfied within
    the internal database even after renaming. Furthermore, the files contained
    in the playlists can be easily referenced, without the use of redundant
    information.

    This approach does not work, when the media files within one folder are
    distributed over several file systems. This could be the case when folder
    'music' contains two child folders like 'a' and 'b'. 'a' is mounted via NFS and
    'b' is mounted via CIFS. Both folders 'a' and 'b' have different parent
    folders on their local file systems. This leads to inconsistent links to
    PARENT_ID within the internal database. As a result of this the back
    navigation at the content directory client can lead to errors.

    \section section_changedetection Change Detection
    The change log status of each file or folder is stored within the CHANGE_LOG flag
    within the table DATA_STRUCTURE (see \ref page_datamodel "Datamodel").
    The change log attribute contains the following values.

    Change Log   | Meaning         | Description
    :----        | :----           | :----
    'C'          | Create          | Represents an new file that does not exists in a previous scan
    'U'          | Update          | Represents an updated file that already exist in a previous scan
    'P'          | Processed       | Represents an existing unchanged file
    'V'          | Virtual         | Represents virtual data structures that does not exists on the file system like the 'root' or the 'playlist' folder
    'I'          | Initial         | Represents a file that has not been processed (file has been deleted)

    A change is detected in the following way. Before each scan process all entries
    within the internal database are marked as initial 'I'. Subsequently, a scan process
    is initiated. All new files gets the flag created 'C'. Changed files that were part 
    of a previous scan will be marked as updated 'U'. Unchanged files were marked as 
    processed 'P'.
    When the scan processed finished all entries marked as initial 'I' gets deleted because
    they have not been changed (updated, created or processed). Data structures marked as 
    virtual 'V' will be ignored. Due to this the internal database contains only new, 
    updated or unchanged entries after a scan process finished. No inital or deleted
    'D' entry should exist, when the internal database has been updated.

    A scan process is triggered via the monitor component that counts the size of each file
    and folder. In case the file size is different than the size of a previous scan the database
    component gets an notification via message driven interprocess communication. The database
    component is responsible to synchronize the local file system with the internal database. The different
    modules and their responsibility are described under the module topic.

    \section section_daemons Daemons vs. Standalone
    The application can be started in two modes. In forground with one process and as 
    a background daemon with four independent processes. The switch between these two
    modes are done via the corresponding configuration parameter that is described
    \ref page_configuration "here" (see DAEMON).

    The application uses the console for logging in case the forground mode is enabled,
    otherwise the file 'syslog.log' is used.

    \section section_mimefile Mime File Handling
    The server works only on the file extensions. There is no check implemented which
    verifies that the contents of a file match its file extention. An overview of the
    supported file extensions can be found \ref page_features "here".


    \section section_transcoding Transcoding/Scaling
    The current version does not support any form of transcoding. The content of
    the requested files are transfered as it is in form of a byte array. Furthermore,
    provides the application no scaling functionality for images or video files.


    @defgroup group_common Common
    \brief This module provides functionality that can be reused from all other modules.

    The module contains general functions for reading configuration settings, error
    handling or application logging.

    @defgroup group_process Process
    \brief This module provides resource independent processes.

    Each module represents an associated standalone Unix process.

    The Unix processes (task.h) have an optional initialization method. If the method exists,
    it is called once. In addition to initialization tasks for the Unix process, the method
    can also be used for any actions that are to be executetd only once.

    Furthermore, every Unix process has a method for recurring tasks. The method is executed
    at constant intervals.

    @defgroup group_db Database
    @ingroup group_process
    \brief This module acts as a central knowledge store 'database'.

    The database contains all file names and their meta information like size and type.
    Furthermore the database contains dynamic properties. This module is the only one
    which writes to the database. All other modules uses only read functions to access
    information from this module.

    @defgroup group_http Http
    @ingroup group_process
    \brief This module contains all HTTP, SOAP and UPnP related functionality and acts
    as a communication end point. It it called 'controlled device' from the UPnP
    architecture point of view (aka server) that provides content directory service
    functionality.

    The module is responsible to answer incomming browse, subscription and property
    requests. It uses the information from the db namespace as source of information
    (read only).

    @defgroup group_monitor Monitor
    @ingroup group_process
    \brief This module monitors whether the file changes and notifies interested processes.

    The dectection of data changes is currently based on the disk space used at the
    directory level. As soon as a change in the directory size is detected, interested
    processes are informed via message driven interprocess communication.
*/
int main(int nargs, char** args) {
    /**
     * Contains the tasks that are executed as independent
     * processes.
     */
    typedef std::shared_ptr<dlna::common::daemon::Task> task;
    std::vector<task> tasks;
    tasks.push_back( task( new dlna::db::ProcessDb() ) );
    tasks.push_back( task( new dlna::http::ProcessHttp() ) );
    tasks.push_back( task( new dlna::monitor::ProcessMonitor() ) );

    try {
        /**
         * The daemon forks the tasks in this vector. Due to this the creation
         * of the tasks is performed before the fork. As a result of this the ctor is
         * called only one-time. The destruction of the task happens after the fork.
         * So the elements in the vectors gets duplicated. So the dtor is called as
         * many times as elements are in the vector of tasks.
         */
        dlna::common::daemon::ProcessUtil util(tasks);

        dlna::common::configuration::Configuration config = dlna::common::configuration::ConfigReader::retrieve();
        if (config.isDaemon()) {
            util.daemonizeProcess();
        } else {
            util.foregroundProcess();
        }
    } catch (dlna::common::exception::FatalException& e) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::FATAL, "Fatal Exception occurred!");

        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
