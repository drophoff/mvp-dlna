#include "dlnabrowsetest.h"

#include <cppunit/XmlOutputter.h>
#include <cppunit/TextTestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>


/**
    This application provides a minimal DLNA compatible client that shows the contents
    of a remote content directory server. The main focus of this application is to show
    the usage of the DLNA client API that is provided via libbrowse.so. Furthermore, the
    application can be used to detect potential memory leaks within the library. The
    library is mainly used by the QT application (see folder named qt).
*/
int main(int nargs, char** args) {
    CppUnit::BriefTestProgressListener progress;

    CppUnit::TestResultCollector collectedResults;

    CppUnit::TestResult testresult;
    testresult.addListener( &collectedResults );
    testresult.addListener( &progress );

    CppUnit::TextTestRunner runner;
    runner.addTest( dlna::test::DlnaBrowseTest::suite() );
    runner.run( testresult );

    std::ofstream outputFile("cppunit.xml");
    CppUnit::XmlOutputter outputter( &collectedResults, outputFile );
    outputter.write();

    return 0;
}
