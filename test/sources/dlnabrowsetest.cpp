#include "dlnabrowsetest.h"

#include <dlna/common/processutil.h>
#include <dlna/common/fatalexception.h>
#include <dlna/common/configreader.h>

#include <string>
#include <stdlib.h>
#include <csignal>
#include <sys/wait.h>


namespace dlna {
namespace test {


void DlnaBrowseTest::setUp() {
    dlna::common::daemon::internal::SignalHandler signaHhandler;
    signaHhandler.registerSignal(SIGFPE);
    signaHhandler.registerSignal(SIGILL);
    signaHhandler.registerSignal(SIGINT);
    signaHhandler.registerSignal(SIGSEGV);
    signaHhandler.registerSignal(SIGTERM);


    this->m_Browser.initialize();
    sleep(10);
}

void DlnaBrowseTest::tearDown() {
}


void DlnaBrowseTest::handleAbstractDataStructure(dlna::client::xml::XmlAbstractDataStructure* object) {
    CPPUNIT_ASSERT_MESSAGE( "Object ID must be equal or greater than zero!", object->getObjectID() >= 0 );
    CPPUNIT_ASSERT_MESSAGE( "Parent ID must be equal or greater than zero!", object->getParentID() >= -1 );
    CPPUNIT_ASSERT_MESSAGE( "Title must be filled!", !object->getTitle().empty() );
}


void DlnaBrowseTest::handleAbstractDataStructure(dlna::client::xml::XmlContainer* container) {
     handleAbstractDataStructure((dlna::client::xml::XmlAbstractDataStructure*)container);

     CPPUNIT_ASSERT_MESSAGE( "Child Count must be equal or greater than zero!", container->getChildCount() >= 0 );
     CPPUNIT_ASSERT_MESSAGE( "Type must be filled!", !container->getType().empty() );
}


void DlnaBrowseTest::handleAbstractDataStructure(dlna::client::xml::XmlItem* item) {
     handleAbstractDataStructure((dlna::client::xml::XmlAbstractDataStructure*)item);

     CPPUNIT_ASSERT_MESSAGE( "Resource URI must be filled!", !item->getResourceURI().empty() );
     CPPUNIT_ASSERT_MESSAGE( "Type must be filled!", !item->getType().empty() );
     CPPUNIT_ASSERT_MESSAGE( "Protocol must be filled!", !item->getProtocol().empty() );
}


void DlnaBrowseTest::handleDataStructure(std::vector<std::shared_ptr<dlna::client::xml::XmlAbstractDataStructure>>& dataStructures ) {
    for (auto dataStructureIterator = dataStructures.begin(); dataStructureIterator != dataStructures.end(); dataStructureIterator++) {
        std::shared_ptr<dlna::client::xml::XmlAbstractDataStructure> abstractDataStructure = *dataStructureIterator;
        if (abstractDataStructure.get() != nullptr) {
            handleAbstractDataStructure( abstractDataStructure.get() );
        }
    }
}


void DlnaBrowseTest::handleMetainformation(dlna::client::xml::XmlBrowseResult& browseResult) {
    CPPUNIT_ASSERT_MESSAGE( "Number Returned must be equal or greater than zero!", browseResult.getNumberReturned() >= 0 );
    CPPUNIT_ASSERT_MESSAGE( "Total Matches must be equal or greater than zero!", browseResult.getTotalMatches() >= 0 );
    CPPUNIT_ASSERT_MESSAGE( "Update ID must be equal or greater than zero!", browseResult.getUpdateID() >= 0 );
}


void DlnaBrowseTest::handleBrowseResult(dlna::client::xml::XmlBrowseResult& browseResult) {
    handleMetainformation( browseResult );

    std::vector<std::shared_ptr<dlna::client::xml::XmlAbstractDataStructure>> dataStructure = browseResult.getDataStructure();
    handleDataStructure( dataStructure );
}


void DlnaBrowseTest::performRequests(const dlna::client::store::Device& device, dlna::client::xml::XmlBrowseResult& browseResult) {
    std::vector<std::shared_ptr<dlna::client::xml::XmlAbstractDataStructure>> dataStructure = browseResult.getDataStructure();

    for (auto dataStructureIterator = dataStructure.begin(); dataStructureIterator != dataStructure.end(); dataStructureIterator++) {
        std::shared_ptr<dlna::client::xml::XmlAbstractDataStructure> abstractDataStructure = *dataStructureIterator;

        if (abstractDataStructure->getType() == "CNT") {
            // Browse Meta Data
            dlna::client::Request request( device );
            request.setObjectID( abstractDataStructure->getObjectID() );

            dlna::client::Response subMetaDataResponse = this->m_Browser.browse( request );
            dlna::client::xml::XmlBrowseResult subMetaDataBrowseResult = subMetaDataResponse.getResult();
            handleBrowseResult( subMetaDataBrowseResult );

            // Browse Direct Children
            request.setData( true );

            dlna::client::Response subDirectDataResponse = this->m_Browser.browse( request );
            dlna::client::xml::XmlBrowseResult subDirectDataBrowseResult = subDirectDataResponse.getResult();
            handleBrowseResult( subDirectDataBrowseResult );

            performRequests( device, subDirectDataBrowseResult );
       }
    }
}


void DlnaBrowseTest::testBrowse() {
    try {
        // Retrieve all available devices
        const std::vector<dlna::client::Response> availableDeviceList = this->m_Browser.retrieveDeviceList();
        CPPUNIT_ASSERT_MESSAGE( "Expect at least one device!", !availableDeviceList.empty() );

        for (auto deviceIterator = availableDeviceList.begin(); deviceIterator != availableDeviceList.end(); deviceIterator++) {
            dlna::client::xml::XmlBrowseResult browseResult = deviceIterator->getResult();
            // Check browse result for the root device (Meta Data)
            handleBrowseResult( browseResult );
            // Iterate through the browse result
            performRequests(deviceIterator->getDevice(), browseResult);
        }
    } catch (dlna::common::exception::FatalException& e) {
        CPPUNIT_FAIL( "Unexpected exception occurred!" );
    }
}

} /* test */
} /* dlna */
