#ifndef BROWSE_TEST_H
#define BROWSE_TEST_H

#include <dlna/client/xmlcontainer.h>
#include <dlna/client/xmlitem.h>
#include <dlna/client/response.h>
#include <dlna/client/contentdirectorybrowser.h>

#include <vector>
#include <memory>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>


namespace dlna {
namespace test {

class DlnaBrowseTest : public CppUnit::TestFixture {
public:
  void setUp();

  void tearDown();

  void testBrowse();

  CPPUNIT_TEST_SUITE( DlnaBrowseTest );
  CPPUNIT_TEST( testBrowse );
  CPPUNIT_TEST_SUITE_END();

private:
  void handleAbstractDataStructure(dlna::client::xml::XmlAbstractDataStructure* object);

  void handleAbstractDataStructure(dlna::client::xml::XmlContainer* container);

  void handleAbstractDataStructure(dlna::client::xml::XmlItem* item);

  void handleDataStructure(std::vector<std::shared_ptr<dlna::client::xml::XmlAbstractDataStructure>>& dataStructures );

  void handleMetainformation(dlna::client::xml::XmlBrowseResult& browseResult);

  void handleBrowseResult(dlna::client::xml::XmlBrowseResult& browseResult);

  void performRequests(const dlna::client::store::Device& device, dlna::client::xml::XmlBrowseResult& browseResult);

  dlna::client::ContentDirectoryBrowser m_Browser;
};

} /* test */
} /* dlna */
#endif /* BROWSE_TEST_H */
