OPTIONS=-plist -analyze-headers -o reports \
-enable-checker alpha.core.BoolAssignment \
-enable-checker alpha.core.CallAndMessageUnInitRefArg \
-enable-checker alpha.core.CastSize \
-enable-checker alpha.core.FixedAddr \
-enable-checker alpha.core.IdenticalExpr \
-enable-checker alpha.core.PointerArithm \
-enable-checker alpha.core.PointerSub \
-enable-checker alpha.core.SizeofPtr \
-enable-checker alpha.core.TestAfterDivZero \
-enable-checker alpha.cplusplus.VirtualCall \
-enable-checker alpha.deadcode.UnreachableCode \
-enable-checker alpha.security.ArrayBound \
-enable-checker alpha.security.ArrayBoundV2 \
-enable-checker alpha.security.MallocOverflow \
-enable-checker alpha.security.ReturnPtrRange \
-enable-checker alpha.security.taint.TaintPropagation \
-enable-checker alpha.unix.Chroot \
-enable-checker alpha.unix.PthreadLock \
-enable-checker alpha.unix.SimpleStream \
-enable-checker alpha.unix.Stream \
-enable-checker alpha.unix.cstring.BufferOverlap \
-enable-checker alpha.unix.cstring.NotNullTerminated \
-enable-checker alpha.unix.cstring.OutOfBounds \
-enable-checker llvm.Conventions \
-enable-checker security.FloatLoopCounter \
-enable-checker security.insecureAPI.rand \
-enable-checker security.insecureAPI.strcpy


all:
			(cd common; make; make install)
			(cd db; make; make install)
			(cd monitor; make; make install)
			(cd http; make; make install)
			(cd server; make)


clean:
			(cd common; make uninstall; make clean)
			(cd db; make uninstall; make clean)
			(cd monitor; make uninstall; make clean)
			(cd http; make uninstall; make clean)
			(cd server; make clean)
			(cd client; make uninstall; make clean)
			(cd test; make clean)


debug:
			(cd common; make debug; make install)
			(cd db; make debug; make install)
			(cd monitor; make debug; make install)
			(cd http; make debug; make install)
			(cd server; make debug)


analyse:
			(cd common; scan-build-3.8 ${OPTIONS} make debug; make install; make analyse)
			(cd db; scan-build-3.8 ${OPTIONS} make debug; make install; make analyse)
			(cd monitor; scan-build-3.8 ${OPTIONS} make debug; make install; make analyse)
			(cd http; scan-build-3.8 ${OPTIONS} make debug; make install; make analyse)
			(cd server; scan-build-3.8 ${OPTIONS} make debug; make analyse)

tests:
			(cd client; scan-build-3.8 ${OPTIONS} make debug; make install; make analyse)
			(cd test; scan-build-3.8 ${OPTIONS} make debug; make analyse)

doc:
			(cd server; make doc)
