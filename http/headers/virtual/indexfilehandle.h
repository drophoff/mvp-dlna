#ifndef INDEX_FILE_HANDLE_H
#define INDEX_FILE_HANDLE_H

#include "abstractfilehandle.h"

#include <string>
#include <sstream>

namespace dlna {
namespace http {
namespace virdir {

/**
 * \file indexfilehandle.h
 * \ingroup group_http
 */

class IndexFileHandle : public AbstractFileHandle {
public:
  IndexFileHandle(std::string version, std::string date);

private:
  std::stringstream m_index;
};

} /* virdir */
} /* http */
} /* dlna */
#endif /* INDEX_FILE_HANDLE_H */
