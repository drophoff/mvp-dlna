#ifndef CDS_FILE_HANDLE_H
#define CDS_FILE_HANDLE_H

#include "abstractfilehandle.h"

#include <string>
#include <fstream>

namespace dlna {
namespace http {
namespace virdir {

/**
 * \file cdsfilehandle.h
 * \ingroup group_http
 */

class CDSFileHandle : public AbstractFileHandle {
public:
  CDSFileHandle(std::string filename);

  ~CDSFileHandle();

private:
  std::ifstream m_file;
};

} /* virdir */
} /* http */
} /* dlna */
#endif /* CDS_FILE_HANDLE_H */
