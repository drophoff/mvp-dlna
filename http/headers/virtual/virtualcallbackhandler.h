#ifndef VIRTUAL_CALLBACK_HANDLER_H
#define VIRTUAL_CALLBACK_HANDLER_H

#include <upnp/upnp.h>

namespace dlna {
namespace http {
namespace virdir {

/**
 * \file virtualcallbackhandler.h
 * \ingroup group_http
 */

class VirtualCallbackHandler {
public:

  static struct UpnpVirtualDirCallbacks* handleVirtual();

private:
  static int webserver_get_info(const char* filename, struct File_Info* info);


  static UpnpWebFileHandle webserver_open(const char* filename, enum UpnpOpenFileMode mode);


  static int webserver_read(UpnpWebFileHandle fh, char* buf, size_t buflen);


  static int webserver_write(UpnpWebFileHandle fh, char* buf, size_t buflen);


  static int webserver_seek(UpnpWebFileHandle fh, off_t offset, int origin);


  static int webserver_close(UpnpWebFileHandle fh);
};

} /* virdir */
} /* http */
} /* dlna */
#endif /* VIRTUAL_CALLBACK_HANDLER_H */
