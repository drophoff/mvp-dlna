#ifndef INDEX_FILE_PROVIDER_H
#define INDEX_FILE_PROVIDER_H

#include "fileprovider.h"

namespace dlna {
namespace http {
namespace virdir {

/**
 * \file indexfileprovider.h
 * \ingroup group_http
 */

class IndexFileProvider : public FileProvider {
public:

  int getFileInfo(const char* filename, struct File_Info *info);

  AbstractFileHandle* createFileHandle(const char* filename);
};

} /* virdir */
} /* http */
} /* dlna */
#endif /* INDEX_FILE_PROVIDER_H */
