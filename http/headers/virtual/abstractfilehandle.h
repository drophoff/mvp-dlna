#ifndef ABSTRACT_FILE_HANDLE_H
#define ABSTRACT_FILE_HANDLE_H

#include <upnp/upnp.h>
#include <istream>

namespace dlna {
namespace http {
namespace virdir {

/**
 * \file abstractfilehandle.h
 * \ingroup group_http
 */

class AbstractFileHandle {
public:
  AbstractFileHandle(std::istream& stream);

  virtual ~AbstractFileHandle() = 0;

  /**
   * Reads the given length into the provided buffer.
   *
   * Returns zero in case there is no more data
   * Returns a value less than zero to indicate an error
   * Otherwise on success the number of read bytes are returned.
   */
  int read(char* buffer, size_t bufferLength);

  /**
   * Seeks within the data given be the offset and direction (origin).
   * The direction can be relative and contains the values for CURRENT,
   * END and SET.
   *
   * Returns zero on success, otherwise a non-zero value is returned,
   * when an error occurred.
   *
   */
  int seek(off_t offset, int origin);

private:
  std::istream& m_stream;
};

} /* virdir */
} /* http */
} /* dlna */
#endif /* ABSTRACT_FILE_HANDLE_H */
