#ifndef FILE_PROVIDER_FACTORY_H
#define FILE_PROVIDER_FACTORY_H

#include "fileprovider.h"
#include <memory>

namespace dlna {
namespace http {
namespace virdir {

/**
 * \file fileproviderfactory.h
 * \ingroup group_http
 */

class FileProviderFactory {
public:

  std::unique_ptr<FileProvider> create(const char* filename);
};

} /* virdir */
} /* http */
} /* dlna */
#endif /* FILE_PROVIDER_FACTORY_H */
