#ifndef CDS_FILE_PROVIDER_H
#define CDS_FILE_PROVIDER_H

#include "fileprovider.h"

namespace dlna {
namespace http {
namespace virdir {

/**
 * \file cdsfileprovider.h
 * \ingroup group_http
 */

class CDSFileProvider : public FileProvider {
public:

  int getFileInfo(const char* filename, struct File_Info *info);

  AbstractFileHandle* createFileHandle(const char* filename);
};

} /* virdir */
} /* http */
} /* dlna */
#endif /* CDS_FILE_PROVIDER_H */
