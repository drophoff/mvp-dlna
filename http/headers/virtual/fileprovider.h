#ifndef FILE_PROVIDER_H
#define FILE_PROVIDER_H

#include "abstractfilehandle.h"
#include <upnp/upnp.h>

namespace dlna {
namespace http {
namespace virdir {

/**
 * \file fileprovider.h
 * \ingroup group_http
 */

class FileProvider {
public:

  virtual int getFileInfo(const char* filename, struct File_Info *info) = 0;

  virtual AbstractFileHandle* createFileHandle(const char* filename) = 0;
};

} /* virdir */
} /* http */
} /* dlna */
#endif /* FILE_PROVIDER_H */
