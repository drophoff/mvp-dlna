#ifndef UPNP_NOTIFICATION_H
#define UPNP_NOTIFICATION_H

#include <string>

namespace dlna {
namespace http {
namespace upnp {

/**
 * \file notification.h
 * \ingroup group_http
 */

class Notification {
public:
  Notification();

  void send();

private:
  std::string findProperty(std::string name);

  void checkAndSendContainerUpdate();

  void checkAndSendSystemUpdate();

  void sendNotification(std::string variableName, std::string variableValue) const;

  std::string m_systemUpdateID;

  std::string m_containerUpdateIDs;
};

} /* upnp */
} /* http */
} /* dlna */
#endif /* UPNP_NOTIFICATION_H */
