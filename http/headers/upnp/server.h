#ifndef UPNP_SERVER_H
#define UPNP_SERVER_H

#include <upnp/upnp.h>

namespace dlna {
namespace http {
namespace upnp {

/**
 * \file server.h
 * \ingroup group_http
 */

class Server {
public:
  /**
   * Default constructor that intialize the underlaying
   * UPnP subsystem.
   */
  Server();


  /**
   * Default destructor that frees all used resources from
   * the underlaying UPnP subsystem.
   */
  ~Server();


  /**
   * Sends an advertisement packet via the first found
   * interface that has a corresponding gateway for routing.
   * \param expirationAge of the advertisement in seconds
   */
  void sendAdvertisement(int expirationAge) const;
};

} /* upnp */
} /* http */
} /* dlna */
#endif /* UPNP_SERVER_H */
