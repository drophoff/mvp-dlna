#ifndef BROWSE_REQUEST_H
#define BROWSE_REQUEST_H

#include "xmlbrowserequestparameter.h"

#include <dlna/common/eventhandler.h>
#include <dlna/db/searchwindow.h>
#include <dlna/db/searchsorting.h>
#include <dlna/db/searchcriterion.h>

#include <upnp/upnp.h>
#include <memory>

namespace dlna {
namespace http {
namespace request {

/**
 * \file browserequest.h
 * \ingroup group_http
 */

class BrowseRequest : public dlna::common::http::EventHandler {
public:
  bool isResponsibleFor(Upnp_EventType event);

  
  void handle(void* event);

private:
  void handleGetSearchCapabilities(struct Upnp_Action_Request* request);


  void handleGetSortCapabilities(struct Upnp_Action_Request* request);


  void handleGetSystemUpdateID(struct Upnp_Action_Request* request);


  void handleRequest(struct Upnp_Action_Request* request, bool isBrowse);


  std::shared_ptr<dlna::db::datastructure::entity::SearchWindowDS> createWindow(const dlna::http::xml::XmlBrowseRequestParameter * const parameter);


  std::shared_ptr<dlna::db::datastructure::entity::SearchCriterionDS> createCriterion(const dlna::http::xml::XmlBrowseRequestParameter * const parameter);


  std::shared_ptr<dlna::db::datastructure::entity::SearchSortingDS> createSorting(const dlna::http::xml::XmlBrowseRequestParameter * const parameter);


  void handleError(struct Upnp_Action_Request* request);
};

} /* request */
} /* http */
} /* dlna */
#endif /* BROWSE_REQUEST_H */
