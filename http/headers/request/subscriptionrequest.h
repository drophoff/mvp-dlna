#ifndef SUBSCRIPTION_REQUEST_H
#define SUBSCRIPTION_REQUEST_H

#include <dlna/common/eventhandler.h>

#include <upnp/upnp.h>

namespace dlna {
namespace http {
namespace request {

/**
 * \file subscriptionrequest.h
 * \ingroup group_http
 */

class SubscriptionRequest : public dlna::common::http::EventHandler {
public:
  bool isResponsibleFor(Upnp_EventType event);


  void handle(void* event);
};

} /* request */
} /* http */
} /* dlna */
#endif /* SUBSCRIPTION_REQUEST_H */
