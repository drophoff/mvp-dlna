#ifndef PROCESS_HTTP_H
#define PROCESS_HTTP_H

#include "server.h"
#include "notification.h"

#include <dlna/db/transactionmanager.h>
#include <dlna/common/task.h>

#include <upnp/upnp.h>

namespace dlna {
namespace http {

/**
 * \file processhttp.h
 * \ingroup group_http
 */

/**
 * This class is responsible to initialize the UPnP (libupnp)
 * subsystem and receive and answer the corresponding http request.
 */
class ProcessHttp : public dlna::common::daemon::Task {
public:
  /**
   * Default constructor
   */
  ProcessHttp();

  
  /**
   * Default destructor
   */
  virtual ~ProcessHttp();


  /**
   * Execute HTTP related actions like answer browse requests,
   * send advertisements and property change events to subscribers.
   */
  void execute();


  /**
   * Initialize the UPnP subsystem
   */
  void executeOneTime();


  unsigned short getDelay() const;


  const std::string getName() const;

private:
  /**
   * Contains a reference to the UPnP subsystem in a role
   * of a controlled device (server).
   */
  dlna::http::upnp::Server* m_upnpSubsystem;

  dlna::http::upnp::Notification* m_notification;

  dlna::db::TxManager* m_transactionManager;

  /**
   * Contains the number how often the executeOneTime function
   * has been called. The counter will be reseted to zero in
   * case he reached a defined max value.
   */
  int m_called;
};

} /* http */
} /* dlna */
#endif /* PROCESS_HTTP_H */
