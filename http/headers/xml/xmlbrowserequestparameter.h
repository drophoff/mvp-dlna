#ifndef XML_BROWSE_REQUEST_PARAMETER_H
#define XML_BROWSE_REQUEST_PARAMETER_H

#include <string>

namespace dlna {
namespace http {
namespace xml {

/**
 * \file xmlbrowserequestparameter.h
 * \ingroup group_common
 */


/**
 * This class contains all parameters that are part of a 'Browse' request.
 */
class XmlBrowseRequestParameter {
public:
  /**
   * Default constructor
   */
  XmlBrowseRequestParameter();

  /**
   * Returns true in case the request is from type browse meta data,
   * otherwise false.
   */
  bool isBrowseMetaData() const;

  /**
   * Used to set the corresponding browse meta data flag.
   */
  void setBrowseMetaData(bool isBrowseMetaData);

  /**
   * Returns true in case the request is from type browse direct
   * children, otherwise false.
   */
  bool isBrowseDirectChildren() const;

  /**
   * Used to set the corresponding browse direct children flag.
   */
  void setBrowseDirectChildren(bool isBrowseDirectChildren);

  /**
   * Returns the object id from the browse request.
   */
  long int getObjectId() const;

  /**
   * Used to set the corresponding object id
   */
  void setObjectId(long int objectId);

  /**
   * Returns the starting index of the browse request.
   */
  long int getStartingIndex() const;

  /**
   * Used to set the corresponding starting index
   */
  void setStartingIndex(long int startingIndex);

  /**
   * Returns the number of requested items.
   */
  long int getRequestedCount() const;

  /**
   * Used to set the corresponding requested item count
   */
  void setRequestedCount(long int requestedCount);

  /**
   * Returns the filter criteria.
   */
  std::string getFilter() const;

  /**
   * Used to set the corresponding filter criteria.
   */
  void setFilter(std::string filter);

  /**
   * Returns the search criteria.
   */
  std::string getCriterion() const;

  /**
   * Used to set the corresponding search criteria.
   */
  void setCriterion(std::string criterion);

  /**
   * Returns the sort criteria.
   */
  std::string getSort() const;

  /**
   * Used to set the corresponding sort criteria.
   */
  void setSort(std::string criterion);

private:
  /**
   * Contains true in case the current request is from type browse meta
   * data, otherwise false.
   */
  bool m_isBrowseMetaData;

  /**
   * Contains true in case the current request is from type browse
   * direct children, otherwise false.
   */
  bool m_isBrowseDirectChildren;

  /**
   * Contains the object id from the current request. The object id identifies
   * the root for which the corresponding data shall be retrieved.
   */
  long int m_objectId;

  /**
   * Contains the starting index from the request. The parameter is used for
   * pagination (selection window) to retrieve only a subset of the requested
   * data. The starting index defines the start point (offset) of the selection
   * windows.
   */
  long int m_startingIndex;

  /**
   * Contains the number of items that are requested. The requested count is
   * added to the starting index to define the end point for the selection window.
   */
  long int m_requestedCount;

  /**
   * Contains the filter criteria that should be applied to the requested
   * result.
   */
  std::string m_filter;

  /**
   * Contains the sorting criteria that should be applied to the requested
   * result.
   */
  std::string m_sort;

  /**
   * Contains the search criteria that should be applied to the requested
   * result.
   */
  std::string m_criterion;
};

} /* xml */
} /* http */
} /* dlna */
#endif /* XML_BROWSE_REQUEST_PARAMETER_H */
