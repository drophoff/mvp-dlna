#ifndef XML_BROWSE_PARSER_H
#define XML_BROWSE_PARSER_H

#include <dlna/common/xmlabstractparser.h>

#include "xmlbrowserequestparameter.h"

namespace dlna {
namespace http {
namespace xml {

/**
 * \file xmlbrowseparser.h
 * \ingroup group_http
 */

/**
 * This class provides functionality to parse the parameter
 * of a 'Browse' request.
 */
class XmlBrowseParser : public dlna::common::xml::XmlAbstractParser<XmlBrowseRequestParameter> {
public:
  /**
   * Default constructor
   */
  XmlBrowseParser(IXML_Document* document);

  /**
   * Default destructor
   */
  virtual ~XmlBrowseParser();

private:
  void handleElement(IXML_Node* node);

  void handleElementValue(std::string name);

  bool handleResult();
};

} /* xml */
} /* http */
} /* dlna */
#endif /* XML_BROWSE_PARSER_H */
