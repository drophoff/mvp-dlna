#include "xmlbrowseparser.h"

#include <stdlib.h>

namespace dlna {
namespace http {
namespace xml {

XmlBrowseParser::XmlBrowseParser(IXML_Document* document) :
    XmlAbstractParser<XmlBrowseRequestParameter>(document) {
}

XmlBrowseParser::~XmlBrowseParser() {
}

void XmlBrowseParser::handleElement(IXML_Node* node) {
}

void XmlBrowseParser::handleElementValue(std::string value) {
    if (getElementName() == "BrowseFlag" ) {
        if (value == "BrowseMetadata") this->m_T.setBrowseMetaData(true);
        if (value == "BrowseDirectChildren") this->m_T.setBrowseDirectChildren(true);
    }

    if (getElementName() == "ObjectID") {
        long int objectId = atol( value.c_str() );
        this->m_T.setObjectId( objectId );
    }

    if (getElementName() == "StartingIndex") {
        long int startingIndex = atol( value.c_str() );
        this->m_T.setStartingIndex( startingIndex );
    }

    if (getElementName() == "RequestedCount") {
        long int requestedCount = atol( value.c_str() );
        this->m_T.setRequestedCount( requestedCount );
    }

    if (getElementName() == "Filter") {
        this->m_T.setFilter( value );
    }

    if (getElementName() == "SearchCriteria") {
        this->m_T.setCriterion( value );
    }

    if (getElementName() == "SortCriteria") {
        this->m_T.setSort( value );
    }
}

bool XmlBrowseParser::handleResult() {
    return true;
}

} /** xml **/
} /** http **/
} /** dlna **/