#include "xmlbrowserequestparameter.h"

namespace dlna {
namespace http {
namespace xml {

XmlBrowseRequestParameter::XmlBrowseRequestParameter() :
    m_isBrowseMetaData(false),
    m_isBrowseDirectChildren(false),
    m_objectId(-1),
    m_startingIndex(0),
    m_requestedCount(0),
    m_filter("*"),
    m_sort(""),
    m_criterion("") {
}


bool XmlBrowseRequestParameter::isBrowseMetaData() const {
    return this->m_isBrowseMetaData;
}

void XmlBrowseRequestParameter::setBrowseMetaData(const bool isBrowseMetaData) {
    this->m_isBrowseMetaData=isBrowseMetaData;
}


bool XmlBrowseRequestParameter::isBrowseDirectChildren() const {
    return this->m_isBrowseDirectChildren;
}

void XmlBrowseRequestParameter::setBrowseDirectChildren(const bool isBrowseDirectChildren) {
    this->m_isBrowseDirectChildren=isBrowseDirectChildren;
}

long int XmlBrowseRequestParameter::getObjectId() const {
    return this->m_objectId;
}

void XmlBrowseRequestParameter::setObjectId(const long int objectId) {
    this->m_objectId=objectId;
}

long int XmlBrowseRequestParameter::getStartingIndex() const {
    return this->m_startingIndex;
}

void XmlBrowseRequestParameter::setStartingIndex(const long int startingIndex) {
    this->m_startingIndex=startingIndex;
}

long int XmlBrowseRequestParameter::getRequestedCount() const {
    return this->m_requestedCount;
}

void XmlBrowseRequestParameter::setRequestedCount(const long int requestedCount) {
    this->m_requestedCount=requestedCount;
}

std::string XmlBrowseRequestParameter::getFilter() const {
    return this->m_filter;
}

void XmlBrowseRequestParameter::setFilter(std::string filter) {
    this->m_filter=filter;
}

std::string XmlBrowseRequestParameter::getCriterion() const {
    return this->m_criterion;
}

void XmlBrowseRequestParameter::setCriterion(std::string criterion) {
    this->m_criterion=criterion;
}

std::string XmlBrowseRequestParameter::getSort() const {
    return this->m_sort;
}

void XmlBrowseRequestParameter::setSort(std::string sort) {
    this->m_sort=sort;
}

} /* xml */
} /* http */
} /* dlna */
