#include "server.h"
#include "subscriptionrequest.h"
#include "virtualcallbackhandler.h"
#include "browserequest.h"

#include <dlna/common/deviceprovider.h>
#include <dlna/common/eventcallbackhandler.h>
#include <dlna/common/constant.h>
#include <dlna/common/logutil.h>
#include <dlna/common/fatalexception.h>
#include <dlna/common/configreader.h>


namespace dlna {
namespace http {
namespace upnp {


Server::Server() {
    /**
     * Initialize the UPnP subsystem
     */
    dlna::common::configuration::Configuration config = dlna::common::configuration::ConfigReader::retrieve();
    int ret = UpnpInit(config.getIP().c_str(), config.getPort());
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::FATAL, "Cannot initialize UPnP subsystem");

        throw dlna::common::exception::FatalException();
    }

    ret = UpnpSetMaxContentLength(dlna::common::configuration::Constant::MAX_CONTENT_LENGTH);
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::WARN, "Cannot increase max content size!");
    }

    /**
     * Enable the internal web service
     */
    std::string webserver = config.getPath();
    webserver += dlna::common::configuration::Constant::PHYSICAL_WEB_DIR;

    ret = UpnpSetWebServerRootDir( webserver.c_str() );
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::FATAL, "Cannot set web root dir ");

        throw dlna::common::exception::FatalException();
    }

    /**
     * Configure virtual directory callbacks
     */
    char* ipAddress = UpnpGetServerIpAddress();
    unsigned short currentPort = UpnpGetServerPort();

    std::string description;
    description  = "http://";;
    description += ipAddress;
    description += ":";
    description += std::to_string(currentPort);
    description += "/";
    description += "description.xml";

    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::DEBUG, "Publish service description at " + description);

    typedef std::shared_ptr<dlna::common::http::EventHandler> eventHandler;
    dlna::common::http::EventCallbackHandler::registerEventHandler( eventHandler( new dlna::http::request::BrowseRequest() ));
    dlna::common::http::EventCallbackHandler::registerEventHandler( eventHandler( new dlna::http::request::SubscriptionRequest() ));

    ret = UpnpRegisterRootDevice( description.c_str(), &dlna::common::http::EventCallbackHandler::handleEvent, nullptr, dlna::common::http::DeviceProvider::getInstance().getDevice());
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::FATAL, "Cannot register root device");

        throw dlna::common::exception::FatalException();
    }

    ret = UpnpSetVirtualDirCallbacks( dlna::http::virdir::VirtualCallbackHandler::handleVirtual() );
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::FATAL, "Cannot register virtual directory callbacks");

        throw dlna::common::exception::FatalException();
    }

    std::string virtualDir = dlna::common::configuration::Constant::VIRTUAL_CDS_DIR;
    ret = UpnpAddVirtualDir( virtualDir.c_str() );
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::FATAL, "Cannot register virtual cds directory");

        throw dlna::common::exception::FatalException();
    }

    virtualDir = dlna::common::configuration::Constant::VIRTUAL_WEB_DIR;
    ret = UpnpAddVirtualDir( virtualDir.c_str() );
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::FATAL, "Cannot register virtual web directory");

        throw dlna::common::exception::FatalException();
    }

    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Internal UPnP subsystem initialized");
}


Server::~Server() {
    int ret = UpnpUnRegisterRootDevice(*dlna::common::http::DeviceProvider::getInstance().getDevice());
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Cannot unregister root device");
    }

    ret = UpnpFinish();
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Cannot stop UPnP subsystem");
    }

    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Internal UPnP subsystem stopped");
}

void Server::sendAdvertisement(int expirationAge) const {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Send Advertisement");

    /**
     * Send advertisements and use the internal default expire time
     */
    int expiration = (expirationAge >= 0) ? expirationAge : 1800;
    int ret = UpnpSendAdvertisement(*dlna::common::http::DeviceProvider::getInstance().getDevice(), expiration);
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Cannot send advertisement");

        throw dlna::common::exception::FatalException();
    }
}

} /* upnp */
} /* http */
} /* dlna */
