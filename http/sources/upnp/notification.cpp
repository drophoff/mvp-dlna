#include "notification.h"

#include <dlna/db/dbreadservice.h>
#include <dlna/common/xmlutil.h>
#include <dlna/common/deviceprovider.h>
#include <dlna/common/logutil.h>
#include <dlna/common/constant.h>
#include <dlna/common/descriptionreader.h>

#include <string>
#include <upnp/upnp.h>
#include <upnp/upnptools.h>

namespace dlna {
namespace http {
namespace upnp {

Notification::Notification() :
    m_systemUpdateID("0"),
    m_containerUpdateIDs("") {
}

void Notification::send() {
    checkAndSendSystemUpdate();
    checkAndSendContainerUpdate();
}


void Notification::checkAndSendContainerUpdate() {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Handle Notification: Container Update ID");
    std::string containerUpdateIDs = findProperty( dlna::common::configuration::Constant::PROPERTY_CONTAINER_UPDATE_ID );

    if (this->m_containerUpdateIDs != containerUpdateIDs) {
        this->m_containerUpdateIDs = containerUpdateIDs;

        sendNotification(dlna::common::configuration::Constant::PROPERTY_CONTAINER_UPDATE_ID, this->m_containerUpdateIDs);
    }
}

void Notification::checkAndSendSystemUpdate() {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Handle Notification: System Update");
    std::string systemUpdateID = findProperty( dlna::common::configuration::Constant::PROPERTY_SYSTEM_UPDATE_ID );

    if (this->m_systemUpdateID != systemUpdateID) {
        this->m_systemUpdateID = systemUpdateID;

        sendNotification(dlna::common::configuration::Constant::PROPERTY_SYSTEM_UPDATE_ID, this->m_systemUpdateID);
    }
}


void Notification::sendNotification(std::string variableName, std::string variableValue) const {
    if (!variableName.empty() && !variableValue.empty()) {
        IXML_Document* properties = nullptr;
        int ret = UpnpAddToPropertySet(&properties, variableName.c_str(), variableValue.c_str());
        if (ret != UPNP_E_SUCCESS) {
          dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Update of " + variableName + " failed!");
        }

        dlna::common::configuration::ServiceDescription description = dlna::common::configuration::DescReader::retrieve();
        UpnpDevice_Handle* handle = dlna::common::http::DeviceProvider::getInstance().getDevice();
        ret = UpnpNotifyExt( *handle, description.getUniqueDeviceID().c_str(), description.getCDServiceID().c_str(), properties );
        if (ret != UPNP_E_SUCCESS) {
          dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Upnp notification failed!");
        }

        dlna::common::xml::XmlUtil::log( properties );

        ixmlDocument_free(properties);
    }
}


std::string Notification::findProperty(std::string name) {
    dlna::db::DbReadService dbReadService;
    std::string propertyValue = dbReadService.findPropertyByKey( name );

    return propertyValue;
}


} /* upnp */
} /* http */
} /* dlna */

