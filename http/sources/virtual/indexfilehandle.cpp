#include "indexfilehandle.h"

namespace dlna {
namespace http {
namespace virdir {

IndexFileHandle::IndexFileHandle(std::string version, std::string date) :
    AbstractFileHandle(m_index) {
    this->m_index << "<html  xmlns=\"http://www.w3.org/1999/xhtml\">"                                       << std::endl;
    this->m_index <<     "<head>"                                                                           << std::endl;
    this->m_index <<         "<meta charset=UTF-8\" />"                                                     << std::endl;
    this->m_index <<         "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"     << std::endl;
    this->m_index <<         "<link rel=\"stylesheet\" href=\"/web/bootstrap/css/bootstrap.css\">"          << std::endl;
    this->m_index <<         "<script src=\"/web/jquery/jquery.js\"></script>"                              << std::endl;
    this->m_index <<         "<script src=\"/web/bootstrap/js/bootstrap.min.js\"></script>"                 << std::endl;
    this->m_index <<         "<title>DLNA</title>"                                                          << std::endl;
    this->m_index <<     "</head>"                                                                          << std::endl;
    this->m_index <<     "<body>"                                                                           << std::endl;
    this->m_index <<         "<div class=\"container\">"                                                    << std::endl;
    this->m_index <<             "<div class=\"page-header\">"                                              << std::endl;
    this->m_index <<                 "<h1>DLNA</h1>"                                                        << std::endl;
    this->m_index <<             "</div>"                                                                   << std::endl;
    this->m_index <<             "<div class=\"panel panel-default\">"                                      << std::endl;
    this->m_index <<                 "<div class=\"panel-body\">"                                           << std::endl;
    this->m_index <<                     "<h3>Common Information</h3>"                                      << std::endl;
    this->m_index <<                 "</div>"                                                               << std::endl;
    this->m_index <<                 "<div class=\"table-responsive\">"                                     << std::endl;
    this->m_index <<                     "<table class=\"table table-striped\">"                            << std::endl;
    this->m_index <<                         "<tbody>"                                                      << std::endl;
    this->m_index <<                             "<tr>"                                                     << std::endl;
    this->m_index <<                                 "<td width=\"50%\">Version</td>"                       << std::endl;
    this->m_index <<                                 "<td width=\"50%\">" << version << "</td>"             << std::endl;
    this->m_index <<                             "</tr>"                                                    << std::endl;
    this->m_index <<                             "<tr>"                                                     << std::endl;
    this->m_index <<                                 "<td>Status</td>"                                      << std::endl;
    this->m_index <<                                 "<td>Running</td>"                                     << std::endl;
    this->m_index <<                             "</tr>"                                                    << std::endl;
    this->m_index <<                         "</tbody>"                                                     << std::endl;
    this->m_index <<                     "</table>"                                                         << std::endl;
    this->m_index <<                 "</div>"                                                               << std::endl;
    this->m_index <<             "</div>"                                                                   << std::endl;
    this->m_index <<             "<div class=\"panel panel-default\">"                                      << std::endl;
    this->m_index <<                 "<div class=\"panel-body\">Generated: " << date << "</div>"            << std::endl;
    this->m_index <<             "</div>"                                                                   << std::endl;
    this->m_index <<         "</div>"                                                                       << std::endl;
    this->m_index <<     "</body>"                                                                          << std::endl;
    this->m_index << "</html>"                                                                              << std::endl;
}

} /* virdir */
} /* http */
} /* dlna */
