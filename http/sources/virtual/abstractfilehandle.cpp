#include "abstractfilehandle.h"

namespace dlna {
namespace http {
namespace virdir {

AbstractFileHandle::AbstractFileHandle(std::istream& stream) :
  m_stream(stream) {
}

AbstractFileHandle::~AbstractFileHandle() {
}

int AbstractFileHandle::read(char* buffer, size_t bufferLength) {
    int retValue = -1;
    if (this->m_stream.good()) {
        this->m_stream.read(buffer, bufferLength);

        retValue = this->m_stream.gcount();
    } else {
        if (this->m_stream.eof()) {
            retValue = 0;
        }
    }

    return retValue;
}

int AbstractFileHandle::seek(off_t offset, int origin) {
    int retValue = -1;

    if (this->m_stream.good()) {
        switch (origin) {
            case SEEK_CUR:
              this->m_stream.seekg(offset, std::ios::cur);
              break;
            case SEEK_END:
              this->m_stream.seekg(offset, std::ios::end);
              break;
            case SEEK_SET:
              this->m_stream.seekg(offset);
              break;
        }

        retValue = 0;
    }

    return retValue;
}

} /* virdir */
} /* http */
} /* dlna */

