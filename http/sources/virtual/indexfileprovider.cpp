#include "indexfileprovider.h"
#include "indexfilehandle.h"

#include <dlna/common/xmlutil.h>

#include <dlna/common/stringutil.h>
#include <dlna/common/constant.h>
#include <dlna/common/fileutil.h>

#include <ctime>

namespace dlna {
namespace http {
namespace virdir {

int IndexFileProvider::getFileInfo(const char* filename, struct File_Info *info) {
    info->file_length = -1;
    info->is_directory = false;
    info->is_readable = true;
    info->content_type = dlna::common::xml::XmlUtil::allocate("text/html");

    return 0;
}

AbstractFileHandle* IndexFileProvider::createFileHandle(const char* filename) {
    std::string version = dlna::common::configuration::Constant::VERSION;

    std::time_t now = std::time(nullptr);
    std::tm local_time;
    std::tm *gmtm = gmtime_r( &now, &local_time );

    char str[26];
    std::string date = "UTC ";
    date += asctime_r( gmtm, str);

    dlna::http::virdir::AbstractFileHandle* handle = new dlna::http::virdir::IndexFileHandle(version, date);
    return handle;
}

} /* virdir */
} /* http */
} /* dlna */
