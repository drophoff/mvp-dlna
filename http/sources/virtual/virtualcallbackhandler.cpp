#include "virtualcallbackhandler.h"
#include "fileproviderfactory.h"

#include <dlna/common/logutil.h>
#include <dlna/common/fatalexception.h>

namespace dlna {
namespace http {
namespace virdir {

int VirtualCallbackHandler::webserver_get_info(const char *filename, struct File_Info *info) {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::DEBUG, "Web Server: Get Information");

    FileProviderFactory factory;
    std::unique_ptr<FileProvider> provider = factory.create(filename);

    int retValue = -1;
    try {
        retValue = (provider) ? provider->getFileInfo(filename, info) : -1;
    } catch (dlna::common::exception::FatalException& e) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Web Server: Get Information aborted!");
    }

    if (retValue < 0) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::WARN, "Web Server: Get Information returns an expected error code!");
    }

    return retValue;
}

UpnpWebFileHandle VirtualCallbackHandler::webserver_open(const char *filename, enum UpnpOpenFileMode mode) {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::DEBUG, "Web Server: Open");

    UpnpWebFileHandle returnValue = nullptr;
    if (mode == UPNP_READ) {
        FileProviderFactory factory;
        std::unique_ptr<FileProvider> provider = factory.create(filename);

        returnValue = (provider) ? provider->createFileHandle(filename) : nullptr;
    } else {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::WARN, "Web Server: Only 'read' open file operation is supported");
    }

    return returnValue;
}

int VirtualCallbackHandler::webserver_read(UpnpWebFileHandle fh, char *buf, size_t buflen) {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::DEBUG, "Web Server: Read");

    dlna::http::virdir::AbstractFileHandle* handle = (dlna::http::virdir::AbstractFileHandle*)fh;

    int retValue = -1;
    try {
        retValue = handle->read(buf, buflen);
    } catch (dlna::common::exception::FatalException& e) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Web Server: Read aborted!");
    }

    if (retValue < 0) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::WARN, "Web Server: Read returns an expected error code!");
    }

    return retValue;
}

int VirtualCallbackHandler::webserver_write(UpnpWebFileHandle fh, char *buf, size_t buflen) {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::WARN, "Web Server: Write operation is not supported");

    return 0;
}

int VirtualCallbackHandler::webserver_seek(UpnpWebFileHandle fh, off_t offset, int origin) {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::DEBUG, "Web Server: Seek");

    dlna::http::virdir::AbstractFileHandle* handle = (dlna::http::virdir::AbstractFileHandle*)fh;

    int retValue = 0;
    try {
        retValue = handle->seek(offset, origin);
    } catch (dlna::common::exception::FatalException& e) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Web Server: Seek aborted!");
    }

    if (retValue != 0) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::WARN, "Web Server: Seek returns an expected error code");
    }

    return retValue;
}

int VirtualCallbackHandler::webserver_close(UpnpWebFileHandle fh) {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::DEBUG, "Web Server: Close");

    dlna::http::virdir::AbstractFileHandle* handle = (dlna::http::virdir::AbstractFileHandle*)fh;

    delete handle;
    handle = nullptr;

    return 0;
}

struct UpnpVirtualDirCallbacks* VirtualCallbackHandler::handleVirtual() {
    static struct UpnpVirtualDirCallbacks virtual_dir_callbacks = {
        webserver_get_info,
        webserver_open,
        webserver_read,
        webserver_write,
        webserver_seek,
        webserver_close
    };

    return &virtual_dir_callbacks;
}

} /* virdir */
} /* http */
} /* dlna */
