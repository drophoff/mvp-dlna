#include "cdsfilehandle.h"

#include <ios>

namespace dlna {
namespace http {
namespace virdir {


CDSFileHandle::CDSFileHandle(std::string filename) :
    AbstractFileHandle(m_file) {
    m_file.open(filename, std::ios::in|std::ios::binary);
}

CDSFileHandle::~CDSFileHandle() {
    if (this->m_file.is_open()) {
        this->m_file.close();
    }
}

} /* virdir */
} /* http */
} /* dlna */
