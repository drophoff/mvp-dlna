#include "fileproviderfactory.h"
#include "cdsfileprovider.h"
#include "indexfileprovider.h"

#include <dlna/common/constant.h>

#include <string>

namespace dlna {
namespace http {
namespace virdir {

std::unique_ptr<FileProvider> FileProviderFactory::create(const char* filename) {
    std::string file(filename);

    std::unique_ptr<FileProvider> fileProvider;
    if (file.find(dlna::common::configuration::Constant::VIRTUAL_CDS_DIR) == 0) {
        fileProvider =  std::unique_ptr<FileProvider>(new CDSFileProvider());
    } else if (file.find(dlna::common::configuration::Constant::VIRTUAL_WEB_DIR) == 0) {
        fileProvider =  std::unique_ptr<FileProvider>(new IndexFileProvider());
    }

    return fileProvider;
};

} /* virdir */
} /* http */
} /* dlna */
