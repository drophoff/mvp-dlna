#include "cdsfileprovider.h"
#include "cdsfilehandle.h"

#include <dlna/common/xmlutil.h>

#include <dlna/db/dbreadservice.h>
#include <dlna/common/stringutil.h>
#include <dlna/common/constant.h>
#include <dlna/common/fileutil.h>

namespace dlna {
namespace http {
namespace virdir {

int CDSFileProvider::getFileInfo(const char* filename, struct File_Info *info) {
    int retValue = -1;

    dlna::common::util::StringUtil stringUtil(filename);
    std::string file = stringUtil.remove(dlna::common::configuration::Constant::VIRTUAL_CDS_DIR).getString();

    if (!file.empty()) {
        long int metaInfoId = std::stol( file );

        dlna::db::DbReadService dbReadService;
        std::string realFileName = dbReadService.findFilenameById( metaInfoId );

        if (!realFileName.empty()) {
            dlna::common::util::FileUtil fileUtil(realFileName);
            info->file_length = fileUtil.getSize();
            info->last_modified = fileUtil.getTime();
            info->is_directory = fileUtil.isFile();
            info->is_readable = true;
            info->content_type = dlna::common::xml::XmlUtil::allocate("application/octet-stream");

            retValue = 0;
        }
    }

    return retValue;
}

AbstractFileHandle* CDSFileProvider::createFileHandle(const char* filename) {
    dlna::common::util::StringUtil stringUtil(filename);
    std::string file = stringUtil.remove(dlna::common::configuration::Constant::VIRTUAL_CDS_DIR).getString();
    long int metaInfoId = std::stol( file );

    dlna::db::DbReadService dbReadService;
    std::string realFileName = dbReadService.findFilenameById( metaInfoId );

    dlna::http::virdir::AbstractFileHandle* handle = new dlna::http::virdir::CDSFileHandle(realFileName);
    return handle;
}

} /* virdir */
} /* http */
} /* dlna */
