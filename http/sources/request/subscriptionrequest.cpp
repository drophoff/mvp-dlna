#include "subscriptionrequest.h"

#include <dlna/common/xmlutil.h>
#include <dlna/common/logutil.h>
#include <dlna/common/constant.h>
#include <dlna/common/deviceprovider.h>

#include <iostream>
#include <upnp/upnp.h>
#include <upnp/upnptools.h>

namespace dlna {
namespace http {
namespace request {


bool SubscriptionRequest::isResponsibleFor(Upnp_EventType event) {
    return (event == UPNP_EVENT_SUBSCRIPTION_REQUEST) ? true : false;
}

void SubscriptionRequest::handle(void* event) {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Handle Request: Subscription");
    struct Upnp_Subscription_Request* request = (struct Upnp_Subscription_Request*) event;

    IXML_Document* properties = nullptr;
    int ret = UpnpAddToPropertySet(&properties, dlna::common::configuration::Constant::PROPERTY_SYSTEM_UPDATE_ID.c_str(), "0");
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Creation of system update property failed!");
    }

    ret = UpnpAddToPropertySet(&properties, dlna::common::configuration::Constant::PROPERTY_CONTAINER_UPDATE_ID.c_str(), "");
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Creation of container update property failed!");
    }

    UpnpDevice_Handle* handle = dlna::common::http::DeviceProvider::getInstance().getDevice();
    ret = UpnpAcceptSubscriptionExt( *handle, request->UDN, request->ServiceId, properties, request->Sid );
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Subscription failed");
    }

    dlna::common::xml::XmlUtil::log( properties );

    ixmlDocument_free(properties);
}

} /* request */
} /* http */
} /* dlna */

