#include "browserequest.h"
#include "xmlbrowseparser.h"
#include "xmlbrowserequestparameter.h"

#include <dlna/db/dbreadservice.h>
#include <dlna/common/logutil.h>
#include <dlna/common/configreader.h>
#include <dlna/common/constant.h>
#include <dlna/common/descriptionreader.h>
#include <dlna/common/xmlutil.h>

#include <upnp/upnp.h>
#include <string>
#include <cstring>
#include <limits>

namespace dlna {
namespace http {
namespace request {


bool BrowseRequest::isResponsibleFor(Upnp_EventType event) {
    return (event == UPNP_CONTROL_ACTION_REQUEST) ? true : false;
}

void BrowseRequest::handle(void* event) {
    struct Upnp_Action_Request* request = (struct Upnp_Action_Request*) event;

    dlna::common::xml::XmlUtil::log( request->ActionRequest );

    if (strcmp(request->ActionName, "Browse") == 0) {
        handleRequest(request, true);
    } else if (strcmp(request->ActionName, "Search") == 0) {
        handleRequest(request, false);
    } else if (strcmp(request->ActionName, "GetSystemUpdateID") == 0) {
        handleGetSystemUpdateID(request);
    } else if (strcmp(request->ActionName, "GetSortCapabilities") == 0) {
        handleGetSortCapabilities(request);
    } else if (strcmp(request->ActionName, "GetSearchCapabilities") == 0) {
        handleGetSearchCapabilities(request);
    } else {
        handleError(request);
    }

    dlna::common::xml::XmlUtil::log( request->ActionResult );
}

void BrowseRequest::handleError(struct Upnp_Action_Request* request) {
    std::string invalidRequest = "Invalid Action Request: ";
    invalidRequest += request->ActionName;

    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, invalidRequest);

    request->ErrCode = UPNP_E_INVALID_ACTION;
    request->ActionResult = nullptr;
}

void BrowseRequest::handleGetSearchCapabilities(struct Upnp_Action_Request* request) {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Handle Request: Get Search Capabilities");

    dlna::common::configuration::ServiceDescription description = dlna::common::configuration::DescReader::retrieve();
    int ret = UpnpAddToActionResponse(&request->ActionResult, request->ActionName, description.getCDServiceType().c_str(), "SearchCaps", "@id,@parentID" );
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Failure at GetSortCapabilities");
    }
}


void BrowseRequest::handleGetSortCapabilities(struct Upnp_Action_Request* request) {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Handle Request: Get Sort Capabilities");
    dlna::common::configuration::Configuration config = dlna::common::configuration::ConfigReader::retrieve();
    std::string sortCapabilities = (config.isStrictModeOn()) ? "dc:title"  : "dc:title,upnp:originalTrackNumber";
    dlna::common::configuration::ServiceDescription description = dlna::common::configuration::DescReader::retrieve();

    int ret = UpnpAddToActionResponse(&request->ActionResult, request->ActionName, description.getCDServiceType().c_str(), "SortCaps", sortCapabilities.c_str() );
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Failure at GetSortCapabilities");
    }
}


void BrowseRequest::handleGetSystemUpdateID(struct Upnp_Action_Request* request) {
    dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Handle Request: System Update ID");
    dlna::db::DbReadService dbReadService;
    std::string propertySystemUpdateID = dbReadService.findPropertyByKey( dlna::common::configuration::Constant::PROPERTY_SYSTEM_UPDATE_ID );
    dlna::common::configuration::ServiceDescription description = dlna::common::configuration::DescReader::retrieve();

    int ret = UpnpAddToActionResponse(&request->ActionResult, request->ActionName, description.getCDServiceType().c_str(), "Id", propertySystemUpdateID.c_str() );
    if (ret != UPNP_E_SUCCESS) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "Failure at " + dlna::common::configuration::Constant::PROPERTY_SYSTEM_UPDATE_ID);
    }
}


void BrowseRequest::handleRequest(struct Upnp_Action_Request* request, bool isBrowse) {
    if (isBrowse) {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Handle Request: Browse");
    } else {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::INFO, "Handle Request: Search");
    }

    dlna::http::xml::XmlBrowseParser xmlParser(request->ActionRequest);
    const dlna::http::xml::XmlBrowseRequestParameter * const requestParameter = xmlParser.parse();

    if (requestParameter != nullptr) {
        std::shared_ptr<dlna::db::datastructure::entity::SearchWindowDS> searchWindow;
        std::shared_ptr<dlna::db::datastructure::entity::SearchCriterionDS> searchCriterion;
        if (isBrowse) {
            searchWindow = createWindow( requestParameter );
        } else {
            searchCriterion = createCriterion( requestParameter );
        }
        std::shared_ptr<dlna::db::datastructure::entity::SearchSortingDS> searchSorting = createSorting( requestParameter );

        dlna::db::DbReadService dbReadService;
        dlna::db::datastructure::entity::SearchResultDS searchResult = dbReadService.find( searchWindow, searchCriterion, searchSorting ); 
        std::vector<dlna::db::datastructure::entity::DataStructureDTO> resultData = searchResult.getResult();

        long int totalNumbers = searchResult.getTotalNumber();
        long int updated = searchResult.getContainerUpdated();
        std::string totalNumberReturned = std::to_string( totalNumbers );
        std::string numberReturned = std::to_string( resultData.size() );
        std::string updatedReturned = std::to_string( updated );

        dlna::common::configuration::Configuration config = dlna::common::configuration::ConfigReader::retrieve();
        dlna::common::configuration::ServiceDescription description = dlna::common::configuration::DescReader::retrieve();

        std::string virtualServer;
        virtualServer = "http://" + config.getIP() + ":" + std::to_string( config.getPort() ) + dlna::common::configuration::Constant::VIRTUAL_CDS_DIR;

        std::string xml;
        xml ="<DIDL-Lite xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:upnp=\"urn:schemas-upnp-org:metadata-1-0/upnp/\" xmlns=\"urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/\">";
        for (unsigned int index = 0; index < resultData.size(); index++) {
            dlna::db::datastructure::entity::DataStructureDTO data = resultData.at( index );

            if (data.getType().getName() == "object.container.storageFolder" ) {
                xml +="<container id=\""+std::to_string(data.getID())+"\" parentID=\""+std::to_string(data.getParentId())+"\" restricted=\"true\" childCount=\""+std::to_string(data.getChildCount())+"\">";
                xml +="<dc:title>"+data.getName()+"</dc:title>";
                xml +="<upnp:class>"+data.getType().getName()+"</upnp:class>";
                xml +="<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>";
                xml +="<upnp:storageUsed>-1</upnp:storageUsed>";
                xml +="</container>";
            } else {
                xml +="<item id=\""+std::to_string(data.getID())+"\" parentID=\""+std::to_string(data.getParentId())+"\" restricted=\"true\">";
                xml +="<dc:title>"+data.getName()+"</dc:title>";
                xml +="<upnp:class>"+data.getType().getName()+"</upnp:class>";

                std::string date = data.getMetaInfo().getDate();
                if (config.isStrictModeOn()) {
                    date = date.substr(0, date.find("T"));
                }
                xml +="<dc:date>"+date+"</dc:date>";

                xml +="<res size=\""+std::to_string(data.getMetaInfo().getSize())+"\" protocolInfo=\""+data.getType().getProtocol()+"\">" + virtualServer + std::to_string( data.getMetaInfo().getID() ) +"</res>";
                xml +="</item>";
            }
        }
        xml +="</DIDL-Lite>";

        UpnpAddToActionResponse(&request->ActionResult, request->ActionName, description.getCDServiceType().c_str(), "Result", xml.c_str());
        UpnpAddToActionResponse(&request->ActionResult, request->ActionName, description.getCDServiceType().c_str(), "NumberReturned", numberReturned.c_str());
        UpnpAddToActionResponse(&request->ActionResult, request->ActionName, description.getCDServiceType().c_str(), "TotalMatches", totalNumberReturned.c_str());
        UpnpAddToActionResponse(&request->ActionResult, request->ActionName, description.getCDServiceType().c_str(), "UpdateID", updatedReturned.c_str());
    } else {
        handleError(request);
    }
}

std::shared_ptr<dlna::db::datastructure::entity::SearchWindowDS> BrowseRequest::createWindow(const dlna::http::xml::XmlBrowseRequestParameter * const parameter) {
    long int objectID = parameter->getObjectId();
    long int startIndex = parameter->getStartingIndex();
    long int requestedCount = parameter->getRequestedCount();
    bool forItSelf = (parameter->isBrowseMetaData()) ? true : false;

    long int lmax = std::numeric_limits<long int>::max() - 1;
    long int numberOfEntries = (requestedCount == 0) ? lmax : requestedCount;

    std::shared_ptr<dlna::db::datastructure::entity::SearchWindowDS> searchWindow = std::shared_ptr<dlna::db::datastructure::entity::SearchWindowDS>(new dlna::db::datastructure::entity::SearchWindowDS(objectID, startIndex, numberOfEntries, forItSelf));
    return searchWindow;
}

std::shared_ptr<dlna::db::datastructure::entity::SearchCriterionDS> BrowseRequest::createCriterion(const dlna::http::xml::XmlBrowseRequestParameter * const parameter) {
    std::shared_ptr<dlna::db::datastructure::entity::SearchCriterionDS> searchCriterion = std::shared_ptr<dlna::db::datastructure::entity::SearchCriterionDS>(new dlna::db::datastructure::entity::SearchCriterionDS());
    return  searchCriterion;
}

std::shared_ptr<dlna::db::datastructure::entity::SearchSortingDS> BrowseRequest::createSorting(const dlna::http::xml::XmlBrowseRequestParameter * const parameter) {
    std::string sort = parameter->getSort();

    /**
     * Determine the sort order based on the client request.
     */
    dlna::db::datastructure::entity::SortOrder titleSortOrder = dlna::db::datastructure::entity::SortOrder::NOT_DEFINED;
    if (sort.find("+dc:title") != std::string::npos || sort.find("+upnp:originalTrackNumber") != std::string::npos) {
        titleSortOrder = dlna::db::datastructure::entity::SortOrder::ASCENDING;
    } else if (sort.find("-dc:title") != std::string::npos || sort.find("-upnp:originalTrackNumber") != std::string::npos) {
        titleSortOrder = dlna::db::datastructure::entity::SortOrder::DESCENDING;
    }

    /**
     * Use ascending sorting as default in case the client provides no explicit
     * sort order and the strict DLNA mode is not enabled. This is necessary because
     * some clients provides no sorting criterion.
     */
    if (titleSortOrder == dlna::db::datastructure::entity::SortOrder::NOT_DEFINED) {
        dlna::common::configuration::Configuration config = dlna::common::configuration::ConfigReader::retrieve();
        bool strictModeEnabled = config.isStrictModeOn();
        if (!strictModeEnabled) {
            titleSortOrder = dlna::db::datastructure::entity::SortOrder::ASCENDING;
        }
    }

    std::shared_ptr<dlna::db::datastructure::entity::SearchSortingDS> searchSorting = std::shared_ptr<dlna::db::datastructure::entity::SearchSortingDS>(new dlna::db::datastructure::entity::SearchSortingDS(titleSortOrder));
    return searchSorting;
}


} /* request */
} /* http */
} /* dlna */

