#include "processhttp.h"

#include <dlna/db/dbreadservice.h>
#include <dlna/common/configreader.h>
#include <dlna/common/logutil.h>
#include <dlna/common/fatalexception.h>
#include <dlna/common/constant.h>

#include <sys/wait.h>

namespace dlna {
namespace http {

ProcessHttp::ProcessHttp() {
    this->m_upnpSubsystem = nullptr;
    this->m_notification = nullptr;
    this->m_transactionManager = nullptr;
    this->m_called = 0;
}

ProcessHttp::~ProcessHttp() {
    /**
     * Should be empty, otherwise it will be called serveral times
     * depending on the size of Tasks (see main).
     *
     * But we need to release the resources, so make the implementation
     * stateless (can be called several times).
     */
    if (this->m_upnpSubsystem != nullptr) {
        delete this->m_upnpSubsystem;
        this->m_upnpSubsystem = nullptr;
    }
    if (this->m_notification != nullptr) {
        delete this->m_notification;
        this->m_notification = nullptr;
    }
    if (this->m_transactionManager != nullptr) {
        delete this->m_transactionManager;
        this->m_transactionManager = nullptr;
    }
}


void ProcessHttp::execute() {
    /**
     * Increase counter on every execution (see getDelay)
     * to have two different logical executions.
     */
    this->m_called++;

    /**
     * Sends notification to registered clients
     */
    if (this->m_called % 2 == 0) {
        this->m_notification->send();
    }

    if (this->m_called == 1) {
         this->m_upnpSubsystem->sendAdvertisement( 46 * 60 );
    }

    if (this->m_called % 44 == 0) {
        this->m_called = 0;
    }

    /**
     * Flush the current transaction on every execution
     * to avoid a transactional journal overflow.
     */
    this->m_transactionManager->flush();
}


unsigned short ProcessHttp::getDelay() const {
    return 60;
}

void ProcessHttp::executeOneTime() {
    if (this->m_transactionManager == nullptr) {
        /**
         * Add delay to avoid concurrent access from ProcessDB
         * and ProcessHttp.
         */
        sleep( 5 );
        /**
         * Use a global transaction that is used from all requests.
         */
        this->m_transactionManager = new dlna::db::TxManager();
    }


    /**
     * Check precondition
     */
    dlna::common::configuration::Configuration config = dlna::common::configuration::ConfigReader::retrieve();
    bool currentStrictMode = config.isStrictModeOn();

    dlna::db::DbReadService dbReadService;
    std::string originalStrictMode = dbReadService.findPropertyByKey( dlna::common::configuration::Constant::PROPERTY_STRICT_MODE );

    bool isStrictModeUnchanged = ((originalStrictMode == "false" && currentStrictMode == false) || (originalStrictMode == "true" && currentStrictMode == true) ) ? true : false;
    if (isStrictModeUnchanged) {
        if (this->m_upnpSubsystem == nullptr) {
            this->m_upnpSubsystem = new dlna::http::upnp::Server();
        }

        if (this->m_notification == nullptr) {
            this->m_notification = new dlna::http::upnp::Notification();
        }
    } else {
        dlna::common::logging::LogUtil::getInstance().log(dlna::common::logging::LogLevel::ERROR, "The Strict Mode has changed, recreate the database or revert the configuration change!");

        throw dlna::common::exception::FatalException();
    }
}


const std::string ProcessHttp::getName() const {
    return "http";
}

} /* http */
} /* dlna */
